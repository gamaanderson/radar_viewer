#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mar 2013
@author: tiagoburiol@gmail.com
"""
import pygame
import time
import logging
from Sprites import *
from palette import palette
from palette import HMC_legend


class Text:

    
    
    def __init__(self,father):
        self.father=father
        self.updateFontSize()
        self.textColor = (255, 255, 255)
        self.textColorOutline=(0,0,0) 
        self.textPosH  = (5, 0)
        self.textPosF  = (5, father.rect.height -self.textFont.get_height()-3)
        #self.font = pygame.font.SysFont("ubuntumono", 13)
        
        
        
        
        
        #text sprites
        self.father.sprites['latlon']=Sprite(pygame.Rect(self.textPosF,(150,self.textFont.get_height()+3)),who_created_me="Text.__init__")
        w = self.textFontOutline.size("W Elevation: 99.9\xb0 W")[0]+10
        self.father.sprites['elev_text']=Sprite(pygame.Rect(2*father.rect.width/5,0,w,self.textFont.get_height()+3),who_created_me="Text.__init__")
        w = self.textFontOutline.size("Range: 500.0 km")[0]
        self.father.sprites['range_text']=Sprite(pygame.Rect(3*father.rect.width/5,father.rect.height-self.textFont.get_height()-3,w,self.textFont.get_height()+3),who_created_me="Text.__init__")
        w = self.textFontOutline.size("Altitude: 500.0 km")[0]
        self.father.sprites['height_text']=Sprite(pygame.Rect(4*father.rect.width/5,father.rect.height-self.textFont.get_height()-3,w,self.textFont.get_height()+3),who_created_me="Text.__init__")
        self.father.sprites['date']=Sprite(pygame.Rect(self.textPosH,(220,self.textFont.get_height()+3)),who_created_me="Text.__init__")
        w = self.textFontOutline.size("WIDTHH: No Value")[0]
        posZ = (int((father.rect.width - w) * 0.5)-20, self.textPosF[1])
        self.father.sprites['data']=Sprite(pygame.Rect(posZ,(w,self.textFont.get_height()+3)),who_created_me="Text.__init__")

        self.father.sprites['latlon'].image.set_colorkey((255,0,255))
        self.father.sprites['elev_text'].image.set_colorkey((255,0,255))
        self.father.sprites['range_text'].image.set_colorkey((255,0,255))
        self.father.sprites['height_text'].image.set_colorkey((255,0,255))
        self.father.sprites['date'].image.set_colorkey((255,0,255))
        self.father.sprites['data'].image.set_colorkey((255,0,255))

        self.father.sprites['latlon'].image.fill((255,0,255)) 
        self.father.sprites['elev_text'].image.fill((255,0,255))
        self.father.sprites['range_text'].image.fill((255,0,255))
        self.father.sprites['height_text'].image.fill((255,0,255))
        self.father.sprites['date'].image.fill((255,0,255)) 
        self.father.sprites['data'].image.fill((255,0,255)) 


    
    # Ajusta o tamanho da fonte para ficar bem para o tamanho da tela
    def updateFontSize(self):
        """ TODO: definir diferentes tamanhos de fontes """

        res = self.father.rect.size
        
        if res[1]<500:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        elif res[1]<700:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        elif res[1]<900:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        elif res[1]<1200:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        
    	self.textPosH  = (5, 0)
        self.textPosF  = (5, res[1]-self.textFont.get_height()-3)
        
        
    def getMetadataFromFile(self,nc): #recebe o arquivo NetCDF e le os metadados
        tm=time.strptime("".join(nc.variables['time_coverage_start'].data), "%Y-%m-%dT%H:%M:%SZ")
        self.metadados= ["Base Time: "+ time.asctime(tm), 
                 "Bins Number: "+ str(nc.dimensions['range']),
                 "Range: "+ str(nc.variables['range'][-1])+"m",
                 "Radar Latitude: "+ str(nc.variables['latitude'].data),
                 "Radar Longitude: "+ str(nc.variables['longitude'].data)]

    def printDate(self, base_time):
        try:
    	    tm=time.strptime("".join(base_time), "%Y-%m-%dT%H:%M:%SZ")
    	except:
    	    tm=time.strptime("".join(base_time), "%Y-%m-%dT%H:%M:%S.%fZ")
    	t = time.asctime(tm) 
    	t = u'\u25c4 '+t+u' \u25ba'
        self.father.sprites['date'].image.fill((255,0,255))
        writePixel(self.father.sprites['date'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,t)
        self.father.sprites['date'].dirty=1
        #write(self.father.sprites['date'].image,self.textFont,(1, 1),self.textColor,t)
        
    def printLatLon(self):
        t = "Lat:"+ str(self.lat)[0:6] + " Lon:"+ str(self.lon)[0:6]
        #pos = (5, pygame.display.get_surface().get_height()-18)
        self.father.sprites['latlon'].image.fill((255,0,255))
        writePixel(self.father.sprites['latlon'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,t)
        #write(self.father.sprites['latlon'].image,self.textFont,(1,1),self.textColor,t)
        self.father.sprites['latlon'].dirty=1
#        textshadow = fontshadow.render(t, 1, (0,0,0))
#        displayWindow.self.father.sprites['latlon'].blit(textshadow, (4, 2))
#        textshadow2 = fontshadow2.render(t, 1, (100,100,100))
#        displayWindow.self.father.sprites['latlon'].blit(textshadow2, (6, 0))
#        
#        text = font.render(t, 1, (255,255,255))
#        displayWindow.self.father.sprites['latlon'].blit(text, (5, 1))
        #text = font.render(t, 1, (0,0,0))
        #screen.blit(text, (5+512, 256+5))
        


    def setLatLon(self, latlon):
        self.lat = latlon[0]
        self.lon = latlon[1]

    def printDRH(self, (d,r,h),moment="DBZH"):
        if moment=="HMC" and type(d) is not str:
            try: d=HMC_legend[int(d)-1] 
            except: pass
        if type(d) is str:
            t = moment+":"+ d
        else:
            t = moment+": %.2f"%d+palette[moment][5]
        self.father.sprites['data'].image.fill((255,0,255))
        writePixel(self.father.sprites['data'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,t)
        #write(self.father.sprites['data'], self.textFont, (1,1), self.textColor, t)
        self.father.sprites['data'].dirty=1

        t ="range: " + "%.1f"%r + "km" 
        self.father.sprites['range_text'].image.fill((255,0,255))
        writePixel(self.father.sprites['range_text'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,t)
        #write(self.father.sprites['reflectivity'], self.textFont, (1,1), self.textColor, t)
        self.father.sprites['range_text'].dirty=1
        
        if h!=None:
            t ="altitude: " + str(h) + "m" 
            self.father.sprites['height_text'].image.fill((255,0,255))
            writePixel(self.father.sprites['height_text'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,t)
            #write(self.father.sprites['reflectivity'], self.textFont, (1,1), self.textColor, t)
            self.father.sprites['height_text'].dirty=1
        
    def printElev(self, elev, angle):
        self.father.sprites['elev_text'].image.fill((255,0,255))
        writePixel(self.father.sprites['elev_text'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,u'\u25b2 Elevation: %0.1f\xb0 \u25bc' % angle)
        #write(self.father.sprites['elev_text'].image,
        #      self.textFont, 
        #      (1,1), self.textColor, 
        #      "Elevation: "+ str(elev))

    def printAltitude(self, elev, alt):
        self.father.sprites['elev_text'].image.fill((255,0,255))
        writePixel(self.father.sprites['elev_text'].image,self.textFont,self.textFontOutline,(1,1),self.textColor,self.textColorOutline,u'\u25b2 Altitude: %0.1f km \u25bc' % alt)        

        
        
def writePixel(s,font,outline,pos,color,outline_color,text):
    text=" "+text
    text_surf=font.render(text,0,color)
    outline_surf=outline.render(text,0,outline_color)
    s.blit(outline_surf,(pos[0]-3,pos[1]))  
    s.blit(text_surf,(pos[0]-3,pos[1]))      
        
        
# RETIRADO DE GPU
# Python Game Utilities
# https://code.google.com/p/pgu/downloads/detail?name=pgu-0.18.zip
"""A collection of text rendering functions"""

def write(s,font,pos,color,text,border=1):
    """Write text to a surface with a black border"""
    # Render the text in black, at various offsets to fake a border
    tmp = font.render(text,0,(0,0,0))
    dirs = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
    for dx,dy in dirs: 
        s.blit(tmp,(pos[0]+dx*border,pos[1]+dy*border))
    # Now render the text properly, in the proper color
    tmp = font.render(text,1,color)
    s.blit(tmp,pos)

def writec(s,font,color,text,border=1):
    """Write centered text to a surface with a black border"""
    # Center the text within the destination surface
    w,h = font.size(text)
    x = (s.get_width()-w)/2
    y = (s.get_height()-h)/2
    write(s,font,(x,y),color,text,border)
    
def writepre(s,font,rect,color,text):
    """Write preformatted text on a pygame surface"""
    r,c,txt = rect,color,text
    txt = txt.replace("\t","        ")
    tmp = font.render(" ",1,c)
    sw,sh = tmp.get_size()
    y = r.top
    for sentence in txt.split("\n"):
        x = r.left
        tmp = font.render(sentence,1,c)
        s.blit(tmp,(x,y))
        y += sh

def writewrap(s, font, rect, color, text, maxlines=None, wrapchar=False):
    """Write wrapped text on a pygame surface.

    maxlines -- specifies the maximum number of lines to write 
        before stopping
    wrapchar -- whether to wrap at the character level, or 
        word level
    """
    r,c,txt = rect,color,text
    txt = txt.replace("\t", " "*8)
    tmp = font.render(" ", 1, c)
    sw,sh = tmp.get_size()
    y = r.top
    row = 1
    done = False
    for sentence in txt.split("\n"):
        x = r.left
        if wrapchar:
            words = sentence
        else:
            words = sentence.split(" ")
            
        for word in words:
            if (not wrapchar):
                word += " "
            tmp = font.render(word, 1, c)
            (iw, ih) = tmp.get_size()
            if (x+iw > r.right):
                x = r.left
                y += sh
                row += 1
                if (maxlines != None and row > maxlines):
                    done = True
                    break
            s.blit(tmp, (x, y))
            #x += iw+sw
            x += iw
        if done:
            break
        y += sh
        row += 1
        if (maxlines != None and row > maxlines):
            break
