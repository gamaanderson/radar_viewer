#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np 
import pygame
from palette import palette
import netcdf_module
import hdf5_module
import mdv_module
 
from scipy import ndimage

volume=None
fixed_angles_rad=None
volume_moment=None


def start_file(hdf,moment):#for compactibility with modules that use pre-processing
    make_vol_360(hdf, moment)
    

def returnBackground(image,scaled_rect,image_rect): # load backgrond return surface of size of surfrec (rectangle). If full=False cut current PPI region
        if image==None:
            surf=pygame.Surface(image_rect.size)
            return surf            
            
        background = pygame.image.load(image)
        background=background.convert()
        X=-scaled_rect.left*background.get_width()/scaled_rect.size[0]
        Y=-scaled_rect.top*background.get_width()/scaled_rect.size[1]
        width=image_rect.width*background.get_width()/scaled_rect.size[0]
        height=image_rect.height*background.get_height()/scaled_rect.size[1]
        rect=pygame.Rect(X,Y,width,height)
    
        #Now we have to fix rectange outside surface
        if background.get_rect().contains(rect):# no problem
            #cut rect off and scale
            return pygame.transform.smoothscale(background.subsurface(rect), image_rect.size)
        else:# this only make sence for full=False
            #clif rectangle
            new_rect=rect.clip(background.get_rect())
            # calculate new size to scale
            scale_X=new_rect.width*image_rect.width/width
            scale_Y=new_rect.height*image_rect.height/height
            background_scaled=pygame.transform.smoothscale(background.subsurface(new_rect), (scale_X,scale_Y))
            surf=pygame.Surface(image_rect.size)
            if background.get_rect().collidepoint(rect.topleft): # top-left corner is in the surf, good, no translation to make
                surf.blit(background_scaled,(0,0))
            else: #have to make some coodinates change, this also work in the above case, but we don't need it there
                #calculate new_rect.topleft in relation to rect.topleft
                new_X=new_rect.left-rect.left
                new_Y=new_rect.top-rect.top
                #scale to surf[0] size
                posX=new_X*image_rect.width/rect.width
                posY=new_Y*image_rect.width/rect.width
                surf.blit(background_scaled,(posX,posY))
                
            return surf





def makePPI_hdf((hdf,elev,moment,threshold,smooth), scaled_rect,image_rect,backgroundPath):
    import palette
    if volume_moment!=moment:
        make_vol_360(hdf, moment)
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
    print color
    print threshold
    surf=PPI(hdf,scaled_rect,image_rect,elev,smooth,color)
    if smooth:
        array = ndimage.filters.median_filter(array, size = 3)
    
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2
    

def makeVerticalCut_hdf((hdf,moment,P,threshold,smooth), scaled_rect,image_rect,topoPath):

    import palette
    if volume_moment!=moment:
        make_vol_360(hdf, moment)
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
    #print (x0,y0),(x1,y1)
#    print scaled_rect.size
    array = geraCorte(hdf,scaled_rect,P[0],P[1])
#    print array.shape
    if smooth:
        array = ndimage.filters.median_filter(array, size = 3)
#    print array.shape
    surf = colormap(np.transpose(array), color, (175, 175, 175))
#    print "surf",surf.get_size()
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((200, 200, 200))
    

    corteSurf.blit(surf,(0,0))
    makeGrid(corteSurf)
    drawGround(topoPath,P,corteSurf)
    return corteSurf
    







       
def makeGrid(surf):
    cor = (60,60,60)
    cor_eixos = (50, 50, 50)
    nx=10
    ny=8

    height = surf.get_height() #altura da surface para calcular o numero de divisoes
    width = surf.get_width() #largura ra surface

    #dx = grid_width/nx
    dy = float(height)/ny
      
    # linhas horizontais e texto, ny = numero de divisoes horizontais
    for i in range(ny+1):
        #print i
        i=i+0.5
        P0 = (0, int(height-i*dy)) # y de baixo pra cima
        P1 = (width+2, int(height-i*dy))
        pygame.draw.aaline(surf, cor, P0,P1)

    # linhas verticais e texto

    dx = float(width)/ny

    for i in range(nx+1):
        P0 = (int(width-i*dx), 0)
        P1 = (int(width-i*dx), height+2)
        #pygame.draw.aaline(surf, cor, P0,P1)
    pygame.draw.rect(surf,cor, surf.get_rect(),1)



import math
def drawGround(topoFile,P,surf):
#	print "drawGround",topoFile
	if topoFile==None: return 0
	n_points=180
	TOPO = np.fromfile( topoFile, dtype= np.int16)
#	print TOPO
	size = math.sqrt(TOPO.size)
#	print size,TOPO.size, TOPO.shape
	TOPO = np.reshape(TOPO, (size,size))
#	TOPO = TOPO.transpose()
	
	
 	cutWidth = surf.get_width()
 	cutHeight =  surf.get_height()

 	xground = []
 	yground = []
 	p = []

	#for i in range(x1-x0):
	for k in range(n_points):
		x = (P[0][0]+k*(P[1][0]-P[0][0])/(n_points-1.))* size/1000000.+size/2.
		y = -(P[0][1]+k*(P[1][1]-P[0][1])/(n_points-1.))* size/1000000.+size/2.
		xground.append(int(k*cutWidth/(n_points-1.)))
		if x<size and y<size and x>=0 and y>=0:
			if TOPO[int(x)][int(y)]<0:
				TOPO[int(x)][int(y)]=0
			elev = TOPO[int(x)][int(y)]*float(cutHeight)/16000
			yground.append(cutHeight- elev)
 			#print x, y, TOPO[y][x]
 		 	p.append((xground[-1], yground[-1]))

	if len(p)>2:
		pygame.draw.lines(surf, (200,200,200), False, p,1)

###################################################################################
###################################################################################
###################################################################################



def colormap(data,palette, backcolor=(0,0,0)):
    #data = ndimage.filters.median_filter(data, size = 3)
    data = np.transpose(data)
    size = data.shape
    surf = pygame.Surface((size[0],size[1]))     # Create a same shape surface
    surf.fill(backcolor)
    #surf.set_colorkey(backcolor)
    colorArray = pygame.surfarray.array3d(surf)  # Create a colorArray from surface
    ncolors = len(palette[0])        # Get the number of
    #print colorArray.shape
    # For each color in palette, evaluate the data interval for this color, 
    # find the index of all data in the interval and place the colorRGB values 
    # in mapped surface  
    for c in range(ncolors):
        color = palette[0][c]         # get RGB values of color number n
        minimo = palette[1][c]             # The minimum value for this color
        maximo = palette[1][c+1]           # The maximum value for this color
        idx = np.where((data>=minimo)&(data<maximo)) # Get the index for data in interval 
        colorArray[idx] = color

    pygame.surfarray.blit_array(surf, colorArray)
    return pygame.Surface.convert(surf)


# @tiago
#def draw_poligons(dw, rd):
#    surf = pygame.display.get_surface()
#    pointlist = [(50, 50),(200,50),(120,230)]
#    pygame.draw.polygon(surf, (255,0,255), pointlist, 0)
#    
#    # Get a sweep from data volume       
#    Zpolar = rd.volume[:,:,0]
#    for r in range((Zpolar).shape[0]-1):
#        for b in range((Zpolar).shape[1]-1):
#            print r, b

#@jit
def azim(I, J):
    AZIM = np.rad2deg(np.arctan2(I,-J))
    AZIM = np.where(AZIM<0,AZIM+360,AZIM)
    return AZIM



#@jit
def corrected_range(I, J, fi):
    #----------------------------------------------------
    # Faz algumas contas para corrigir o range considerando 
    # a curvatura da terra 
    #----------------------------------------------------
    # ver esquema em https://www.dropbox.com/s/3lstq35j052s19h/esquema_formulas.png
    S = np.sqrt(I**2+J**2) # distancia a ate o radar, tomada sobre a superficie da terra
    
    rt =  8500000
    inv_rt = 0.000000118
    r0 = np.tan(S*inv_rt)*rt 
    sigma = np.arctan(r0*inv_rt)
    betha = 0.5 * math.pi + sigma
    alpha = 0.5 * math.pi - sigma - fi
    R = r0 * np.sin(betha) / np.sin(alpha)
    return R
    

def create_ppi(hdf,scaled_rect, rect,elev):
    # Get the size of screen and create a matrix of no-value 
    res = rect.size
    #Zrect = np.tile(999.9, res)    # Matrix of no-value data, waiting to be fill
    
    # Get infos from dw 
    e = elev    # Elevation
    pos = scaled_rect.topleft
    print e
    print volume.shape
    # Get a sweep from data volume       
    Zpolar = volume[:,:,e]
    n_gates = Zpolar.shape[1] # number of gates (bins)
    #Zpolar = ndimage.filters.median_filter(Zpolar, size = 3)
    #Zpolar = ndimage.filters.gaussian_filter(Zpolar, sigma = 1)
  
    radar_range = hdf["dataset1"]["where"].attrs["rstart"]*1000.+hdf["dataset1"]["where"].attrs["rscale"]*hdf["dataset1"]["where"].attrs["nbins"]
    
    Bin_Spacing = hdf["dataset1"]["where"].attrs["rscale"] #250.0 meters
    #----------------------------------------------
    #Cria algumas constantes de conversao de dados
    #----------------------------------------------
    toBin = 1/Bin_Spacing
    # rt: raio da terra em metros
    rt =  8500000 #4 * 6375000 / 3  
    #inv_rt = 1/rt

    # rg: range ground (range do radar sobre a superficie terrestre)
    # ou raio do radar em metros
    fi = fixed_angles_rad[e] # elevacao
    rg = rt * math.atan2((radar_range * math.cos(fi)),(rt+radar_range*math.sin(fi)))

    # Cria uma grid regular para a imagem escalonada do ppi
    # Para o ppi parcial
    i , j = np.meshgrid(np.arange(res[0]), np.arange(res[0]))
    resScaled = scaled_rect.size 
    print "pos:",pos
    metrosPorPixels_x = 2 * 500000 / resScaled[0]
    metrosPorPixels_y = 2 * 500000 / resScaled[1]
    I = (0.5 + i - resScaled[0]*0.5 - pos[0]) * metrosPorPixels_x #coordenadas em metros sobre a superficie da terra
    J = (0.5 + j - resScaled[1]*0.5 - pos[1]) * metrosPorPixels_y #coordenadas em metros sobre a superficie da terra


    RNG = corrected_range(I,J,fi)

    #----------------------------------------------------
    # Calcula o azimute, bin e ray para cada ponto 
    #----------------------------------------------------
    AZIM = azim(I, J)
    
    RAY = (np.round(AZIM)).astype(int) 
    RAY = np.where(RAY==360,0,RAY)
    # Gate indexes @tiago
    BIN = (np.round(RNG * toBin)).astype(int)
    # To avoid index out of baunds @tiago
    BIN2 = np.where(BIN<n_gates-1, BIN, 0)
    # Fill the cartesian matrix of data
    Zrect = np.where(BIN<n_gates-1,  Zpolar[RAY,BIN2], 999.9)
    return Zrect


def PPI(hdf,scaled_rect,image_rect,elev,smooth,color):
    ppidata = create_ppi(hdf,scaled_rect, image_rect,elev)
    if smooth:
        ppidata = ndimage.filters.median_filter(ppidata, size = 3)
    ppiSurf = colormap(ppidata,color)
    return ppiSurf



#################################################################3
#3                           RADAR DATA                          #
#################################################################3


#--------------------------------------------------------
# Create a volume with 360 rays from netcdf file data
#--------------------------------------------------------    
def make_vol_360(hdf, moment = "DBZH"):
    for i in range(100):
        if "dataset%i"%(i+1) not in hdf:
            break
    num_sweeps=i
    fixed_angles = []
    for i in range(num_sweeps):
        fixed_angles.append(hdf["dataset%i"%(i+1)]["where"].attrs["elangle"])
    global fixed_angles_rad
    fixed_angles_rad =  np.deg2rad(fixed_angles)  #elevacoes nominais em radianos
    for i in range(100):
        if "data%i"%(i+1) in hdf["dataset1"]:
            if hdf["dataset1"]["data%i"%(i+1)]["what"].attrs["quantity"]==moment:
                break
    if i==99:
        moment_index=0
    else:
        moment_index=i
    no_value = hdf["dataset1"]["data%i"%(moment_index+1)]["what"].attrs["undetect"]
    max_gates = hdf["dataset1"]["where"].attrs["nbins"]
        
    #print "self.max_gates= " , self.max_gates,"self.num_sweeps = ", self.num_sweeps
    
    # cria um volume sem dados
    global volume
    volume = np.tile(-999.0, (360, max_gates, num_sweeps))
    global volume_moment
    volume_moment=moment
    # Fill the volume
    for e in xrange(num_sweeps):  # for each elevation elevacao...
        #print "num_rays = ", num_rays
        #print ini, end 
        azims = np.linspace(0,359,hdf["dataset%i"%(e+1)]["where"].attrs["nrays"])
        raw_data=hdf["dataset%i"%(e+1)]["data%i"%(moment_index+1)]["data"]
        nGates=hdf["dataset%i"%(e+1)]["where"].attrs["nbins"]
        #print azims
        for r in xrange(360):
            difs = abs(azims-r)
            idx = np.argmin(difs) #indice do raio nos dados mais proximo de r
            dados = raw_data[idx]
            dados = 1.0*dados * hdf["dataset%i"%(e+1)]["data%i"%(moment_index+1)]["what"].attrs["gain"]+ hdf["dataset%i"%(e+1)]["data%i"%(moment_index+1)]["what"].attrs["offset"]
            dados = np.where(abs(raw_data[idx]-no_value)<1,-999,dados)
            tile = np.tile(-999, max_gates- nGates)
            #tile = np.tile(0, self.max_gates- nGates)
            volume [r,:,e] = np.r_[dados, tile]
    return volume

########################################################################
#                       VERTICAL_CUT                                   #
########################################################################

def geraCorte(hdf,rect,(x0, y0), (x1, y1), height=16):
    
    
    data = volume #(360, n_gates, s_sweeps)
    nBins = data.shape[1]
    # pega as elevacoes, converte em radianos e guarda em um array do numpy 

    elevs = fixed_angles_rad
    
    #print nc.variables['elevation'].data
    # Pega o numero de elevacoes, o mesmo que: nElevs = len(nc.variables['elevation'].data)
    nElevs = len(elevs)
    
    # Cria algumas constantes 
    meioGrauEmRad = 0.008726646#(pi/360)#*0.2
    rt =  8500000# (4/3)*6375000
    

         
    # Resolucao horizontal e vertical da matriz do corte
    # Calcula o numero de linhas e colunas para gerar a imagem
    # Descontando o espacamento para o texto e numeros (offset) 
    nRows = rect.height
    nCols = rect.width
    
    
    # Gera as coordenadas espaciais dos pontos do corte
    # Observe que x, y e z sao arrays, cada (x[i],y[i]) corresponde a um
    # ponto no plano horizontal sobre a reta que eh a base do plano do corte vertical 
    x = x0 + (0.5 + np.arange(0,nCols,1)) * (x1-x0)/nCols # Comeca em x0 e vai incrementando dx=(x1-x0)/nCols
    y = y0 + (0.5 + np.arange(0,nCols,1)) * (y1-y0)/nCols # Comeca em y0 e vai incrementando dy=(y1-y0)/nCols
    z = -hdf["/where"].attrs["height"] + (0.5 + np.arange(0,nRows,1)) * height * 1000/nRows
    
    # reverte a ordem dos elementos do vetor z (util para gerar a imagem)
    h = z[::-1]
    # Calcula as distancias de cada ponto da linha base do plano 
    # do corte vertical ate o radar
    s = np.sqrt(x*x+y*y)
    # Calcula os angulos polares no sistema de coordenadas do radar
    # Observe que s e theta sao arrays que dao as coordenadas polares dos 
    # pontos (x,y) sobre a reta que eh a base do plano de corte 
    theta = np.arctan2(x,y)
    
    
    # Array com os indices dos rays que interceptam o plano de corte
    # Esses ja sao os indices correspondentes aos rays do arquivo nc (NetCDF)
    #rays = np.array([], dtype=np.int16) # define o tipo de dado como int16 (parece deixar o programa mais rapido)
    rays = (np.round(np.degrees(theta))) # converte theta para graus e arredonda para o inteiro mais proximo
    rays = np.where(rays>359,rays-360,rays) # se passar de 359, reinicia a contagem :)
    rays = rays.astype(int)
    # Cria uma matriz de nRows por nCols preenchida com valores -999.9
    # Eh nessa matriz que serao inseridos os dados de refletividade do corte.
    # Essa matriz representa o plano de corte.
    DATA = np.tile(-999.9, ( nCols,nRows))
#    print DATA.shape
    
    ################################################
    # Faz as contas para corrigir as alturas
    ################################################
    # Cria uma grade 2D a partir dos arrays s e h
    H,S = np.meshgrid(h,s) 
#    print S.shape
    R0 = rt*np.tan(S/rt)
    H0 = np.sqrt(R0**2 + rt**2)-rt
    SIGMA = S/rt
    BETHA = math.pi*0.5+ SIGMA    
    H1 = H-H0
    R = np.sqrt(np.multiply(R0,R0) + np.multiply(H1,H1) - 2*np.multiply(np.multiply(R0,H1),np.cos(BETHA))) 
    ELEVS = np.arcsin(np.divide(np.multiply(H1,np.sin(BETHA)),R))
    
    gate_size =  hdf["dataset1"]["where"].attrs["rscale"] 
    
    R = np.round(R/gate_size)
    R = np.where(R<nBins-1,R,-1) # to avoid "out of bound"
    #R = (R/gate_size).astype(int)
    R = R.astype(int)
    ################################################
    
    
    # armazena os indices da elevacao correspondente
    ELEVS2 = ELEVS
    
    for i in range(nElevs):
        ELEVS2 = np.where(abs(ELEVS2-elevs[i])>meioGrauEmRad,ELEVS2,i)
    
    ELEVS2 = np.where(ELEVS2-ELEVS==0,-1,ELEVS2)
    ELEVS2 = ELEVS2.astype(int)


    
    # Create a matrix of rays for each poisn in the cut plane
#    RAYS = np.tile(rays, (nCols,nRows))
#    print RAYS.shape
    BAD,RAYS =  np.meshgrid(h,rays) 
#    print RAYS.shape
    
    # Fill the cut matrix with data (New way, much faster) @tiago(18/07/2014)
    #DATA = np.where((R<nBins-1)&(ELEVS2>=0),data[RAYS,R,ELEVS2], -999.9 )
    idx = np.where((R<nBins-1)&(ELEVS2>=0))
    DATA[idx] = data[RAYS[idx],R[idx],ELEVS2[idx]]
    
    # Old way, much slower @tiago(18/07/2014)
#     I = idx[0]
#     J = idx[1]
#     for idx in range(len(I)-1):
#         #print idx
#         e = ELEVS2[I[idx],J[idx]]
#         #r = rays[J[idx]]
#         r = RAYS[I[idx],J[idx]]
#         b = R[I[idx],J[idx]]
#         DATA[I[idx],J[idx]] = data[r,b,e]
    
    
    # reconstroi o perfil    
    #DATA = inter.interpola1D(DATA)
    #print DATA.shape
    return DATA

