#include <Python.h>
#include <stdio.h>
#include <hdf5.h>
#include <netcdf.h>
#include <math.h>
#include <numpy/arrayobject.h>
//#include <time.h>
#define HALF_PI (1.570796326794897)

float range_res=100;
float azimuth_res=0.5;
float elev_res=0.1;


typedef struct {
    int id;
    size_t len;
} Dim;



static PyObject* set_resolution(PyObject* self, PyObject* args){

    PyArg_ParseTuple(args, "fff", &range_res, &azimuth_res, &elev_res);
    return  Py_BuildValue("i",0);

}
/*
static PyObject* radial(PyObject* self, PyObject* args){
    float meioGrauEmRad = 0.008726646,rt = 8500000;//raio aparente da terra
    int heightkm=16;
    int height=200,width=400,azi;
    int i,j,k,flag;
    const char *pathnc,*moment;
    PyObject* obj;
    PyArrayObject * py_palette;
    int * palette;
    float max,min;
//    clock_t start = clock(),end ;
//    printf("start one cut\n");
printf("radial cut is out of date, it is not safe to use it");
    PyArg_ParseTuple(args, "s(ii)is(Off)", &pathnc, &width, &height, &azi,&moment,&obj,&max,&min);


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj,NPY_INT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    
    

    
    
    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    
    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int range_index[max_range_index];

    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;

    
    // get moments and attributes
    short *data=(short*)malloc(sizeof(short)*n_points_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        nc_get_var_short(ncid,data_id, data);
    
    short fill_value;
    nc_get_att_short(ncid, data_id, "_FillValue", &fill_value);
    float scale;
    nc_get_att_float(ncid, data_id, "scale_factor",&scale);
    float offset;
    nc_get_att_float(ncid, data_id, "add_offset", &offset);
    



    nc_close(ncid);
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;


    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh-altitude;
    }
    float ds = radarRange/nCols,SIGMA;
    for(j=0;j<nCols;j++){
        s[j] = (0.5 + j) * ds;
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;
        
    }
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//   start=end;
    
    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    short DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
            H1 = h[i]-H0[j];
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R);
                        
            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
            
        }
    }
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;



    //lets found the index (time) of the azimuth given every_sweep
    int azi_elev[sweep_dim.len];//receve sweep number and giver ray number with needed azimuth
    float near=100000000;
    
    for(i=0;i<sweep_dim.len;i++){
        near=100000000;
        for(j=sweep_start_ray_index[i];j<sweep_end_ray_index[i]+1;j++){
            if(near>(azi-azimuth[j])*(azi-azimuth[j])){
                near=(azi-azimuth[j])*(azi-azimuth[j]);
                azi_elev[i]=j;
            }
        }
        
    }

    float lower,higher;
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    for(j=0;j<nCols;j++){
    
    
        k=0;
        flag=0;
        lower=elevation[sweep_start_ray_index[k]]*HALF_PI/90-meioGrauEmRad;
        higher=elevation[sweep_start_ray_index[k]]*HALF_PI/90+meioGrauEmRad;
        for(i=nRows-1;i>-1;i--){
        
        
            //acha a elevacao nos dados e Alimenta a matriz do corte com os dados 
            if(flag){ 
                if(ELEVS[i][j]<higher){ 
                    if(Rint[i][j]>=ray_n_gates[azi_elev[k]])Rint[i][j]=ray_n_gates[azi_elev[k]]-1;
                    DATA[i][j]=data[ray_start_index[azi_elev[k]]+Rint[i][j]];
                    }
                else{ 
                    flag=0;
                    k++;
                    if(k==sweep_dim.len){
                    higher=10000;lower=10000;
                    }
                    else{
                        lower=higher;
                        higher=elevation[sweep_start_ray_index[k]]*HALF_PI/90+meioGrauEmRad;
                    }
                    i++;
                }
            }
            
            else{  
                if(ELEVS[i][j]<lower)
                    DATA[i][j]=fill_value+10;
                else{
                    flag=1;
                    if(Rint[i][j]>=ray_n_gates[azi_elev[k]]) Rint[i][j]=ray_n_gates[azi_elev[k]]-1;
                    DATA[i][j]=data[ray_start_index[azi_elev[k]]+Rint[i][j]];
                   
                }
            
            }
            
            
        }
    }
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    //map values to colors
    int pixelarray[nCols][nRows];
    float step=(max-min)/ncolors;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        if(DATA[i][j]*scale+offset>min && DATA[i][j]*scale+offset<max){
            k=floor((DATA[i][j]*scale+offset-min)/step);
            pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }
    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);
    free(data);
    
    
    
    
    
    return array;
}






static PyObject* vertical2(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k,flag;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    
    PyArg_ParseTuple(args, "s(ii)(ff)(ff)s(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&moment,&obj1,&obj2);

    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to netcdf data
    //double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_elev_index=floor(90/elev_res)+1;
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int elev_index[max_elev_index];
    int azimuth_index[sweep_dim.len][max_azimuth_index];

    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[i][k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    float lower,higher;
    k=0;
    flag=0;
    lower=elevation[sweep_start_ray_index[k]]-0.5;
    higher=elevation[sweep_start_ray_index[k]]+0.5;
    for(i=0;i<max_elev_index;i++){
   
   
        //acha a elevacao 
        if(flag){ 
            if(i*elev_res<higher){ 
                elev_index[i]=k;
                }
            else{ 
                flag=0;
                k++;
                if(k==sweep_dim.len){
                higher=10000;lower=10000;
                }
                else{
                    lower=higher;
                    higher=elevation[sweep_start_ray_index[k]]+0.5;
                }
                i--;
            }
        }
        else{  
            if(i*elev_res<lower)
                elev_index[i]=-1;
            else{
                flag=1;
                i--;
            }
        
        }

        
    }
    k=0;flag=360;
    int azi1,azi2;
    for(i=0;i<sweep_dim.len;i++){
        for(k=0;k<sweep_end_ray_index[i]-sweep_start_ray_index[i]+1;k++){
            azi1=floor((azimuth[sweep_start_ray_index[i]+k]-0.5)/azimuth_res);
            azi2=ceil((azimuth[sweep_start_ray_index[i]+k]+1.5)/azimuth_res);
//            if(azi1<0) azi1=0;
//            if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
            for(j=azi1;j<azi2;j++) azimuth_index[i][j%max_azimuth_index]=sweep_start_ray_index[i]+k;
        }
    }

    // get moments and attributes
    short *data=(short*)malloc(sizeof(short)*n_points_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        nc_get_var_short(ncid,data_id, data);
    
    short fill_value;
    nc_get_att_short(ncid, data_id, "_FillValue", &fill_value);
    float scale;
    nc_get_att_float(ncid, data_id, "scale_factor",&scale);
    float offset;
    nc_get_att_float(ncid, data_id, "add_offset", &offset);
    
    //radarRange=range[range_dim.len-1]; //maximal range in meters


    nc_close(ncid);

    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    int azi[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh -altitude;
    }
    float SIGMA, t,a;
    for(j=0;j<nCols;j++){
        t=(j*1.0/(nCols-1));
        s[j] = sqrt(pow((1-t)*X1+t*X2,2)+pow((1-t)*Y1+t*Y2,2));
        a=atan2((1-t)*X1+t*X2,(1-t)*Y1+t*Y2)*90/HALF_PI;
        if(a<0) a=a+360;
        azi[j]=floor(a/azimuth_res);//index for azimuth_index
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;

    }

    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    short DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
 
            H1 = h[i]-H0[j];
            
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R)*90/HALF_PI;//in degrees

            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
        }
    }


    int E;
    for(i=0;i<nRows;i++){
            //printf("%f %i %f %i\n",ELEVS[i][0],E,azi[0]*azimuth_res,Rint[i][0]);
        for(j=0;j<nCols;j++){
            if(ELEVS[i][j]<0) E=-1;
            else E=elev_index[(int)floor(ELEVS[i][j]/elev_res)];
            if(E<0 || azimuth_index[E][azi[j]]<0 || Rint[i][j]<0 || Rint[i][j]>=ray_n_gates[azimuth_index[E][azi[j]]] ){DATA[i][j]=fill_value+10;}
            else {
                DATA[i][j]=data[ray_start_index[azimuth_index[E][azi[j]]]+Rint[i][j]];}
        }
    }
    
    

    //map values to colors
    int pixelarray[nCols][nRows];
    float value;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        value=DATA[i][j]*scale+offset;
        if(value>step[0] && value<step[ncolors]){
           k=0;
           while(value>step[k+1]) k++;
           pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }

    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);
    free(data);
  
    

    return array;
}


static PyObject* vertical1(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k,flag;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    
    PyArg_ParseTuple(args, "s(ii)(ff)(ff)s(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&moment,&obj1,&obj2);

    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to netcdf data
    //double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
//    int ray_n_gates_id;
//    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
//        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
//        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
//    int ray_start_index_id;
//    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
//       nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
//        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_elev_index=floor(90/elev_res)+1;
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int elev_index[max_elev_index];
    int azimuth_index[sweep_dim.len][max_azimuth_index];

    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[i][k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    float lower,higher;
    k=0;
    flag=0;
    lower=elevation[sweep_start_ray_index[k]]-0.5;
    higher=elevation[sweep_start_ray_index[k]]+0.5;
    for(i=0;i<max_elev_index;i++){
   
   
        //acha a elevacao 
        if(flag){ 
            if(i*elev_res<higher){ 
                elev_index[i]=k;
                }
            else{ 
                flag=0;
                k++;
                if(k==sweep_dim.len){
                higher=10000;lower=10000;
                }
                else{
                    lower=higher;
                    higher=elevation[sweep_start_ray_index[k]]+0.5;
                }
                i--;
            }
        }
        else{  
            if(i*elev_res<lower)
                elev_index[i]=-1;
            else{
                flag=1;
                i--;
            }
        
        }

        
    }
    k=0;flag=360;
    int azi1,azi2;
    for(i=0;i<sweep_dim.len;i++){
        for(k=0;k<sweep_end_ray_index[i]-sweep_start_ray_index[i]+1;k++){
            azi1=floor((azimuth[sweep_start_ray_index[i]+k]-0.5)/azimuth_res);
            azi2=ceil((azimuth[sweep_start_ray_index[i]+k]+1.5)/azimuth_res);
//            if(azi1<0) azi1=0;
//            if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
            for(j=azi1;j<azi2;j++) azimuth_index[i][j%max_azimuth_index]=sweep_start_ray_index[i]+k;
        }
    }

    // get moments and attributes
    short *data=(short*)malloc(sizeof(short)*n_points_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        nc_get_var_short(ncid,data_id, data);
    
    short fill_value;
    nc_get_att_short(ncid, data_id, "_FillValue", &fill_value);
    float scale;
    nc_get_att_float(ncid, data_id, "scale_factor",&scale);
    float offset;
    nc_get_att_float(ncid, data_id, "add_offset", &offset);
    
    //radarRange=range[range_dim.len-1]; //maximal range in meters


    nc_close(ncid);

    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    int azi[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh -altitude;
    }
    float SIGMA, t,a;
    for(j=0;j<nCols;j++){
        t=(j*1.0/(nCols-1));
        s[j] = sqrt(pow((1-t)*X1+t*X2,2)+pow((1-t)*Y1+t*Y2,2));
        a=atan2((1-t)*X1+t*X2,(1-t)*Y1+t*Y2)*90/HALF_PI;
        if(a<0) a=a+360;
        azi[j]=floor(a/azimuth_res);//index for azimuth_index
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;

    }

    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    short DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
 
            H1 = h[i]-H0[j];
            
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R)*90/HALF_PI;//in degrees

            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
        }
    }


    int E;
    for(i=0;i<nRows;i++){
            //printf("%f %i %f %i\n",ELEVS[i][0],E,azi[0]*azimuth_res,Rint[i][0]);
        for(j=0;j<nCols;j++){
            if(ELEVS[i][j]<0) E=-1;
            else E=elev_index[(int)floor(ELEVS[i][j]/elev_res)];
            if(E<0 || azimuth_index[E][azi[j]]<0 || Rint[i][j]<0 || Rint[i][j]>=range_dim.len){DATA[i][j]=fill_value+10;}
            else {
                DATA[i][j]=data[range_dim.len*(azimuth_index[E][azi[j]])+Rint[i][j]];}
        }
    }
    
    

    //map values to colors
    int pixelarray[nCols][nRows];
    float  value;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        value=DATA[i][j]*scale+offset;
        if(value>step[0] && value<step[ncolors]){
           k=0;
           while(value>step[k+1]) k++;
           pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }

    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);

    free(data);
  
    

    return array;
}


static PyArrayObject * PPI2(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    int elev;
    
    PyArg_ParseTuple(args, "s(ii)(ii)(ii)is(OO)", &pathnc, &height, &width,&left,&top,&right,&bottom, &elev,&moment,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);



    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int azimuth_index[max_azimuth_index];

    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    k=0;
    int azi1,azi2;
    for(k=sweep_start_ray_index[elev];k<sweep_end_ray_index[elev]+1;k++){
        azi1=floor((azimuth[k]-0.5)/azimuth_res);
        azi2=ceil((azimuth[k]+1.5)/azimuth_res);
//        if(azi1<0) azi1=0;
//       if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
        for(j=azi1;j<azi2;j++){ azimuth_index[j%max_azimuth_index]=k;
        }
    }


    // get moments and attributes
    short *data=(short*)malloc(sizeof(short)*n_points_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        nc_get_var_short(ncid,data_id, data);
    
    short fill_value;
    nc_get_att_short(ncid, data_id, "_FillValue", &fill_value);
    float scale;
    nc_get_att_float(ncid, data_id, "scale_factor",&scale);
    float offset;
    nc_get_att_float(ncid, data_id, "add_offset", &offset);
    
    radarRange=500000; //maximal range in meters

    nc_close(ncid);

    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    float s,R0,R,BETHA,ALPHA;
    int azi;
    int Rint,Ri;
    short DATA;
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float SIGMA,a,x,y;
    
    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;
    
    for(j=left;j<right;j++){
        for(i=top;i<bottom;i++){
            x=-radarRange+j*2*radarRange/height;
            y=radarRange-i*2*radarRange/width;

            s = sqrt(x*x+y*y);
            a=atan2(x,y)*90/HALF_PI;
            if(a<0) a=a+360;
            azi=floor(a/azimuth_res);//index for azimuth_index
            SIGMA = s/rt;
            R0 = rt*tan(SIGMA);
            BETHA =HALF_PI+ SIGMA;
            ALPHA =HALF_PI- SIGMA-elevation[azimuth_index[azi]]*HALF_PI/90;
            R = R0 * sin(BETHA) / sin(ALPHA); 
            Ri=floor(R/range_res);
            if(R>=range[ray_n_gates[azimuth_index[azi]]-1]) {
               DATA=fill_value+10;
            }
            else {
                Rint =range_index[Ri];
                DATA=data[ray_start_index[azimuth_index[azi]]+Rint];
            }
             //map values to colors
            value=DATA*scale+offset;
            if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[(j-left)*nCols+(i-top)]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else if(DATA==fill_value+10)
                pixelarray[(j-left)*nCols+(i-top)]=1*256*256+1*256+1;
            else pixelarray[(j-left)*nCols+(i-top)]=0;
        }
    }


    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);
    free(data);
    
    

    return array;
}
*/


static PyArrayObject * PPI1(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    int moment_index;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    int elev;
    hid_t file_id;
    
    PyArg_ParseTuple(args, "i(ii)(ii)(ii)ii(OO)", &file_id, &height, &width,&left,&top,&right,&bottom, &elev,&moment_index,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to hdf data

	hid_t attr_id, dataset_id;  
	hid_t dataspace;
	hid_t attr_type, datatype;
	
    unsigned char* data_char;
    unsigned short* data_short; 

    //get hdf data
//    printf("%s\n",pathhdf);
//	file_id = H5Fopen(pathhdf, H5F_ACC_RDONLY, H5P_DEFAULT );
    //get hdf dimension

	int range_len, time_len;
	char temp_string[100];
	sprintf(temp_string, "/scan%d/moment_%d",elev, moment_index);

	dataset_id = H5Dopen1(file_id, temp_string);


	dataspace = H5Dget_space(dataset_id);
    hsize_t DIMS2[2];
    H5Sget_simple_extent_dims(dataspace, DIMS2, NULL);
    time_len = DIMS2[0];
    range_len = DIMS2[1];

    //get hdf variables
    
    double maxRange;
	sprintf(temp_string, "/scan%d/how",elev);
	attr_id = H5Aopen_by_name( file_id, temp_string, "range" , H5P_DEFAULT, H5P_DEFAULT );
	attr_type  = H5Aget_type(attr_id);
	H5Aread(attr_id, attr_type, &maxRange);

	sprintf(temp_string, "/scan%d/ray_header",elev);
	hid_t header = H5Dopen1(file_id, temp_string);
	float *ray_azim0 = (float *) malloc(time_len *  sizeof(float));
	float *ray_azim1 = (float *) malloc(time_len *  sizeof(float));
	hid_t azim0 = H5Tcreate(H5T_COMPOUND, sizeof(float));
	hid_t azim1 = H5Tcreate(H5T_COMPOUND, sizeof(float));
	H5Tinsert(azim0, "azimuth_start", 0, H5T_NATIVE_FLOAT);
	H5Tinsert(azim1, "azimuth_stop", 0, H5T_NATIVE_FLOAT);
	H5Dread(header, azim0, H5S_ALL, H5S_ALL, H5P_DEFAULT, ray_azim0);
	H5Dread(header, azim1, H5S_ALL, H5S_ALL, H5P_DEFAULT, ray_azim1);
	H5Tclose(azim0);
	H5Tclose(azim1);

	 
    double sweepElevation;
	sprintf(temp_string, "/scan%d/how",elev);    
	attr_id = H5Aopen_by_name( file_id, temp_string, "elevation" , H5P_DEFAULT, H5P_DEFAULT );
    attr_type  = H5Aget_type(attr_id);
    H5Aread(attr_id, attr_type, &sweepElevation);



    int max_azimuth_index=ceil(360/azimuth_res);
    int azimuth_index[max_azimuth_index];


    //fill with -1
    for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[k]=-1;

    // fill index arrays

    k=0;
    int azi1,azi2;
    for(k=0;k<time_len;k++){
        azi1=floor((ray_azim0[k]-0.5)/azimuth_res);
        azi2=ceil((ray_azim1[k]+0.5)/azimuth_res);
//        if(azi1<0) azi1=0;
//        if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
       // printf("\n %f, %i,  %i\n",azimuth[k],azi1,azi2);
        for(j=azi1;j<azi2;j++){ azimuth_index[j%max_azimuth_index]=k;//printf("%i ",k);printf("haqha");
        }
        //printf("\nbibi");
    }


    // get moments and attributes
    
	datatype = H5Dget_type(dataset_id);    
	size_t datasize = H5Tget_size (datatype);

	if (datasize == 1) data_char= (unsigned char *) malloc(sizeof(char)*time_len*range_len ); // CHAR      
	else if (datasize == 2) data_short= (unsigned short *) malloc(sizeof(short)*time_len*range_len ); // INTEGER/SHORT 
    else {fprintf(stderr,"anderson didn't recognise data size\n%i\n",datasize); exit(-1);}

    if (datasize == 1) { // CHAR                           
        H5Dread( dataset_id, H5Tget_native_type(datatype, H5T_DIR_DEFAULT), H5S_ALL, H5S_ALL, H5P_DEFAULT, data_char );          
    } else if (datasize == 2) { // INTEGER/SHORT               
        H5Dread( dataset_id, H5Tget_native_type(datatype, H5T_DIR_DEFAULT), H5S_ALL, H5S_ALL, H5P_DEFAULT, data_short ); 
    }


//    short fill_value=0;

    double range_min;float range_min_float;
    attr_id = H5Aopen_name(dataset_id, "dyn_range_min"); 
    int storage_size=H5Aget_storage_size(  attr_id);
    attr_type  = H5Aget_type(attr_id);
    if(storage_size==8)
	    H5Aread(attr_id, attr_type, &range_min);
	else
		H5Aread(attr_id, attr_type, &range_min_float);    

    double range_max=3;float range_max_float;
    attr_id = H5Aopen_name(dataset_id, "dyn_range_max"); 
    attr_type  = H5Aget_type(attr_id);
    if(storage_size==8)
	    H5Aread(attr_id, attr_type, &range_max);
	else
		H5Aread(attr_id, attr_type, &range_max_float);

	float scale,offset;
	if(storage_size==8){
		if (datasize == 1) scale =(range_max - range_min)  /254.;
		else if (datasize == 2) scale =(range_max - range_min)  /(pow(2,16)-2.);
		else scale = 1;
		offset=range_min-scale;
	}
	else{
		if (datasize == 1) scale =(range_max_float - range_min_float)  /254.;
		else if (datasize == 2) scale =(range_max_float - range_min_float)  /(pow(2,16)-2.);
		else scale = 1; 	
		offset=range_min_float-scale;
	}


    double radarRange=500000; //maximal range in meters
	
//    H5Fclose(file_id);

    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    float s,R0,R,BETHA,ALPHA;
    int azi;
    int Rint;
    int DATA;
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float SIGMA,a,x,y;

    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;

printf("data size %i\n",datasize);
    for(j=left;j<right;j++){
        for(i=top;i<bottom;i++){

            x=-radarRange+j*2*radarRange/height;
            y=radarRange-i*2*radarRange/width;

            s = sqrt(x*x+y*y);
            a=atan2(x,y)*90/HALF_PI;
            if(a<0) a=a+360;
            azi=floor(a/azimuth_res);//index for azimuth_index
            SIGMA = s/rt;
            R0 = rt*tan(SIGMA);
            BETHA =HALF_PI+ SIGMA;
            ALPHA =HALF_PI- SIGMA-sweepElevation*HALF_PI/90;

            R = R0 * sin(BETHA) / sin(ALPHA); 

            if(R>=maxRange) {
               DATA=999999;
            }
            else if(azimuth_index[azi]<0) DATA=999999;
            else {
                Rint =floor(range_len*R/maxRange);
                if (datasize == 1) DATA=data_char[azimuth_index[azi]*range_len+Rint];
                else if (datasize == 2)  DATA=data_short[azimuth_index[azi]*range_len+Rint];
                else DATA=999999;
                
            }
            
             //map values to colors
            value=DATA*scale+offset;

            if (DATA==0) value=-9999;
            if(DATA==999999)
                pixelarray[(j-left)*nCols+(i-top)]=1*256*256+1*256+1;
            else if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[(j-left)*nCols+(i-top)]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else pixelarray[(j-left)*nCols+(i-top)]=0;

        }
    }
	printf("ncolor %i %f %f\n",ncolors,step[0],step[ncolors]);

    free(ray_azim0);
    free(ray_azim1);
    
    if (datasize == 1) free(data_char);
	else if (datasize == 2) free(data_short);
 
    return array;
}






/*  define functions in module */
static PyMethodDef Hdf5Methods[] =
{
//     {"radial", radial, METH_VARARGS, "make radial cut"},
//     {"vertical1", vertical1, METH_VARARGS, "make vertical cut for hdf5 format gamic"},
//     {"vertical2", vertical2, METH_VARARGS, "make vertical cut for netcdf format 1.2"},
     {"PPI1", PPI1, METH_VARARGS, "make PPI for hdf5 format gamic"},
//     {"PPI2", PPI2, METH_VARARGS, "make PPI for netcdf format 1.2"},
     {"set_resolution",set_resolution,METH_VARARGS,"Set Resolution for (range,azimuth,elevation) in order to make the images"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC

inithdf5_module(void)
{
     (void) Py_InitModule("hdf5_module", Hdf5Methods);
      /* IMPORTANT: this must be called */
     import_array();
}
