import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
import h5py
import pygame
import GeraImages
# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import PPI_hdf as PPI
from ToolsPanel import MainMenu
import Line
import Corte
import Colorbar
hdf_path=[#"/simepar/projetos/opera/Radar_VISU_POL/txs256.hdf",
#"/simepar/radarvols/decea/morrodaigreja/2014/05/26/2014-05-26--01:40:03,00.hdf",
#"/simepar/radarvols/dinacpy/2014/04/11/2014-04-11--08:54:07,00.mvol"
"/simepar/radar2/decea/cgufrog/250km_15elev_600hz/CGU-250--2014-07-23--19-10-23.mvol",
"/simepar/radar2/decea/cgufrog/400km_3elev_350hz/CG--2014-06-16--00-06-37.mvol",
"/simepar/radar2/decea/ga1frog/250km_15elev_600hz/GA1-250--2014-07-03--00-00-26.mvol",
"/simepar/radar2/decea/ga1frog/400km_3elev_350hz/GA1-400--2014-07-03--00-06-46.mvol",
"/simepar/radar2/decea/gafrog/250km_15elev_600hz/GA--2014-06-16--00-00-26.mvol",
"/simepar/radar2/decea/gafrog/400km_3elev_350hz/GA--2014-06-16--00-06-45.mvol",
"/simepar/radar2/decea/mdifrog/250km_15elev_600hz/MD--2014-06-16--00-00-03.mvol",
"/simepar/radar2/decea/mdifrog/400km_3elev_350hz/MD--2014-06-16--00-05-50.mvol",
"/simepar/radar2/decea/pcofrog/250km_15elev_600hz/PC--2014-07-01--00-00-26.mvol",
"/simepar/radar2/decea/pcofrog/400km_3elev_350hz/PC--2014-06-16--23-06-32.mvol",
"/simepar/radar2/decea/srofrog/250km_15elev_600hz/SR--2014-06-16--00-00-26.mvol",
"/simepar/radar2/decea/srofrog/400km_3elev_350hz/SR--2014-06-16--00-06-49.mvol",
"/simepar/radar2/decea/stifrog/250km_15elev_600hz/ST--2014-07-01--00-00-23.mvol",
"/simepar/radar2/decea/stifrog/400km_3elev_350hz/ST--2014-06-16--00-06-41.mvol",]

hdf_path=[
"/simepar/projetos/opera/radares_hdf5/cgufrog/CGU-250--2014-07-23--19-10-23.mvol",
"/simepar/projetos/opera/radares_hdf5/cgufrog/CGU-400--2014-07-23--19-06-37.mvol",
"/simepar/projetos/opera/radares_hdf5/ga1frog/GA1-250--2014-07-21--00-20-26.mvol",
"/simepar/projetos/opera/radares_hdf5/ga1frog/GA1-400--2014-07-21--00-16-47.mvol",
"/simepar/projetos/opera/radares_hdf5/mdifrog/MDI-250--2014-07-24--02-40-03.mvol",
"/simepar/projetos/opera/radares_hdf5/mdifrog/MDI-400--2014-07-24--02-35-50.mvol",
"/simepar/projetos/opera/radares_hdf5/pcofrog/PCO-250--2014-07-18--22-30-26.mvol",
"/simepar/projetos/opera/radares_hdf5/pcofrog/PCO-400--2014-07-18--22-26-47.mvol",
"/simepar/projetos/opera/radares_hdf5/srofrog/SRO-250--2014-07-18--11-30-22.mvol",
"/simepar/projetos/opera/radares_hdf5/srofrog/SRO-400--2014-07-18--11-26-30.mvol",
"/simepar/projetos/opera/radares_hdf5/stifrog/STI-250--2014-07-22--21-10-23.mvol",
"/simepar/projetos/opera/radares_hdf5/stifrog/STI-400--2014-07-22--21-06-36.mvol",]

new_hdf=[h5py.File(hdf_path[0],'r')]
print "end open nercdf"
Kern.start_up((1200,800))
ppi_rect=pygame.Rect(50,50,600,600)
moment=["Z"]
threshold=[0]
ppi=PPI.PPI(ppi_rect,Kern.jobs,GeraImages,(new_hdf,[0],moment,threshold),name="PPI")


Kern.add_module(ppi,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(50,100,55,500),Kern.jobs,(moment,threshold,ppi.value,ppi.dirty),name="Colorbar"),2)

Kern.add_module(MainMenu(pygame.Rect(600,0,30,30),Kern.jobs,(hdf_path,new_hdf),file_type="HDF",name="MainMenu"),2)

line=Line.Line(ppi_rect,Kern.jobs,(new_hdf,ppi.scaledDataSprite,ppi.dirty),name="Line",)
Kern.add_module(line,0)


cut_rect=pygame.Rect(700,100,450,300)

#Kern.add_module(Corte.VerticalCut(cut_rect,Kern.jobs,GeraImages(new_hdf,moment,threshold,line.P,line.Proj),ppi.convertion_to_radar,name="Cut"),0)

Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")
