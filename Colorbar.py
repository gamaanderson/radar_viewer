# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 13:58:39 2013

@author: tiagoburiol@gmail.com
"""
#Import Modules
import os, pygame
from pygame.locals import *
import Text
from Sprites import *
from palette import palette
import bisect


"""COLORBAR"""
class Colorbar():#Subclass of the General Sprite (above)
    width  = None
    height = None       
    pos    = None
    vmax   = None
    cores  = None
    surf   = None
    textFont   = None
    fontSource = None
    fonteSize  = None
    #ncores = None

    up_to_date=True # lets mantain this as a flag if the Sprite has changed
    moved=False # lets mantain this as a flag if the Sprite was moved
    who_created_me= "" # as sprites may be created every-where, this is impostant to help locating the source of a sprite, this is os reponsability of the develorpers
    mouse=pygame.image.load("images/scroll.bmp")

    """ inicia uma barra de cores"""
#    def __init__(self,moment,threshold, rect=None, image=None, pos=(0,0),who_created_me="don't now"):
    def __init__(self,rect,jobs,(moment,threshold,value,external_dirty),name="Colorbar"):
        self.rect=rect
        self.image=pygame.display.get_surface().subsurface(self.rect)
        self.jobs=jobs
        self.name=name
        self.dirty=[2]
        self.threshold_button=pygame.image.load("images/button.bmp")
        self.threshold_button= pygame.transform.smoothscale(self.threshold_button, (self.rect.width-30, 8))
        self.value_marker=pygame.Surface((10,10))
        self.value_marker.fill((255,0,255))
        self.value_marker.set_colorkey((255,0,255))
        text=u'\u25ba'
        font_regular=pygame.font.Font("images/Regular.ttf", 14)
        font_outline=pygame.font.Font("images/Outline.ttf", 14)
        Text.writePixel(self.value_marker,font_regular,font_outline,(-3,-4),(255,0,0),(0,0,0),text)
        self.new_moment=moment
        self.moment=None
        self.new_threshold=threshold
        self.threshold=threshold[0]
        self.new_value=value
        self.value=None
        self.external_dirty=external_dirty
        
        jobs["%s:THRESHOLD_UP"%name]=self.__MOVE_THRESHOLD
        jobs["%s:THRESHOLD_DOWN"%name]=self.__MOVE_THRESHOLD
        
        self.__setSize()

    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf=self.rect.collidepoint(pos[0],pos[1])


        if event.type==pygame.MOUSEBUTTONDOWN:

            # RODINHA GIROU PARA FRENTE
            if event.button==4 and over_surf:
                return "%s:THRESHOLD_UP"%self.name

            # RODINHA GIROU PARA TRAS
            if event.button==5 and over_surf:
                return "%s:THRESHOLD_DOWN"%self.name

        return "NO_JOB"



    def __setSize(self):
        # altura e do retangulozinho colorido para cada valor
        h = self.rect.height/(23)
        """ TODO: definir diferentes tamanhos de fontes """

        if h<13:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 10)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 10)
        elif h<15:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 10)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 10)
        elif h<20:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        else:
            self.textFont  = pygame.font.Font("images/Regular.ttf", 14)
            self.textFontOutline  = pygame.font.Font("images/Outline.ttf", 14)
        
    def __make(self):
        import Text
        self.bar=self.image.copy()
        self.bar.fill((255,0,255))
        self.bar.set_colorkey((255,0,255))
        h = float(self.rect.height-5)/(len(palette[self.moment][0]))
        width=self.rect.width-45
        #print h
        #font = pygame.font.SysFont("ubuntumono", h)
        # desenha as cores
        length=len(palette[self.moment][0])
        for i in range(length):
            level = pygame.draw.rect(self.bar, palette[self.moment][0][len(palette[self.moment][0])-1-i], (10, int(i*h), width, int((i+1)*h)-int(i*h)), 0)
            if abs(palette[self.moment][1][-1])>10: t = "%.0f"%((palette[self.moment][1][length-i]+palette[self.moment][1][length-1-i])/2)
            elif abs(palette[self.moment][1][-1])>1: t = "%.1f"%((palette[self.moment][1][length-i]+palette[self.moment][1][length-1-i])/2)
            else: t = "%.2f"%((palette[self.moment][1][length-i]+palette[self.moment][1][length-1-i])/2)
            pos = (10+width + 2 , int(i*h))
            color = (255,255,255)
            
            Text.writePixel(self.bar,self.textFont,self.textFontOutline,pos,color,(0,0,0),t)
        if self.dirty[0]==0: self.dirty[0]=1
#        self.bar.fill((0,255,255),pygame.Rect(0,0,10,self.rect.height))



    """MOVE_THRESHOLD"""
    def __MOVE_THRESHOLD(self,job):
        if job=="%s:THRESHOLD_DOWN"%self.name: delta=-1
        elif job=="%s:THRESHOLD_UP"%self.name: delta=1
        if self.threshold+delta>=0 and self.threshold+delta<len(palette[self.moment][0]):
            self.new_threshold[0]=self.threshold+delta
        self.external_dirty[0]=1

    def loop(self):
        if self.moment!=self.new_moment[0]:
            self.moment=self.new_moment[0]
            self.__make()
            self.new_threshold[0]=0
            self.threshold=self.new_threshold[0]
                
        if self.threshold!=self.new_threshold[0]:
            self.threshold=self.new_threshold[0]
        
        if self.value!=self.new_value[0]:
            self.value=self.new_value[0]
            self.external_dirty[0]=1
            
        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__update()





    def __update(self):
        h=float(self.rect.height-5)/(len(palette[self.moment][0]))
        self.image.blit(self.bar,(0,0))
        threshold_pos=int((len(palette[self.moment][0])-self.threshold)* h)-4
        self.image.blit(self.threshold_button,(0,threshold_pos))
        pos=bisect.bisect_left(palette[self.moment][1],self.value)-1
        if pos>=0 and pos<len(palette[self.moment][0]):        
            self.image.blit(self.value_marker,(0,int((len(palette[self.moment][0])-pos-1)* h)))




