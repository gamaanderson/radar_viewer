
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

short *ordena(short *vetor, int cont) {
    int i, j;
    short x;
    
   for (j = 1;j < cont; j++) {
      x = vetor[j];
      i = j - 1; 
      while( (i >= 0) && (vetor[i] > x) ) {
             vetor[i+1] = vetor[i];
             i = i - 1;
      }
        vetor[i+1] = x;
   }

    return(vetor);    
}

float *ordena_float(float *vetor, int cont) {
    int i, j;
    float x;
    
   for (j = 1;j < cont; j++) {
      x = vetor[j];
      i = j - 1; 
      while( (i >= 0) && (vetor[i] > x) ) {
             vetor[i+1] = vetor[i];
             i = i - 1;
      }
        vetor[i+1] = x;
   }

    return(vetor);    
}


short **malloc_short_2_matrix(int height, int width) {

    int i,j;
    
    short **M=malloc(sizeof(short*)*height+sizeof(short)*width*height);
    for(i=0;i<height;i++) 
        {
        M[i]=(short*)(((void*)M)+sizeof(short*)*height+sizeof(short)*width*i);
        for(j=0;j<width;j++) M[i][j]=0;
        }


    return (M);
}

float **malloc_float_2_matrix(int height, int width) {

    int i,j;
    
    float **M=malloc(sizeof(float*)*height+sizeof(float)*width*height);
    for(i=0;i<height;i++) 
        {
        M[i]=(float*)(((void*)M)+sizeof(float*)*height+sizeof(float)*width*i);
        for(j=0;j<width;j++) M[i][j]=0;
        }


    return (M);
}

void median_filter_float (float *M, int heigth,int width, int delta){

    int i, j, x, y;
    int cont, aux, volta, num;

    float **N=malloc_float_2_matrix(heigth,width);

    int max = pow(2*delta+1, 2);   
    float *vetor;
    vetor = (float*) malloc(max*sizeof(float));

    for (i = 0; i < heigth; i++)
        for (j = 0; j < width; j++) {
			cont = 0;
            
            volta = 0; num = 0;
            for (x = i-delta; x <= i+delta; x++){
                for (y = j-delta; y <= j+delta; y++) {
                    if(x >= 0 && x <heigth && y >= 0 && y < width) {                  
                            vetor[cont] = M[x*width+y];
                            cont++;                               
                    }
                }
            }          
            vetor = ordena_float(vetor, cont); 
            aux = (int) cont / 2;
            if (cont % 2 == 1) // impar
                  N[i][j] = vetor[aux];
            else
                N[i][j] = (vetor[aux-1] + vetor[aux])/2;      

        }
    memcpy(M,&N[0][0],sizeof(float)*heigth*width);
    free(N);


}

void median_filter_short (short *M, int heigth,int width, int delta) {

    int i, j, x, y;
    int cont, aux, volta, num;

    short **N=malloc_short_2_matrix(heigth,width);

    int max = pow(2*delta+1, 2);   
    short *vetor;
    vetor = (short*) malloc(max*sizeof(short));

    for (i = 0; i < heigth; i++)
        for (j = 0; j < width; j++) {
			cont = 0;
            
            volta = 0; num = 0;
            for (x = i-delta; x <= i+delta; x++){
                for (y = j-delta; y <= j+delta; y++) {
                    if(x >= 0 && x <heigth && y >= 0 && y < width) {                  
                            vetor[cont] = M[x*width+y];
                            cont++;                               
                    }
                }
            }          
            vetor = ordena(vetor, cont); 
            aux = (int) cont / 2;
            if (cont % 2 == 1) // impar
                  N[i][j] = vetor[aux];
            else
                N[i][j] = (vetor[aux-1] + vetor[aux])/2;      

        }
    memcpy(M,&N[0][0],sizeof(short)*heigth*width);
    free(N);
}
