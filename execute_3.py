import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
from scipy.io import netcdf 
import pygame
# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import PPI
from ToolsPanel import MainMenu
import Line
import Corte
netcdf_path=[os.getcwd()+"/"+"dados/TXS131003180057.RAW03EZ.nc"]
"../../netcdf/9505CAS-20131014-200516-PPIVol.nc"
netcdf_path=[ 
#"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125651-PPI-ZH.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125718-PPIVol-02.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125746-PPIVol-03.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125820-PPIVol-04.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125853-PPIVol-05.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125927-PPIVol-06.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-125957-PPIVol-07.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130030-PPIVol-08.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130103-PPIVol-09.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130137-PPIVol-10.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130210-PPIVol-11.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130244-PPIVol-12.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130317-PPIVol-13.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130349-PPIVol-14.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130424-PPIVol-15.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130459-PPIVol-16.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130538-PPIVol-17.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130609-PPIVol-18.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130636-PPIVol-19.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130703-PPIVol-20.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130728-PPIVol-21.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130754-PPIVol-22.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130825-PPIVol-01.nc",
#"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130825-PPI-ZH.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130852-PPIVol-02.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130920-PPIVol-03.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-130953-PPIVol-04.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131027-PPIVol-05.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131100-PPIVol-06.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131131-PPIVol-07.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131203-PPIVol-08.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131237-PPIVol-09.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131310-PPIVol-10.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131343-PPIVol-11.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131417-PPIVol-12.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131450-PPIVol-13.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131507-PPIVol-14.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131542-PPIVol-15.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131616-PPIVol-16.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131701-PPIVol-17.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131728-PPIVol-18.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131755-PPIVol-19.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131822-PPIVol-20.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131846-PPIVol-21.nc",
"/simepar/projetos/opera/Radar_Cascavel/teste2/9505CAS-20140211-131913-PPIVol-22.nc"
]
netcdf_path=[
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140216/9505CAS-20140216-234057-PPIVol.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140216/9505CAS-20140216-235646-PPIVol.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140217/9505CAS-20140217-092251-PPIVol.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140217/9505CAS-20140217-195349-PPIVol.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140218/9505CAS-20140218-004915-PPIVol.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140218/9505CAS-20140218-103834-PPIVol.nc",
]
netcdf_path=[
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-072827-PPIVol-01.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-072857-PPIVol-02.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-072925-PPIVol-03.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-072955-PPIVol-04.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073028-PPIVol-05.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073101-PPIVol-06.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073131-PPIVol-07.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073204-PPIVol-08.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073237-PPIVol-09.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073309-PPIVol-10.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073342-PPIVol-11.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073415-PPIVol-12.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073448-PPIVol-13.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073521-PPIVol-14.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073555-PPIVol-15.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073628-PPIVol-16.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073702-PPIVol-17.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073737-PPIVol-18.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140227/9505CAS-20140227-073812-PPIVol-19.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140228/9505CAS-20140228-124323-PPIVol.nc"
]
netcdf_path=[
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081004-PPIVol-01.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081037-PPIVol-02.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081110-PPIVol-03.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081143-PPIVol-04.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081214-PPIVol-05.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081244-PPIVol-06.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081317-PPIVol-07.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081350-PPIVol-08.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081424-PPIVol-09.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081457-PPIVol-10.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081530-PPIVol-11.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081558-PPIVol-12.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081625-PPIVol-13.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081654-PPIVol-14.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081716-PPIVol-15.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081739-PPIVol-16.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081803-PPIVol-17.nc",
"/simepar/projetos/opera/Radar_Cascavel/netcdf/20140305/9505CAS-20140305-081829-PPIVol-18.nc"
]
netcdf_path=[
"/discolocal/agama/9505CAS-20140312-164504-PPIVol.nc",
"/discolocal/agama/9505CAS-20140312-164551-PPIVol.nc",
"/discolocal/agama/9505CAS-20140312-165236-PPIVol.nc",
]

new_nc=[netcdf.netcdf_file(netcdf_path[0], 'r')]

Kern.start_up((1600,800))
ppi_rect=pygame.Rect(50,50,600,600)
moment=["DBZH"]
threshold=[25]
ppi=PPI.PPI(ppi_rect,Kern.jobs,(new_nc,[0],moment,threshold),name="PPI")
Kern.modules.append(ppi)
Kern.modules.append(MainMenu(pygame.Rect(600,0,30,30),Kern.jobs,(netcdf_path,new_nc),name="MainMenu"))
#line=Line.Line(ppi_rect,Kern.jobs,(new_nc,ppi.scaledDataSprite,ppi.dirty),name="Line")
#Kern.modules.append(line)
#cut_rect=pygame.Rect(700,50,600,300)

#Kern.modules.append(Corte.VerticalCut(cut_rect,Kern.jobs,(new_nc,moment,threshold,line.P,line.Proj),ppi.convertion_to_radar,name="Cut"))

#cut_rect=pygame.Rect(700,400,600,300)

#Kern.modules.append(Corte.VerticalCut(cut_rect,Kern.jobs,(new_nc,["DBZH"],[33],line.P,line.Proj),ppi.convertion_to_radar,name="Cut"))

ppi_rect2=pygame.Rect(700,50,600,600)
#ppi2=PPI2.PPI(ppi_rect2,Kern.jobs,([netcdf.netcdf_file(netcdf_path[3], 'r')],[0],moment,threshold),name="PPI2")
#Kern.modules.append(ppi2)
Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")


