# -*- coding: utf-8 -*-
"""
File to contain sprites, including Colorbar
"""

import os, pygame
from pygame.locals import *


coresDBZH = ((0,219,219),(0,186,186),(0,153,153),(0,117,120),(94,94,255),(71,71,255),
                        (46,46,255),(23,23,255),(0,250,0),(0,207,0),(0,179,0),(0,150,0),
                        (0,120,0),(0,84,0),(0,56,0),(250,242,0),(245,224,0),(245,209,0),
                        (242,194,0),(232,140,0),(230,122,0),(250,0,0),(222,0,0),(199,0,0),
                        (184,0,0),(166,0,0),(255,158,255),(255,115,247),(224,33,224),(204,33,235),
                        (176,41,219),(161,43,214),(145,43,204))  
                            

spritedic={} #make Spritedic global, at list for the files that import this file

"""GENERAL SPRITE"""
class Sprite(pygame.sprite.DirtySprite):
    """subclass from Sprite, add blit, blit_in, and mouse_over functions, as well as flags for change"""
    who_created_me= "" # as sprites may be created every-where, this is impostant to help locating the source of a sprite, this is os reponsability of the develorpers

    
    def __init__(self, rect=None, image=None, pos=(0,0),who_created_me="don't know"):# can be inicialased with either a rect or a image
        pygame.sprite.DirtySprite.__init__(self)
        self.who_created_me=who_created_me
        if rect==None:
            self.image=image
            self.rect=image.get_rect()
            self.rect.move_ip(pos[0],pos[1])
        else:
            self.rect=rect
            self.image=pygame.Surface(rect.size)
        self.image.convert()
        self.dirty=1# image is always dirty and will always be printed, unless I say diffente elsewhere. This is pygame internal

    def blit(self,*arg):# the same as blit but for sprite
        self.up_to_date=False
        self.image.blit(*arg)
    def blit_in(self,surface,*arg): #blit sprite in given surface
        surface.blit(self.image,self.rect,*arg)
    def write(self,surf,back):
        if self.dirty>0:
            if self.dirty==1:
                self.dirty=0
            if back:
                surf.blit(back.image,self.rect,self.rect.move((-back.rect.left,-back.rect.top)))
            surf.blit(self.image,self.rect)
        elif self.dirty==-1:
            surf.blit(back.image,self.rect,self.rect.move((-back.rect.left,-back.rect.top)))
        
    
    
    def mouse_over(self,add=(0,0)):#get mouse position and see if it is in rect
        pos=pygame.mouse.get_pos()
        return self.rect.collidepoint(pos[0]-add[0],pos[1]-add[1])
    def reset(self, rect=None, image=None, pos=(0,0),who_created_me="don't know"): # same as __init__ but don't recreate de sprite
        if not who_created_me=="don't know":
            self.who_created_me=who_created_me
        if image!=None:
            self.image=image
            self.rect=image.get_rect()
            self.rect.move_ip(pos[0],pos[1])
        elif rect!=None:
            self.rect=rect
            self.image=pygame.Surface(rect.size)
            
 
 


"""CORTES RADIAIS"""
class RadialCut(Sprite):
    """subclass from Sprite, add blit, blit_in, and mouse_over functions, as well as flags for change"""
    up_to_date=True # lets mantain this as a flag if the Sprite has changed
    moved=False # lets mantain this as a flag if the Sprite was moved
    who_created_me= "" # as sprites may be created every-where, this is impostant to help locating the source of a sprite, this is os reponsability of the develorpers
    cuts={} #dictionary that will constain the cuts already make

    def __init__(self, rect=None, image=None, pos=(0,0),who_created_me="don't know"):# can be inicialased with either a rect or a image
        Sprite.__init__(self,rect,image,pos,who_created_me)


    def reset(self, rect=None, image=None, pos=(0,0),who_created_me="don't know"): # same as __init__ but don't recreate de sprite
        super(RadialCut,self).reset(rect, image, pos,who_created_me)
        self.cuts.clear()

    def makeCut(self,angle, ui):
        azimute=int(angle) #force angle to int
        import GeraCorte # I got a strange erros when this is in the header, so let it here
        #try:
        self.cuts[angle]= GeraCorte.CorteRadial_in_C(ui, self.rect, azimute)
        #except:
         #   self.cuts[angle]=GeraCorte.returnCorteRadial(ui.nc, self.rect, azimute)
        
    def setCut(self,angle):
        #will get the next cut with smaller azimuth and put in in image, as well as return the angle
        smaller=-400
        for key in self.cuts.keys():
            if key<angle and key>smaller:
                smaller=key
        if smaller==-400:
            return 0
        self.image=self.cuts[smaller]
        return smaller
        
    def die(self):
        import gc
        for key in self.cuts.keys():
            del self.cuts[key]
            
        gc.collect()
        
     

        
