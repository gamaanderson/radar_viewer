#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Fev 2013
@author: tiagoburiol@gmail.com
"""
# Instalacao das bibliotecas necessarias
# sudo apt-get install python-numpy python-scipy python-matplotlib
from pygame.locals import *
import numpy as np 
import interpolador as inter
#import subprocess
from Sprites import *
import Text
import pygame
import math
import GeraProds
import logging
from Sprites import *
import sys
from palette import palette
try:
    import cut_module
    cut_module.set_resolution(100,0.5,0.1) #set resolution range=100m, azimuth=0.5 elevation=0.1
except:
    pass

    
#class GeraCorte:
run = True
offset = 32 #espacamento dado ate limite da imagem para caber os numeros
corteSurf = pygame.Surface((50,50))
logger = logging.getLogger('GeraCorte')


def getOffset():
	return offset
def getCorteSurf():
	return corteSurf



 
     
def makeGrid(surf, nx, ny, xmin, xmax, ymin, ymax, vlabel, hlabel):
    """
    Cria uma grade na surface com linhas verticais e horizontais.
    nx e ny sao os numeros de divisoes em cada eixo para gerar a grade
    xmin e xmax são os valores min e max para o eixo horizontal
    ymin e ymax são os valores min e max para o eixo vertical
    vlabel e hlabel sao os nomes das variáveis nos eixos vert e hor
    """
    fontSize = 16
    font = pygame.font.SysFont("ubuntumono", fontSize) 
    cor = (60,60,60)
    cor_eixos = (50, 50, 50)
	
	# Cria uma surface transparente para desenhar a grade  
    gridSurf = pygame.Surface(surf.get_size())
    gridSurf.fill((0,0,255))          # preenche a surface com a cor azul
    gridSurf.set_colorkey((0,0,255))  # define essa cor como transparente
	
    grid_height = surf.get_height() - offset#altura da surface para calcular o numero de divisoes
    grid_width = surf.get_width() - offset#largura ra surface
	
	# coordenadas inicial e final para as extremidades das linhas
    xi = int(offset*0.8)
    xf = int(surf.get_width() - offset*0.2)
    yi = int(offset*0.2)
    yf = int(surf.get_height() - offset*0.8)

    #dx = grid_width/nx
    dy = grid_height/ny
      
    # linhas horizontais e texto, ny = numero de divisoes horizontais
    for i in range(ny+1):
		#print i
	
		P0 = (xi, int(yf-i*dy)) # y de baixo pra cima
		P1 = (xf, int(yf-i*dy))
		pygame.draw.aaline(gridSurf, cor, P0,P1)
		t = "%.f"% (ymin + i*(ymax-ymin)/ny)
		text = font.render(t, 1, cor_eixos)
		surf.blit(text, (offset*0.8 - text.get_width(), grid_height-i*dy))


	# linhas verticais e texto
    dx = np.array([1, 2.5, 5, 10, 20, 25, 50])
    idx = np.argmin(abs(np.tile(xmax/nx, (7)) - dx))
    nx =int((xmax/dx[idx]))
    

    for i in range(nx):
		P0 = (int(offset*0.8 + i*(grid_width/nx)), yi)
		P1 = (int(offset*0.8 + i*(grid_width/nx)), yf)
		pygame.draw.aaline(gridSurf, cor, P0,P1)
		t = str( int(i * dx[idx]))
		text = font.render(t, 1, cor_eixos)
		surf.blit(text, ( offset*0.8+i*(grid_width/nx), grid_height+offset*0.2))
	
	# desenha eixo vertical
    pygame.draw.line(gridSurf, cor_eixos, (xi,yi), (xi,yf), 2)
    # texto eixo vertical
    text = font.render(vlabel, 1, cor_eixos)
    text = pygame.transform.rotate(text, 90)  # rotaciona o texto
    posx = 0
    posy = gridSurf.get_height()*0.5 - text.get_height()*0.5
    surf.blit(text, (posx, posy))
    
    # desenha eixo horizontal
    pygame.draw.line(gridSurf, cor_eixos, (xi,yf), (xf,yf), 2) 
    # texto eixo horizontal
    text = font.render(hlabel, 1, cor_eixos)
    posx = gridSurf.get_width()*0.5 - text.get_width()*0.5
    posy = gridSurf.get_height() - text.get_height() + 1 
    surf.blit(text, (posx, posy))
    
    # cola a grid na surf de visualizacao do corte
    surf.blit(gridSurf, (0,0))
    return surf
     
     
     
     
 ###################################################################
def geraCorte(nc, dw, x0, y0, x1, y1, moment='DBZH', height=16):
    #print x0, y0, x1, y1    
    # pega os valores apropriados para a formacao da matriz no netcdf_radial
    e=0
    Sweep_start=nc.variables['sweep_start_ray_index'].data #[0  360  720 1080 1440 1800 2160 2520 2880 3240 3600 3960]

    Sweep_end=nc.variables['sweep_end_ray_index'].data #[ 359  719 1079 1439 1799 2159 2519 2879 3239 3599 3959 4319]
  
    Gates=nc.variables['ray_n_gates'].data

    Distance=nc.variables['ray_start_index'].data
    N_Sweeps=nc.dimensions['sweep']
    # Pega o numero de Bins
    nBins = Gates[Sweep_end[e]]
    # Pega o range e converte para metros        
    radarRange = nc.variables['range'].data #200.0 km
    radarRange = radarRange[-1] - radarRange[0] # Range em metros
    # Pega os dados de refletividade do arquivo NetCDF
    nrays = Sweep_end - Sweep_start + 1
    nbins = Gates[Sweep_start]
    nelevs = N_Sweeps
    nvalores = nrays*nbins
    maxbins = nbins.max()
    maxrays = nrays.max()
    data = np.zeros((maxrays, maxbins, nelevs))
    dat = np.array(nc.variables[moment].data)
    Azim=nc.variables['azimuth'].data    
    Azim1=Azim[Sweep_start[e]]#START AZIMUTH
    Azim2=Azim[Sweep_end[e]] #END AZIMUTH
    ase=np.zeros(N_Sweeps)
    
    ini = 0
    for e in xrange(nelevs):
        fim = (Sweep_end[e]+1)*nbins[e]
        #data[e] = np.zeros(Sweep_end[e]-Sweep_start[e]+1,Gates[Sweep_end[e]])
        valores = np.r_[dat[Distance[Sweep_start[e]]:Distance[Sweep_end[e]]+Gates[Sweep_end[e]]], np.tile(-32768.,(maxbins*maxrays)-nvalores[e])]
        data[:,:,e] = valores.reshape(maxrays, maxbins)
        #dat = dat[range(Distance[Sweep_start[e]],Distance[Sweep_end[e]]+Gates[Sweep_end[e]])]
        #dat = dat.reshape(Sweep_end[e]-Sweep_start[e]+1,Gates[Sweep_end[e]])   
        #data[:,:,e]=dat
        ini = fim
        ase[e]=Azim[Sweep_start[e]]

 
    #data=np.zeros((360,1184,N_Sweeps+1))
    #print data.shape 
    #for h in range(N_Sweeps+1):
    #for i in range(Sweep_end[h]-Sweep_start[h]+1):
    #for j in range(Gates[Sweep_end[h]]):
    #dat = np.array(nc.variables['DBZH'].data) # data.shape=(800, 360, 19)
    #dat = dat[range(Distance[Sweep_start[h]],Distance[Sweep_end[h]]+Gates[Sweep_end[h]])]
    #dat = dat.reshape(Sweep_end[h]-Sweep_start[h]+1,Gates[Sweep_end[h]])
    #data[:,:,h]=dat
    #data = np.where(data <> dat, -32768., data)
    #print data.shape() 
    data = data*nc.variables[moment].scale_factor
    # pega as elevacoes, converte em radianos e guarda em um array do numpy 
    # elevs
    elevs = np.deg2rad((np.array(nc.variables['fixed_angle'].data)))
    print nc.variables['fixed_angle'].data
    # Pega o numero de elevacoes, o mesmo que: nElevs = len(nc.variables['elevation'].data)
    nElevs = len(elevs)
    
    # Cria algumas constantes 
    #pi = 3.14159265359#math.pi   # a constante trigonometrica Pi
    meioGrauEmRad = 0.008726646#(pi/360)#*0.2
    # Altura do plano de corte em km
    #height = 16
    #raio da terra
    rt =  8500000# (4/3)*6375000
    
    # resolucao da surf do radar escalonada
    #res = int(dw.getRadarDataSurf().get_height() * dw.scaleFactor)
    #res = dw.scaledRes
    
    
    # Mapeia os pontos dados em coordenadas da tela para coordenadas do radar (em metros).
    # Nas coordenadas da tela o canto superior esquerdo eh o (x,y)=(0,0) e o y incrementa para baixo.
    # Nas coordenadas no radar, o centro eh o radar e o y incrementa para cima.
    # Entao, eh preciso transladar a origem, inverter o eixo y, e escalonar as coordenadas 
    # em um fator=diametro/resolucao. E...Voilah!
    (x0,y0)=GeraProds.screenToRadarCoords((x0,y0),dw,nc)
    (x1,y1)=GeraProds.screenToRadarCoords((x1,y1),dw,nc)
    # Calcula a distancia em metros entre (x0, y0) e (x1, y1)
    dist = math.sqrt((x1-x0)**2 + (y1-y0)**2)
    
     
    # Resolucao horizontal e vertical da matriz do corte
    # Calcula o numero de linhas e colunas para gerar a imagem
    # Descontando o espacamento para o texto e numeros (offset) 
    nRows = dw.getCorteSurf().get_height()-  offset
    nCols = dw.getCorteSurf().get_width()-  offset
    
    
    # Gera as coordenadas espaciais dos pontos do corte
    # Observe que x, y e z sao arrays, cada (x[i],y[i]) corresponde a um
    # ponto no plano horizontal sobre a reta que eh a base do plano do corte vertical 
    x = x0 + (0.5 + np.arange(0,nCols,1)) * (x1-x0)/nCols # Comeca em x0 e vai incrementando dx=(x1-x0)/nCols
    y = y0 + (0.5 + np.arange(0,nCols,1)) * (y1-y0)/nCols # Comeca em y0 e vai incrementando dy=(y1-y0)/nCols
    z = -nc.variables['altitude'].data + (0.5 + np.arange(0,nRows,1)) * (height * 1000)/nRows
    
    # reverte a ordem dos elementos do vetor z (util para gerar a imagem)
    h = z[::-1]
    # Calcula as distancias de cada ponto da linha que e a base do plano 
    # do corte vertical ate o radar
    s = np.sqrt(x*x+y*y)
    # Calcula os angulos polares no sistema de coordenadas do radar
    # Observe que s e theta sao arrays que dao as coordenadas polares dos 
    # pontos (x,y) sobre a reta que eh a base do plano de corte 
    theta = np.arctan2(x,y)
    theta = np.degrees(theta)    
    theta_res=theta
    # Array com os indices dos rays que interceptam o plano de corte
    # Esses ja sao os indices correspondentes aos rays do arquivo nc (NetCDF)
    #rays = np.array([], dtype=np.int16) # define o tipo de dado como int16 (parece deixar o programa mais rapido)
    rays=np.zeros((theta.shape[0],N_Sweeps))
    for i in xrange(N_Sweeps):
        theta=theta_res 
        Azim1=Azim[Sweep_start[i]]#START AZIMUTH
        Azim2=Azim[Sweep_end[i]] #END AZIMUTH    
        theta = np.where(theta<0,theta+360,theta)
        theta=theta-Azim1
        theta = np.where(theta<0,theta+360,theta)
        rays[:,i]=theta
        if(Azim2>Azim1):
            Azim2=Azim[Sweep_end[i]-1]
        if(Azim2>Azim1):
            Azim2=Azim[Sweep_end[i]-2]
        dAzim=Azim2-Azim1;   
        if dAzim<0:
            dAzim=dAzim+360
        rays[:,i] = np.round(rays[:,i]*(Sweep_end[i]-Sweep_start[i])/(dAzim)).astype(int) # converte theta para graus e arredonda para o inteiro mais proximo    
        rays[:,i] = np.where(rays[:,i]>Sweep_end[i]-Sweep_start[i],0,rays[:,i])

    #rays = np.where(rays>359,rays-360,rays) # se passar de 359, reinicia a contagem :)
    # Cria uma matriz de nRows por nCols preenchida com valores -999.9
    # Eh nessa matriz que serao inseridos os dados de refletividade do corte.
    # Essa matriz representa o plano de corte.
    DATA = np.tile(-32768., (nRows, nCols))
    
    
    ################################################
    # Faz as contas para corrigir as alturas
    ################################################
    # Cria uma grade 2D a partir dos arrays s e h
    S, H = np.meshgrid(s, h) 
    R0 = rt*np.tan(S/rt)
    H0 = np.sqrt(R0**2 + rt**2)-rt
    SIGMA = S/rt
    BETHA = math.pi*0.5+ SIGMA    
    H1 = H-H0
    R = np.sqrt(np.multiply(R0,R0) + np.multiply(H1,H1) - 2*np.multiply(np.multiply(R0,H1),np.cos(BETHA))) 
    ELEVS = np.arcsin(np.divide(np.multiply(H1,np.sin(BETHA)),R))
    R = np.round(R/nc.variables['range'].meters_between_gates)            #250 para netcdf e 125 para netcdf-radial
    ################################################
    
    
    # armazena os indices da elevacao correspondente
    ELEVS2 = ELEVS
    for i in range(nElevs):
        ELEVS2 = np.where(abs(ELEVS2-elevs[i])>meioGrauEmRad,ELEVS2,i)
    
    ELEVS2 = np.where(ELEVS2-ELEVS==0,-1,ELEVS2)
    ELEVS2 = ELEVS2.astype(int)

    
    I = np.where((R<nBins)&(ELEVS2>=0))[0]
    J = np.where((R<nBins)&(ELEVS2>=0))[1]
    
    
    # Alimenta a matriz do corte com os dados 
    
    for idx in range(len(I)):
        e = ELEVS2[I[idx]][J[idx]]
        r = rays[J[idx]][e]
        b = R[I[idx]][J[idx]]
        
        DATA[I[idx]][J[idx]] = data[r][b][e]
#         DATA[I[idx]][J[idx]] = data[R[I[idx]][J[idx]]][rays[J[idx]]][ELEVS2[I[idx]][J[idx]]    
    
    # reconstroi o perfil    
    DATA = inter.interpola1D(DATA)
    #print DATA.shape
    return DATA

      
    
def show(nc, dw, x0, y0, x1, y1,moment):
    # gera os dados
    data = geraCorte(nc, dw, x0, y0, x1, y1,moment)

    # cria uma surf para renderizar os dados   
    corteSurf = pygame.Surface((data.shape[1], data.shape[0]))
    corteSurf.fill((150, 150, 150))
    dw.getCorteSurf().fill((175, 175, 175))

    corteSurf = GeraProds.mapeiaCores(corteSurf, data,moment)

    dw.getCorteSurf().blit(corteSurf, ( offset*0.8,  offset*0.2))

    # gera a grade
    dist = math.sqrt((x1-x0)**2 + (y1-y0)**2)
    height = 16
    # gera a grade
    spritedic['vertical_cut'].blit(makeGrid(dw.getCorteSurf(), 10, 8, 0, dist, 0, height, "km", moment),(0,0))
    #spritedic['vertical_cut'].blit(corteSurf,(0,0))
    return corteSurf    
        
        
        
        
#ASCOPE
def ascope( nc, dw, x0, y0, x1, y1,moment='DBZH'):
    pass

def CorteRadial_in_C(ui,rect,azimute):#this returns a surf
    #offset=30
    import palette
    array=cut_module.radial(ui.open_path,(rect.height-offset,rect.width-offset),azimute,ui.moment, palette.palette[ui.moment][0:3])
    surf=pygame.Surface((rect.width-offset,rect.height-offset),depth=32)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((150, 150, 150))
    corteSurf.blit(surf,(0,0))
    
    spritesurf=pygame.Surface(rect.size)
    spritesurf.fill((175, 175, 175))
    
    spritesurf.blit(corteSurf, ( offset*0.8,  offset*0.2))
    
    radarRange = (ui.nc.variables['range'][-1]) #200km
    x1 = radarRange * math.sin(math.radians(azimute)) 
    y1 = -radarRange * math.cos(math.radians(azimute))
    
    # gera a grade
    dist = math.sqrt((x1)**2 + (y1)**2)
    height = 16 
    makeGrid(spritesurf, 10, 8,  0, dist*0.001, 0, height, "km", ui.moment)
    return spritesurf 

def returnBackground(self,image,scaled_size,image_rect): # load backgrond return surface of size of surfrec (rectangle). If full=False cut current PPI region
        background = pygame.image.load(image)
        
        X=self.image_rect.left*background.get_width()/scaled_size[0]
        Y=self.image_rect.top*background.get_width()/scaled_size[1]
        width=image_rect.width*background.get_width()/scaled_size[0]
        height=image_rect.height*background.get_height()/scaled_size[1]
        rect=pygame.Rect(X,Y,width,height)
    
        #Now we have to fix rectange outside surface
        if background.get_rect().contains(rect):# no problem
            #cut rect off and scale
            return pygame.transform.smoothscale(background.subsurface(rect), image_rect.size)
        else:# this only make sence for full=False
            #clif rectangle
            new_rect=rect.clip(background.get_rect())
            # calculate new size to scale
            scale_X=new_rect.width*image_rect.width/width
            scale_Y=new_rect.height*image_rect.height/height
            background_scaled=pygame.transform.smoothscale(background.subsurface(new_rect), (scale_X,scale_Y))
            surf=pygame.Surface(image_rect.size)
            if background.get_rect().collidepoint(rect.topleft): # top-left corner is in the surf, good, no translation to make
                surf.blit(background_scaled,(0,0))
            else: #have to make some coodinates change, this also work in the above case, but we don't need it there
                #calculate new_rect.topleft in relation to rect.topleft
                new_X=new_rect.left-rect.left
                new_Y=new_rect.top-rect.top
                #scale to surf[0] size
                posX=new_X*image_rect.width/rect.width
                posY=new_Y*image_rect.width/rect.width
                surf.blit(background_scaled,(posX,posY))
                
            return surf





def makePPI((nc,elev,moment,threshold), scaled_size,image_rect,dw,backgroundPath):
    
    import palette
    #print (x0,y0),(x1,y1)
    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
                                                    palette.palette[moment][1]-
                                                    (palette.palette[moment][1]-palette.palette[moment][2])
                                                    *threshold/len(palette.palette[moment][0]))
    array=cut_module.PPI(nc.filename,scaled_size,image_rect.topleft, image_rect.size,elev,moment,color)
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_size,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(200)
    back2.blit(back,(0,0))
    back.set_colorkey((100,100,100))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2


def makePPIfull(dw,ui,rect,pos):
    
    import palette
    #print (x0,y0),(x1,y1)
    array=cut_module.PPI(ui.open_path,spritedic['PPI'].rect.size,(0, 0), spritedic['PPI'].rect.size,ui.elev,ui.moment,palette.palette[ui.moment][0:3])
    surf=pygame.Surface(spritedic['PPI'].rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=dw.returnBackground(dw.backgroundPath,spritedic['PPI'],True)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(200)
    back2.blit(back,(0,0))
    back.set_colorkey((100,100,100))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    
    dw.radarDataSurf.blit(back2,(0,0))
    dw.scaledDataSurf = pygame.transform.smoothscale(dw.radarDataSurf, (dw.scaledRes, dw.scaledRes))
    dw.desenhaCirculos(dw.radarDataSurf)
    dw.desenhaCirculos(dw.scaledDataSurf)

def CorteVertical_in_C(ui,dw,x0, y0, x1, y1,):#this returns a surf
    #offset=30
    import palette
    (x0,y0)=GeraProds.screenToRadarCoords((x0,y0),dw,ui.nc)
    (x1,y1)=GeraProds.screenToRadarCoords((x1,y1),dw,ui.nc)
    rect=spritedic['vertical_cut'].rect
    #print (x0,y0),(x1,y1)
    array=cut_module.vertical(ui.open_path,(rect.height-offset,rect.width-offset),(x0,y0),(x1,y1),ui.moment,palette.palette[ui.moment][0:3])
    
    surf=pygame.Surface((rect.width-offset,rect.height-offset))
    surf=surf.convert(32)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((150, 150, 150))
    

    corteSurf.blit(surf,(0,0))
    
    spritesurf=pygame.Surface(rect.size)
    spritesurf.fill((175, 175, 175))
    
    

    spritesurf.blit(corteSurf, ( offset*0.8,  offset*0.2))
    
    radarRange = (ui.nc.variables['range'][-1]) #200km
    
    
    # gera a grade
    dist = math.sqrt((x1-x0)**2 + (y1-y0)**2)
    print "geracorte", dist
    height = 16
    makeGrid(spritesurf, 10, 8, 0, dist*0.001, 0, height, "km", ui.moment)
    spritedic['vertical_cut'].blit(spritesurf,(0,0))


# gera os cortes radiais
def returnCorteRadial(nc, rect, azimute):#this returns a surface
     # gera os dados
     data =geraCorteRadial(nc, rect, azimute)
    
     radarRange = (ui.nc.variables['range'][-1]) #200km
     x1 = radarRange * math.sin(math.radians(azimute)) 
     y1 = -radarRange * math.cos(math.radians(azimute))
    
     # cria uma surf para renderizar os dados   
     corteSurf = pygame.Surface((data.shape[1], data.shape[0]))
     corteSurf.fill((150, 150, 150))
     corteSurf = GeraProds.mapeiaCores(corteSurf, data)
     
     spritesurf=pygame.Surface(rect.size)
     spritesurf.fill((175, 175, 175))
     
     spritesurf.blit(corteSurf, ( offset*0.8,  offset*0.2))
     
     # gera a grade
     dist = math.sqrt((x1)**2 + (y1)**2)
     
     height = 16
     makeGrid(spritesurf, 10, 8, dist, height)
     return spritesurf 




        
def geraCorteRadial(nc, rect, ray, moment='DBZH', height=16,):#this returns a matriz with the data, no image

    # Pega as informacoes do arquivo de dados
    # pega os valores apropriados para a formacao da matriz no netcdf_radial
    Sweep_start=f.variables['sweep_start_ray_index'].data
    Sweep_end=f.variables['sweep_end_ray_index'].data
    Gates=f.variables['ray_n_gates'].data
    Distance=f.variables['ray_start_index'].data     
    nBins = Gates[Sweep_end[e]]
    radarRange = (nc.variables['range'][-1]-nc.variables['range'][0]) # Range em metros
    N_Sweeps=nc.dimensions['sweep']
    

    #data = np.array(nc.variables['DBZH'].data) # data.shape=(800, 360, 19)
    # Transformacao de vetor para matriz
    #data = data[range(Distance[Sweep_start[e]],Distance[Sweep_end[e]]+nBins)]
    #data = data.reshape(Sweep_end[e]-Sweep_start[e]+1,nBins)
    nrays = Sweep_end - Sweep_start + 1
    nbins = Gates[Sweep_start]
    nelevs = N_Sweeps
    nvalores = nrays*nbins
    
    maxbins = nbins.max()
    maxrays = nrays.max()
    data = np.zeros((maxrays, maxbins, nelevs))
    dat = np.array(nc.variables[moment].data)
    
    ini = 0
    for e in xrange(nelevs):
        fim = (Sweep_end[e]+1)*nbins[e]
        valores = np.r_[dat[Distance[Sweep_start[e]]:Distance[Sweep_end[e]]+Gates[Sweep_end[e]]], np.tile(-32768.,(maxbins*maxrays)-nvalores[e])]
        data[:,:,e] = valores.reshape(maxrays, maxbins)
        ini = fim
    data = data*nc.variables[moment].scale_factor
    elevs = np.deg2rad((np.array(nc.variables['fixed_angle'].data)))
    nElevs = len(elevs)
    # Cria algumas constantes 
    meioGrauEmRad = 0.008726646#(pi/360)#*0.2
    rt =  8500000# (4/3)*6375000

       
    # Resolucao horizontal e vertical da matriz do corte
    # Calcula o numero de linhas e colunas para gerar a imagem
    # Descontando o espacamento para o texto e numeros (offset) 
    nRows = rect.height-  offset
    nCols = rect.width-  offset

    # cria e reverte a ordem dos elementos do vetor h
    h = (0.5 + np.arange(0,nRows,1)) * height * 1000/nRows
    h = h[::-1]

    # Calcula as distancias de cada ponto da linha base do plano 
    # do corte vertical ate o radar
    ds = radarRange/nCols
    s = (0.5 + np.arange(nCols)) * ds
    
    # Cria uma matriz de nRows por nCols preenchida com valores -999.9
    # Eh nessa matriz que serao inseridos os dados de refletividade do corte.
    # Essa matriz representa o plano de corte.
    DATA = np.tile(-32768., (nRows, nCols))
    
    
    ################################################
    # Faz as contas para corrigir as alturas
    ################################################
    # Cria uma grade 2D a partir dos arrays s e h
    S, H = np.meshgrid(s, h) 
    
    R0 = rt*np.tan(S/rt)
    H0 = np.sqrt(R0**2 + rt**2)-rt
    SIGMA = S/rt
    BETHA = math.pi*0.5+ SIGMA    
    H1 = H-H0
    R = np.sqrt(np.multiply(R0,R0) + np.multiply(H1,H1) - 2*np.multiply(np.multiply(R0,H1),np.cos(BETHA))) 
    ELEVS = np.arcsin(np.divide(np.multiply(H1,np.sin(BETHA)),R))
    R = np.round(R/nc.variables['range'].meters_between_gates)
    ################################################
    
    
    # armazena os indices da elevacao correspondente
    ELEVS2 = ELEVS
    for i in range(nElevs):
        ELEVS2 = np.where(abs(ELEVS2-elevs[i])>meioGrauEmRad,ELEVS2,i)
    
    ELEVS2 = np.where(ELEVS2-ELEVS==0,-1,ELEVS2)
    ELEVS2 = ELEVS2.astype(int)
    #print ELEVS2
    
    I = np.where((R<nBins)&(ELEVS2>=0))[0]
    J = np.where((R<nBins)&(ELEVS2>=0))[1]
    
    
    # Alimenta a matriz do corte com os dados 
    
    for idx in range(len(I)):
        e = ELEVS2[I[idx]][J[idx]]
        r = ray
        b = R[I[idx]][J[idx]]
        
        DATA[I[idx]][J[idx]] = data[r][b][e]
        

    
    # reconstroi o perfil    
    DATA = inter.interpola1D(DATA)
    
    #print DATA.shape
    return DATA



    

