#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np 
import pygame
from palette import palette
import h5py 
from scipy import ndimage



def start_file(nc,moment):#for compactibility with modules that use pre-processing
    return #we don't use pre-processing
    



def CAPPI(nc,scaled_size,corner,size,elev,smooth,moment,colors):

    j,i = np.meshgrid(np.arange(size[0]), np.arange(size[1]))

    I = (i+corner[0]-scaled_size[0]*0.5)*1000000./scaled_size[0] #coordinate in meters over earth surface
    J = -(j+corner[1]-scaled_size[1]*0.5)*1000000./scaled_size[1] #coordinate in meters over earth surface
    
    x = nc.variables["xc"].getValue()*nc.variables["xc"].attributes["scale_factor"]+nc.variables["xc"].attributes["add_offset"]
    y = nc.variables["yc"].getValue()*nc.variables["yc"].attributes["scale_factor"]+nc.variables["yc"].attributes["add_offset"]

    X = np.round((I-x[0])/(x[1]-x[0])).astype(int)
    Y = np.round((J-y[0])/(y[1]-y[0])).astype(int)
    data=nc.variables[moment].getValue()[0][elev]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    missing_value=nc.variables[moment].attributes["missing_value"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    no_echo=nc.variables[moment].attributes["no_echo"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]

    idx=np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y)))
    array=np.ones(size)*missing_value
    array[idx]=data[Y[idx],X[idx]]
#    array = np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y)),  data[Y,X], missing_value)

    if smooth: array = ndimage.filters.median_filter(array, size = 3)
    
    colorArray=np.zeros(size).astype(int)
    ncolors = len(colors[0])        # Get the number of
    #print colorArray.shape
    # For each color in palette, evaluate the data interval for this color, 
    # find the index of all data in the interval and place the colorRGB values 
    # in mapped surface  
    for c in range(ncolors):
        color = colors[0][c]         # get RGB values of color number n
        minimum = colors[1][c]             # The minimum value for this color
        maximum = colors[1][c+1]           # The maximum value for this color
        idx = np.where((array>=minimum)&(array<maximum)) # Get the index for data in interval 
        colorArray[idx] = int(color[0]*256*256+color[1]*256+color[2])
    
    idx = np.where(array==missing_value) # Get the index for data in interval 
    colorArray[idx] = int(1*256*256+1*256+1)
    idx = np.where(array==no_echo) # Get the index for data in interval 
    colorArray[idx] = int(0)
    return colorArray

def vertical(nc,scaled_size,P1,P2,smooth,moment,colors):
    corner=(0,0)
    size=scaled_size
    
    j,i = np.meshgrid(np.arange(size[1]), np.arange(size[0]))
    t=((i+corner[0])*1.0/(scaled_size[0]-1))
    print size,j.shape
    I_x = (1-t)*P1[0]+t*P2[0]  #coordinate_x in meters over earth surface
    I_y = (1-t)*P1[1]+t*P2[1]  #coordinate_y in meters over earth surface7
    
    J = (-j + corner[1]+scaled_size[1])*16000./scaled_size[1] #Height
    
    x = nc.variables["xc"].getValue()*nc.variables["xc"].attributes["scale_factor"]+nc.variables["xc"].attributes["add_offset"]
    y = nc.variables["yc"].getValue()*nc.variables["yc"].attributes["scale_factor"]+nc.variables["yc"].attributes["add_offset"]
    z = nc.variables["bottom_top"].getValue()
    
    X = np.round((I_x-x[0])/(x[1]-x[0])+0.5).astype(int)
    Y = np.round((I_y-y[0])/(y[1]-y[0])+0.5).astype(int)
    Z = np.round((J-z[0])/(z[1]-z[0])-0.5).astype(int)
    
    data=nc.variables[moment].getValue()[0]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    missing_value=nc.variables[moment].attributes["missing_value"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    no_echo=nc.variables[moment].attributes["no_echo"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]

    idx=np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y))& (Z>=0) & (Z<len(z)))
    array=np.ones(size)*missing_value
    array[idx]=data[Z[idx],Y[idx],X[idx]]
#    array = np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y)),  data[Y,X], missing_value)

    if smooth: array = ndimage.filters.median_filter(array, size = 3)
    
    colorArray=np.zeros(size).astype(int)
    ncolors = len(colors[0])        # Get the number of
    #print colorArray.shape
    # For each color in palette, evaluate the data interval for this color, 
    # find the index of all data in the interval and place the colorRGB values 
    # in mapped surface  
    for c in range(ncolors):
        color = colors[0][c]         # get RGB values of color number n
        minimum = colors[1][c]             # The minimum value for this color
        maximum = colors[1][c+1]           # The maximum value for this color
        idx = np.where((array>=minimum)&(array<maximum)) # Get the index for data in interval 
        colorArray[idx] = int(color[0]*256*256+color[1]*256+color[2])

    idx = np.where(array==missing_value) # Get the index for data in interval 
    colorArray[idx] = int(110*256*256+110*256+110)
    idx = np.where(array==no_echo) # Get the index for data in interval 
    colorArray[idx] = int(0)
    return colorArray

def MOSAICO(nc,scaled_size,corner,size,elev,smooth,moment,colors):
    j,i = np.meshgrid(np.arange(size[0]), np.arange(size[1]))

    x = nc.variables["xc"].getValue()*nc.variables["xc"].attributes["scale_factor"]+nc.variables["xc"].attributes["add_offset"]
    y = nc.variables["yc"].getValue()*nc.variables["yc"].attributes["scale_factor"]+nc.variables["yc"].attributes["add_offset"]
    print x
    I = ((i+corner[0])*(x[-1]-x[0])/scaled_size[0])+x[0] #coordinate in meters over earth surface
    J = ((scaled_size[1]-j-corner[1])*(y[-1]-y[0])/scaled_size[1])+y[0] #coordinate in meters over earth surface
    print I
    X = np.round((I-x[0])/(x[1]-x[0])).astype(int)
    Y = np.round((J-y[0])/(y[1]-y[0])).astype(int)
    data=nc.variables[moment].getValue()[0][elev]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    missing_value=nc.variables[moment].attributes["missing_value"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    no_echo=nc.variables[moment].attributes["no_echo"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]

    idx=np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y)))
    array=np.ones(size)*missing_value
    array[idx]=data[Y[idx],X[idx]]
#    array = np.where((X>=0) & (X<len(x)) & (Y>=0) & (Y<len(y)),  data[Y,X], missing_value)

    if smooth: array = ndimage.filters.median_filter(array, size = 3)
    
    colorArray=np.zeros(size).astype(int)
    ncolors = len(colors[0])        # Get the number of
    #print colorArray.shape
    # For each color in palette, evaluate the data interval for this color, 
    # find the index of all data in the interval and place the colorRGB values 
    # in mapped surface  
    for c in range(ncolors):
        color = colors[0][c]         # get RGB values of color number n
        minimum = colors[1][c]             # The minimum value for this color
        maximum = colors[1][c+1]           # The maximum value for this color
        idx = np.where((array>=minimum)&(array<maximum)) # Get the index for data in interval 
        colorArray[idx] = int(color[0]*256*256+color[1]*256+color[2])
    
    idx = np.where(array==missing_value) # Get the index for data in interval 
    colorArray[idx] = int(1*256*256+1*256+1)
    idx = np.where(array==no_echo) # Get the index for data in interval 
    colorArray[idx] = int(0)
    return colorArray


def returnBackground(image,scaled_rect,image_rect): # load backgrond return surface of size of surfrec (rectangle). If full=False cut current PPI region
        if image==None:
            surf=pygame.Surface(image_rect.size)
            return surf            
            
        background = pygame.image.load(image)
        background=background.convert()
        X=-scaled_rect.left*background.get_width()/scaled_rect.size[0]
        Y=-scaled_rect.top*background.get_height()/scaled_rect.size[1]
        width=image_rect.width*background.get_width()/scaled_rect.size[0]
        height=image_rect.height*background.get_height()/scaled_rect.size[1]
        rect=pygame.Rect(X,Y,width,height)
    
        #Now we have to fix rectange outside surface
        if background.get_rect().contains(rect):# no problem
            #cut rect off and scale
            return pygame.transform.smoothscale(background.subsurface(rect), image_rect.size)
        else:# this only make sence for full=False
            #clif rectangle
            new_rect=rect.clip(background.get_rect())
            # calculate new size to scale
            scale_X=new_rect.width*image_rect.width/width
            scale_Y=new_rect.height*image_rect.height/height
            background_scaled=pygame.transform.smoothscale(background.subsurface(new_rect), (scale_X,scale_Y))
            surf=pygame.Surface(image_rect.size)
            if background.get_rect().collidepoint(rect.topleft): # top-left corner is in the surf, good, no translation to make
                surf.blit(background_scaled,(0,0))
            else: #have to make some coodinates change, this also work in the above case, but we don't need it there
                #calculate new_rect.topleft in relation to rect.topleft
                new_X=new_rect.left-rect.left
                new_Y=new_rect.top-rect.top
                #scale to surf[0] size
                posX=new_X*image_rect.width/rect.width
                posY=new_Y*image_rect.height/rect.height
                surf.blit(background_scaled,(posX,posY))
                
            return surf






    

def makeCAPPI_mdv((nc,elev,moment,threshold,smooth), scaled_rect,image_rect,backgroundPath):
    import palette
    print smooth
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])

    array=CAPPI(nc,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,smooth,moment,color)
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2



def makeVerticalCut_mdv((nc,moment,P,threshold,smooth), scaled_rect,image_rect,topoPath):
    
    import palette

    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
#    print color

    array=vertical(nc,scaled_rect.size,P[0],P[1],smooth,moment,color)


    surf=pygame.Surface(scaled_rect.size)

    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((200, 200, 200))
    

    corteSurf.blit(surf,(0,0))
    makeGrid(corteSurf)
    drawGround(topoPath,P,corteSurf)
    return corteSurf
    

def makeMOSAICO((nc,elev,moment,threshold,smooth), scaled_rect,image_rect,backgroundPath):
    import palette
    print smooth
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])

    array=MOSAICO(nc,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,smooth,moment,color)
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2


       
def makeGrid(surf):
    cor = (60,60,60)
    cor_eixos = (50, 50, 50)
    nx=10
    ny=8

    height = surf.get_height() #altura da surface para calcular o numero de divisoes
    width = surf.get_width() #largura ra surface

    #dx = grid_width/nx
    dy = float(height)/ny
      
    # linhas horizontais e texto, ny = numero de divisoes horizontais
    for i in range(ny+1):
        #print i
        i=i+0.5
        P0 = (0, int(height-i*dy)) # y de baixo pra cima
        P1 = (width+2, int(height-i*dy))
        pygame.draw.aaline(surf, cor, P0,P1)

    # linhas verticais e texto

    dx = float(width)/ny

    for i in range(nx+1):
        P0 = (int(width-i*dx), 0)
        P1 = (int(width-i*dx), height+2)
        #pygame.draw.aaline(surf, cor, P0,P1)
    pygame.draw.rect(surf,cor, surf.get_rect(),1)



import math
def drawGround(topoFile,P,surf):
#	print "drawGround",topoFile
	if topoFile==None: return 0
	n_points=180
	TOPO = np.fromfile( topoFile, dtype= np.int16)
#	print TOPO
	size = math.sqrt(TOPO.size)
#	print size,TOPO.size, TOPO.shape
	TOPO = np.reshape(TOPO, (size,size))
#	TOPO = TOPO.transpose()
	
	
 	cutWidth = surf.get_width()
 	cutHeight =  surf.get_height()

 	xground = []
 	yground = []
 	p = []

	#for i in range(x1-x0):
	for k in range(n_points):
		x = (P[0][0]+k*(P[1][0]-P[0][0])/(n_points-1.))* size/1000000.+size/2.
		y = -(P[0][1]+k*(P[1][1]-P[0][1])/(n_points-1.))* size/1000000.+size/2.
		xground.append(int(k*cutWidth/(n_points-1.)))
		if x<size and y<size and x>=0 and y>=0:
			if TOPO[int(x)][int(y)]<0:
				TOPO[int(x)][int(y)]=0
			elev = TOPO[int(x)][int(y)]*float(cutHeight)/16000
			yground.append(cutHeight- elev)
 			#print x, y, TOPO[y][x]
 		 	p.append((xground[-1], yground[-1]))

	if len(p)>2:
		pygame.draw.lines(surf, (200,200,200), False, p,1)



