
import pygame
from Sprites import *
import ToolsPanel

class Info(Sprite):

    class Tools(ToolsPanel.ToolsPanel2):
    
        def __init__(self):
            ToolsPanel.ToolsPanel2.__init__(self,rect=pygame.Rect(0,0,100,20),who_created_me="Info")
        
        
        def loadBotton(self,name):
            self.buttonSize = (16,16)
            rect=pygame.Rect((0,0),self.buttonSize)
            self.buttons['sprites']=ToolsPanel.Button(rect,"images/small_normal.bmp","images/small_pressed.bmp","%s:INFORM_SPRITES"%name,"Informacoes sobre as Sprites")
            rect=pygame.Rect((20,0),self.buttonSize)
            self.buttons['files']=ToolsPanel.Button(rect,"images/small_normal.bmp","images/small_pressed.bmp","%s:INFORM_FILES"%name,"Lista Arquivos Abertos")
            rect=pygame.Rect((40,0),self.buttonSize)
            self.buttons['metadata']=ToolsPanel.Button(rect,"images/small_normal.bmp","images/small_pressed.bmp","%s:INFORM_METADATA"%name,"Informa Meta Dados do arquivo ")
            rect=pygame.Rect((60,0),self.buttonSize)
            self.buttons['exit']=ToolsPanel.Button(rect,"images/exit.bmp","images/exit.bmp","%s:EXIT"%name,"Informa Meta Dados do arquivo ")
                

    def __init__(self,rect,jobs,nc,who_created_me="don't know",name="INFO"):
        Sprite.__init__(self,rect,who_created_me) #call Sprite initializer
        self.nc=nc #every modulle need its own copy of the netcdf file, how to maintain this up to date is REALY good question!
        self.sprites={}
        self.sprites["self"]=self #this is crazy, I know
        self.sprites["menu"]=self.Tools()
        self.sprites["menu"].loadBotton(name)
        for buttons in self.sprites["menu"].buttons.values():
            self.sprites["menu"].image.blit(buttons.normal,buttons.rect.topleft)
        self.name=name
        jobs["%s:INFORM_SPRITES"%name]=self.__INFORM_SPRITES
        jobs["%s:INFORM_FILES"%name]=self.__INFORM_FILES
        jobs["%s:INFORM_METADATA"%name]=self.__INFORM_METADATA
        jobs["%s:PAGE_UP"%name]=self.__PAGE_UP
        jobs["%s:PAGE_DOWN"%name]=self.__PAGE_DOWN
        jobs["%s:EXIT"%name]=self.__EXIT
        
        #the following is specific to this module
        self.text=[]
        self.vertical_position=0 #vertical position of the text in relation to the sprite
        self.font = pygame.font.SysFont("ubuntumono", 15)
    
    
    
    def get_job(self,job,event):
        if event.type==pygame.MOUSEBUTTONDOWN:
            if self.sprites["self"].mouse_over():
                if event.button==4:
                    return "%s:PAGE_UP"%self.name
                    
                if event.button==5:
                    return "%s:PAGE_DOWN"%self.name
            if self.sprites["menu"].mouse_over(self.rect.topleft):       
                return self.sprites['menu'].takeMouseEvents((event.pos[0]-self.rect.left,event.pos[1]-self.rect.top), event.button)
        return "NO_JOB"   
 
    
    """HANDEL_JOBS"""
    def __PAGE_UP(self,displayWindow,job,text,line):
        self.vertical_position=self.vertical_position+15
        self.__rewrite()
    def __PAGE_DOWN(self,displayWindow,job,text,line):
        self.vertical_position=self.vertical_position-15
        self.__rewrite()
    def __INFORM_SPRITES(self,displayWindow,job,text,line):
        self.text=[]
        self.vertical_position=0
        #give the programmer usefull information about the state of the Sprites in the given moment of the program, there is not standart, see current implementation

        self.text.append("EXISTING SPRITES")
        for key, sprite in spritedic.iteritems():
            self.text.append("    %s, Created by:  %s  %i" %(key,sprite.who_created_me,sprite.dirty))
        self.text.append("")
        self.text.append("%i SPRITES in displayWindow.areas_group, between them:"%len(displayWindow.areas_group))
        for key, sprite in spritedic.iteritems():
            if displayWindow.areas_group.has(sprite):
                self.text.append("    %s"%key)
        self.text.append("")
        self.text.append("%i SPRITES in displayWindow.boxes_group, between them:"%len(displayWindow.boxes_group))
        for key, sprite in spritedic.iteritems():
            if displayWindow.boxes_group.has(sprite):
                self.text.append("    %s"%key)
        self.text.append("")
        self.text.append("%i SPRITES in displayWindow.text_group, between them:"%len(displayWindow.text_group)) 
        for key, sprite in spritedic.iteritems():
            if displayWindow.text_group.has(sprite):
                self.text.append("    %s"%key)
        self.__rewrite()
    def __INFORM_FILES(self,displayWindow,job,text,line):
        self.vertical_position=0
        self.text=[]
        for name in displayWindow.ui.netcdf_path:
        	self.text.append("    %s"%name)
        self.__rewrite()
    def __INFORM_METADATA(self,displayWindow,job,text,line):
        self.vertical_position=0
        self.text=[]
        import time
        tm=time.strptime("".join(self.nc.variables['time_coverage_start'].data), "%Y-%m-%dT%H:%M:%SZ")
        self.text= ["Base Time: "+ time.asctime(tm), 
                 "Bins Number: "+ str(self.nc.dimensions['range']),
                 "Range: "+ str(self.nc.variables['range'][-1])+"m",
                 "Radar Latitude: "+ str(self.nc.variables['latitude'].data),
                 "Radar Longitude: "+ str(self.nc.variables['longitude'].data)]
        self.__rewrite()
    def __EXIT(self,displayWindow,job,text,line):
        displayWindow.ui.modules={}
    
    
    
    def __rewrite(self):
        #something like this
        dx = 15
        self.image.fill((0,0,0))
        for i,item in enumerate(self.text):
            x=self.vertical_position+i*dx
            if x>=0:
                text = self.font.render(item, 1, (255,255,255))
                self.image.blit(text, (0, x))
        self.sprites["self"].dirty=1
    
    
    
    def loop(self):
        screen = pygame.display.get_surface()
        if self.sprites["menu"].mouse_over(self.rect.topleft):
            self.sprites["menu"].dirty=1
            self.sprites["self"].dirty=2
        
        if self.sprites["self"].dirty>0:
            screen.blit(self.sprites["self"].image,self.sprites["self"].rect.topleft)
            self.sprites["self"].dirty -=1
        if self.sprites["menu"].dirty>0:
            screen.blit(self.sprites["menu"].image,(self.sprites["menu"].rect.left+self.rect.left,self.sprites["menu"].rect.top+self.rect.top))
            self.sprites["menu"].dirty -=1
                
    
    
    
