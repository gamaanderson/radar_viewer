#include <Python.h>
#include <stdio.h>
#include <netcdf.h>
#include <math.h>
#include <numpy/arrayobject.h>
#include "median.h"
//#include <time.h>
#define HALF_PI (1.570796326794897)

float range_res=100;
float azimuth_res=0.5;
float elev_res=0.1;


typedef struct {
    int id;
    size_t len;
} Dim;



static PyObject* set_resolution(PyObject* self, PyObject* args){

    PyArg_ParseTuple(args, "fff", &range_res, &azimuth_res, &elev_res);
    return  Py_BuildValue("i",0);

}
/*
static PyObject* radial(PyObject* self, PyObject* args){
    float meioGrauEmRad = 0.008726646,rt = 8500000;//raio aparente da terra
    int heightkm=16;
    int height=200,width=400,azi;
    int i,j,k,flag;
    const char *pathnc,*moment;
    PyObject* obj;
    PyArrayObject * py_palette;
    int * palette;
    float max,min;
//    clock_t start = clock(),end ;
//    printf("start one cut\n");
printf("radial cut is out of date, it is not safe to use it");
    PyArg_ParseTuple(args, "s(ii)is(Off)", &pathnc, &width, &height, &azi,&moment,&obj,&max,&min);


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj,NPY_INT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    
    

    
    
    //variable to netcdf data
    double radarRange=500000;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    
    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int range_index[max_range_index];

    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;

    
    // get moments and attributes
    short *data=(short*)malloc(sizeof(short)*n_points_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        nc_get_var_short(ncid,data_id, data);
    
    short fill_value;
    nc_get_att_short(ncid, data_id, "_FillValue", &fill_value);
    float scale;
    nc_get_att_float(ncid, data_id, "scale_factor",&scale);
    float offset;
    nc_get_att_float(ncid, data_id, "add_offset", &offset);
    



    nc_close(ncid);
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;


    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh-altitude;
    }
    float ds = radarRange/nCols,SIGMA;
    for(j=0;j<nCols;j++){
        s[j] = (0.5 + j) * ds;
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;
        
    }
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//   start=end;
    
    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    short DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
            H1 = h[i]-H0[j];
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R);
                        
            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
            
        }
    }
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;



    //lets found the index (time) of the azimuth given every_sweep
    int azi_elev[sweep_dim.len];//receve sweep number and giver ray number with needed azimuth
    float near=100000000;
    
    for(i=0;i<sweep_dim.len;i++){
        near=100000000;
        for(j=sweep_start_ray_index[i];j<sweep_end_ray_index[i]+1;j++){
            if(near>(azi-azimuth[j])*(azi-azimuth[j])){
                near=(azi-azimuth[j])*(azi-azimuth[j]);
                azi_elev[i]=j;
            }
        }
        
    }

    float lower,higher;
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    for(j=0;j<nCols;j++){
    
    
        k=0;
        flag=0;
        lower=elevation[sweep_start_ray_index[k]]*HALF_PI/90-meioGrauEmRad;
        higher=elevation[sweep_start_ray_index[k]]*HALF_PI/90+meioGrauEmRad;
        for(i=nRows-1;i>-1;i--){
        
        
            //acha a elevacao nos dados e Alimenta a matriz do corte com os dados 
            if(flag){ 
                if(ELEVS[i][j]<higher){ 
                    if(Rint[i][j]>=ray_n_gates[azi_elev[k]])Rint[i][j]=ray_n_gates[azi_elev[k]]-1;
                    DATA[i][j]=data[ray_start_index[azi_elev[k]]+Rint[i][j]];
                    }
                else{ 
                    flag=0;
                    k++;
                    if(k==sweep_dim.len){
                    higher=10000;lower=10000;
                    }
                    else{
                        lower=higher;
                        higher=elevation[sweep_start_ray_index[k]]*HALF_PI/90+meioGrauEmRad;
                    }
                    i++;
                }
            }
            
            else{  
                if(ELEVS[i][j]<lower)
                    DATA[i][j]=fill_value+10;
                else{
                    flag=1;
                    if(Rint[i][j]>=ray_n_gates[azi_elev[k]]) Rint[i][j]=ray_n_gates[azi_elev[k]]-1;
                    DATA[i][j]=data[ray_start_index[azi_elev[k]]+Rint[i][j]];
                   
                }
            
            }
            
            
        }
    }
    
//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    //map values to colors
    int pixelarray[nCols][nRows];
    float step=(max-min)/ncolors;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        if(DATA[i][j]*scale+offset>min && DATA[i][j]*scale+offset<max){
            k=floor((DATA[i][j]*scale+offset-min)/step);
            pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }
    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

//    end= clock() ;
//    printf(" time relaps: %f\n",(end-start)/(double)CLOCKS_PER_SEC) ;
//    start=end;
    
    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);
    free(data);
    
    
    
    
    
    return array;
}

*/




static PyObject* vertical2(PyObject* self, PyObject* args){


    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2,*obj3;
    PyArrayObject * py_palette,*py_step, *py_moment;
    int * palette;
    float * step,*data;
    int smooth;
    float fill_value;
    PyArg_ParseTuple(args, "s(ii)(ff)(ff)i(Of)(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&smooth,&obj3,&fill_value,&obj1,&obj2);

    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);
    py_moment=(PyArrayObject * )PyArray_ContiguousFromAny(obj3,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);
    data=(float *)PyArray_DATA(py_moment);

    //variable to netcdf data
    //double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_elev_index=floor(90.2/elev_res)+1;
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int elev_index[max_elev_index];
    int azimuth_index[sweep_dim.len][max_azimuth_index];

    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[i][k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    float current,next;
    k=0;
    current=elevation[sweep_start_ray_index[k]];
    if(sweep_dim.len>1) next=elevation[sweep_start_ray_index[k+1]];
    else next=1000;
    
    for(i=0;i<max_elev_index;i++){
           //acha a elevacao 
        if(fabs(i*elev_res-0.2-current)<fabs(i*elev_res-0.2-next)){
            if(fabs(i*elev_res-0.2-current)<5){
            elev_index[i]=k;
            }
            else{
            elev_index[i]=-1;
            }
        }   
        else{
//            printf("BREAK AT %i %i %f %f %f %f %f %i \n",k,i,i*elev_res,current,next,fabs(i*elev_res-current),fabs(i*elev_res-next),abs(i*elev_res-current)<abs(i*elev_res-next));
            k++;
            i--;
            current=next;
            if(k+1>=sweep_dim.len) next=10000;
            else next=elevation[sweep_start_ray_index[k+1]];
        }

        
    }
    k=0;
    int azi1,azi2;
    for(i=0;i<sweep_dim.len;i++){
        for(k=0;k<sweep_end_ray_index[i]-sweep_start_ray_index[i]+1;k++){
            azi1=floor((azimuth[sweep_start_ray_index[i]+k]-0.5)/azimuth_res);
            azi2=ceil((azimuth[sweep_start_ray_index[i]+k]+1.5)/azimuth_res);
//            if(azi1<0) azi1=0;
//            if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
            for(j=azi1;j<azi2;j++) azimuth_index[i][j%max_azimuth_index]=sweep_start_ray_index[i]+k;
        }
    }


    //radarRange=range[range_dim.len-1]; //maximal range in meters


    nc_close(ncid);

    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    int azi[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh -altitude;
    }
    float SIGMA, t,a;
    for(j=0;j<nCols;j++){
        t=(j*1.0/(nCols-1));
        s[j] = sqrt(pow((1-t)*X1+t*X2,2)+pow((1-t)*Y1+t*Y2,2));
        a=atan2((1-t)*X1+t*X2,(1-t)*Y1+t*Y2)*90/HALF_PI;
        if(a<0) a=a+360;
        azi[j]=floor(a/azimuth_res);//index for azimuth_index
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;

    }

    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    float DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
 
            H1 = h[i]-H0[j];
            
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R)*90/HALF_PI;//in degrees

            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
        }
    }


    int E;
    for(i=0;i<nRows;i++){
            //printf("%f %i %f %i\n",ELEVS[i][0],E,azi[0]*azimuth_res,Rint[i][0]);
        for(j=0;j<nCols;j++){
            if(ELEVS[i][j]<-0.2) E=-1;
            else E=elev_index[(int)floor((ELEVS[i][j]+0.2)/elev_res)];
            if(E<0 || azimuth_index[E][azi[j]]<0 || Rint[i][j]<0 || Rint[i][j]>=ray_n_gates[azimuth_index[E][azi[j]]] ){DATA[i][j]=fill_value+10;}
            else {
                DATA[i][j]=data[ray_start_index[azimuth_index[E][azi[j]]]+Rint[i][j]];}
        }
    }
    
    if (smooth==1) median_filter_float (DATA, nRows,nCols, 1);     

    //map values to colors
    int pixelarray[nCols][nRows];
    float value;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        value=DATA[i][j];
        if(value>step[0] && value<step[ncolors]){
           k=0;
           while(value>step[k+1]) k++;
           pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }

    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);

    return array;
}


static PyObject* vertical1(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2,*obj3;
    PyArrayObject * py_palette,*py_step, *py_moment;
    int * palette;
    float * step,*data;
    int smooth;
    float fill_value;
    PyArg_ParseTuple(args, "s(ii)(ff)(ff)i(Of)(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&smooth,&obj3,&fill_value,&obj1,&obj2);

    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);
    py_moment=(PyArrayObject * )PyArray_ContiguousFromAny(obj3,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);
    data=(float *)PyArray_DATA(py_moment);

    //variable to netcdf data
    //double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
/*    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
  */  int data_id;
    int altitude_id;
    double altitude;
        nc_inq_varid(ncid,"altitude", &altitude_id);
        nc_get_var_double(ncid, altitude_id, &altitude);

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_elev_index=floor(90.2/elev_res)+1;
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int elev_index[max_elev_index];
    int azimuth_index[sweep_dim.len][max_azimuth_index];

    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[i][k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;


    float current,next;
    k=0;
    current=elevation[sweep_start_ray_index[k]];
    if(sweep_dim.len>1) next=elevation[sweep_start_ray_index[k+1]];
    else next=1000;
    for(i=0;i<max_elev_index;i++){
           //acha a elevacao 

        if(fabs(i*elev_res-0.2-current)<fabs(i*elev_res-0.2-next)){
            if(fabs(i*elev_res-0.2-current)<5){
            elev_index[i]=k;
            }
            else{
            elev_index[i]=-1;
            }
        }   
        else{
//            printf("BREAK AT %i %i %f %f %f %f %f %i \n",k,i,i*elev_res,current,next,fabs(i*elev_res-current),fabs(i*elev_res-next),abs(i*elev_res-current)<abs(i*elev_res-next));
            k++;
            i--;
            current=next;
            if(k+1>=sweep_dim.len) next=10000;
            else next=elevation[sweep_start_ray_index[k+1]];
        }

        
    }
   
    k=0;
    int azi1,azi2;
    for(i=0;i<sweep_dim.len;i++){
        for(k=0;k<sweep_end_ray_index[i]-sweep_start_ray_index[i]+1;k++){
            azi1=floor((azimuth[sweep_start_ray_index[i]+k]-0.5)/azimuth_res);
            azi2=ceil((azimuth[sweep_start_ray_index[i]+k]+1.5)/azimuth_res);
//            if(azi1<0) azi1=0;
//            if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
            for(j=azi1;j<azi2;j++) azimuth_index[i][j%max_azimuth_index]=sweep_start_ray_index[i]+k;
        }
    }

    // get moments and attributes
    //radarRange=range[range_dim.len-1]; //maximal range in meters


    nc_close(ncid);

    //criating data matriz
    int nRows = height;
    int nCols = width;
    float h[nRows],s[nCols],R0[nCols],BETHA[nCols],H0[nCols];
    int azi[nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0
    float dh=heightkm * 1000/nRows;
    for(i=0;i<nRows;i++){
        h[nRows-i-1] = (0.5 + i) * dh -altitude;
    }
    float SIGMA, t,a;
    for(j=0;j<nCols;j++){
        t=(j*1.0/(nCols-1));
        s[j] = sqrt(pow((1-t)*X1+t*X2,2)+pow((1-t)*Y1+t*Y2,2));
        a=atan2((1-t)*X1+t*X2,(1-t)*Y1+t*Y2)*90/HALF_PI;
        if(a<0) a=a+360;
        azi[j]=floor(a/azimuth_res);//index for azimuth_index
        SIGMA = s[j]/rt;
        R0[j] = rt*tan(SIGMA);
        BETHA[j] =HALF_PI+ SIGMA;
        H0[j] = sqrt(R0[j]*R0[j]+rt*rt)-rt;

    }

    //converte altura e distancia em elevacão e range
    float H1,R,ELEVS[nRows][nCols];
    float DATA[nRows][nCols];
    int Rint[nRows][nCols],Ri;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
 
            H1 = h[i]-H0[j];
            
            R = sqrt(R0[j]*R0[j] + H1*H1 - 2*R0[j]*H1*cos(BETHA[j]));
            ELEVS[i][j] = asin(H1*sin(BETHA[j])/R)*90/HALF_PI;//in degrees

            Ri=floor(R/range_res);
            if(Ri>=max_range_index) Rint[i][j]=-1;
            else Rint[i][j] =range_index[Ri];
        }
    }


    int E;

    for(i=0;i<nRows;i++){
            //printf("%f %i %f %i\n",ELEVS[i][0],E,azi[0]*azimuth_res,Rint[i][0]);
        for(j=0;j<nCols;j++){
            if(ELEVS[i][j]<-0.2) E=-1;
            else {E=elev_index[(int)floor((ELEVS[i][j]+0.2)/elev_res)];}
            if(E<0 || azimuth_index[E][azi[j]]<0 || Rint[i][j]<0 || Rint[i][j]>=range_dim.len){DATA[i][j]=fill_value+10;}
            else {
                DATA[i][j]=data[range_dim.len*(azimuth_index[E][azi[j]])+Rint[i][j]];}
        }
    }
    
    if (smooth==1) median_filter_float (DATA, nRows,nCols, 1); 

    //map values to colors
    int pixelarray[nCols][nRows];
    float  value;
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
        value=DATA[i][j];
        if(value>step[0] && value<step[ncolors]){
           k=0;
           while(value>step[k+1]) k++;
           pixelarray[j][i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
        }
        else if(DATA[i][j]==fill_value+10)
            pixelarray[j][i]=110*256*256+110*256+110;
        else pixelarray[j][i]=0;
        
        }
    }

    npy_intp dims[2]={nCols,nRows};
    PyObject* array=PyArray_SimpleNewFromData(2,dims,NPY_UINT32,&pixelarray[0][0]);
    

    
    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);


  
    

    return array;
}


static PyArrayObject * PPI2(PyObject* self, PyObject* args){
    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2,*obj3;
    PyArrayObject * py_palette,*py_step, *py_moment;
    int * palette;
    float * step,*data;
    int elev;
    int smooth;
    float fill_value;
    PyArg_ParseTuple(args, "s(ii)(ii)(ii)ii(Of)(OO)", &pathnc, &height, &width,&left,&top,&right,&bottom, &elev,&smooth,&obj3,&fill_value,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);
    py_moment=(PyArrayObject * )PyArray_ContiguousFromAny(obj3,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);
    data=(float *)PyArray_DATA(py_moment);


    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim n_points_dim;
        nc_inq_dimid(ncid, "n_points", &n_points_dim.id);
        nc_inq_dimlen(ncid, n_points_dim.id,&n_points_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);
        
    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int ray_n_gates_id;
    int *ray_n_gates=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_n_gates", &ray_n_gates_id);
        nc_get_var_int(ncid, ray_n_gates_id, ray_n_gates);
    int ray_start_index_id;
    int * ray_start_index=(int*)malloc(sizeof(int)*time_dim.len);
        nc_inq_varid(ncid,"ray_start_index", &ray_start_index_id);
        nc_get_var_int(ncid, ray_start_index_id, ray_start_index);
    int data_id;

    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int azimuth_index[max_azimuth_index];

    //fill with -1
    for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[k]=-1;


    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    k=0;
    int azi1,azi2;
    for(k=sweep_start_ray_index[elev];k<sweep_end_ray_index[elev]+1;k++){
        azi1=floor((azimuth[k]-1)/azimuth_res);
        azi2=ceil((azimuth[k]+1)/azimuth_res);
//        if(azi1<0) azi1=0;
//       if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
        for(j=azi1;j<azi2;j++){ azimuth_index[j%max_azimuth_index]=k;
        }
    }



    
    radarRange=500000; //maximal range in meters

    nc_close(ncid);

    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    float s,R0,R,BETHA,ALPHA;
    int azi;
    int Rint,Ri;

    // cria os elementos do vetor h, s, R0, BETHA e H0

    float SIGMA,a,x,y;
    float DATA[nRows][nCols];
    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];
    float value;
    

    for(j=0;j<nCols;j++){
        for(i=0;i<nRows;i++){ 
            x=-radarRange+(j+left)*2*radarRange/height;
            y=radarRange-(i+top)*2*radarRange/width;
            
            s = sqrt(x*x+y*y);
            a=atan2(x,y)*90/HALF_PI;
            if(a<0) a=a+360;
            azi=floor(a/azimuth_res);
            SIGMA = s/rt;
            R0 = rt*tan(SIGMA);
            BETHA =HALF_PI+ SIGMA;
            ALPHA =HALF_PI- SIGMA-elevation[azimuth_index[azi]]*HALF_PI/90;
            R = R0 * sin(BETHA) / sin(ALPHA); 
            Ri=floor(R/range_res);
            if(R>=range[ray_n_gates[azimuth_index[azi]]-1] || R<0) {
               DATA[i][j]=fill_value+10;
            }
            else {
                Rint =range_index[Ri];
                DATA[i][j]=data[ray_start_index[azimuth_index[azi]]+Rint];
            }
             //map values to colors

        }
    }


    if (smooth==1) median_filter_float (&DATA[0][0], nRows,nCols, 1); 
    

    for(j=0;j<nCols;j++){
        for(i=0;i<nRows;i++){ 
            value=DATA[i][j];
            if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[j*nRows+i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else if(DATA[i][j]==fill_value+10)
                pixelarray[j*nRows+i]=1*256*256+1*256+1;
            else pixelarray[j*nRows+i]=0;
        }
    }

    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);
    free(ray_n_gates);
    free(ray_start_index);
    return array;
}



static PyArrayObject * PPI1(PyObject* self, PyObject* args){

 
    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2,*obj3;
    PyArrayObject * py_palette,*py_step, *py_moment;
    int * palette;
    float * step,*data;
    int elev;
    int smooth;
    float fill_value;
    PyArg_ParseTuple(args, "s(ii)(ii)(ii)ii(Of)(OO)", &pathnc, &height, &width,&left,&top,&right,&bottom, &elev,&smooth,&obj3,&fill_value,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);
    py_moment=(PyArrayObject * )PyArray_ContiguousFromAny(obj3,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);
    data=(float *)PyArray_DATA(py_moment);


    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim range_dim;
        nc_inq_dimid(ncid, "range", &range_dim.id);
        nc_inq_dimlen(ncid, range_dim.id,&range_dim.len);
    Dim time_dim;
        nc_inq_dimid(ncid, "time", &time_dim.id);
        nc_inq_dimlen(ncid, time_dim.id,&time_dim.len);
    Dim sweep_dim;
        nc_inq_dimid(ncid, "sweep", &sweep_dim.id);
        nc_inq_dimlen(ncid, sweep_dim.id,&sweep_dim.len);

    //get netcdf variables

    int range_id;
    float *range=(float*)malloc(sizeof(float)*range_dim.len);
        nc_inq_varid(ncid,"range", &range_id);
        nc_get_var_float(ncid,range_id, range);
    int azimuth_id;
    float *azimuth=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"azimuth", &azimuth_id);
        nc_get_var_float(ncid,azimuth_id, azimuth);
    int elevation_id;
    float *elevation=(float*)malloc(sizeof(float)*time_dim.len);
        nc_inq_varid(ncid,"elevation", &elevation_id);
        nc_get_var_float(ncid, elevation_id, elevation);
    //for(i=0;i<time_dim.len;i++) elevation[i]=0.31860;
    int sweep_start_ray_index_id;
    int *sweep_start_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_start_ray_index", &sweep_start_ray_index_id);
        nc_get_var_int(ncid, sweep_start_ray_index_id, sweep_start_ray_index);
    int sweep_end_ray_index_id;
    int *sweep_end_ray_index=(int*)malloc(sizeof(int)*sweep_dim.len);
        nc_inq_varid(ncid,"sweep_end_ray_index", &sweep_end_ray_index_id);
        nc_get_var_int(ncid, sweep_end_ray_index_id, sweep_end_ray_index);
    int data_id;


    int max_range_index=ceil(range[range_dim.len-1]/range_res);
    int max_azimuth_index=ceil(360/azimuth_res);
    int range_index[max_range_index];
    int azimuth_index[max_azimuth_index];


    //fill with -1
    for(i=0;i<sweep_dim.len;i++) for(k=0;k<ceil(360/azimuth_res);k++) azimuth_index[k]=-1;

    // fill index arrays
    j=0;
    for(i=0;i<max_range_index-1;i++){
        while(i*range_res>range[j]) j++;
        range_index[i]=j;    
    }
    range_index[max_range_index-1]=range_dim.len-1;
    j=0;

    k=0;
    int azi1,azi2;
    for(k=sweep_start_ray_index[elev];k<sweep_end_ray_index[elev]+1;k++){
        azi1=floor((azimuth[k]-1)/azimuth_res);
        azi2=ceil((azimuth[k]+1)/azimuth_res);
//        if(azi1<0) azi1=0;
//        if(azi2>ceil(360/azimuth_res)) azi2=ceil(360/azimuth_res);
       // printf("\n %f, %i,  %i\n",azimuth[k],azi1,azi2);
        for(j=azi1;j<azi2;j++){ azimuth_index[j%max_azimuth_index]=k;//printf("%i ",k);printf("haqha");
        }
    }



    
    radarRange=500000; //maximal range in meters

    nc_close(ncid);


    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    float s,R0,R,BETHA,ALPHA;
    int azi;
    int Rint,Ri;

    // cria os elementos do vetor h, s, R0, BETHA e H0

    float SIGMA,a,x,y;
    float DATA[nRows][nCols];
    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];
    float value;


    
    for(j=0;j<nCols;j++){
        for(i=0;i<nRows;i++){ 

            x=-radarRange+(j+left)*2*radarRange/height;
            y=radarRange-(i+top)*2*radarRange/width;

            s = sqrt(x*x+y*y);
            a=atan2(x,y)*90/HALF_PI;
            if(a<0) a=a+360;
            azi=floor(a/azimuth_res);//index for azimuth_index
            SIGMA = s/rt;
            R0 = rt*tan(SIGMA);
            BETHA =HALF_PI+ SIGMA;
            ALPHA =HALF_PI- SIGMA-elevation[azimuth_index[azi]]*HALF_PI/90;

            R = R0 * sin(BETHA) / sin(ALPHA); 
            Ri=floor(R/range_res);

            if(R>=range[range_dim.len-1]) {
               DATA[i][j]=fill_value+10;
            }
            else if(azimuth_index[azi]<0) DATA[i][j]=fill_value+10;
            else {
                Rint =range_index[Ri];
                DATA[i][j]=data[azimuth_index[azi]*range_dim.len+Rint];
                
            }
            
        }
    }
        
    if (smooth==1) median_filter_float (DATA, nRows,nCols, 1); 
    
    
    for(j=0;j<nCols;j++){
        for(i=0;i<nRows;i++){            
             //map values to colors
            value=DATA[i][j];
            if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[j*nRows+i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else if(DATA[i][j]==fill_value+10)
                pixelarray[j*nRows+i]=1*256*256+1*256+1;
            else pixelarray[j*nRows+i]=0;

        }
    }


    free(range);
    free(azimuth);
    free(elevation);
    free(sweep_start_ray_index);
    free(sweep_end_ray_index);

    
    

    return array;
}





/*  define functions in module */
static PyMethodDef NetcdfMethods[] =
{
//     {"radial", radial, METH_VARARGS, "make radial cut"},
     {"vertical1", vertical1, METH_VARARGS, "make vertical cut for netcdf format 1.1"},
     {"vertical2", vertical2, METH_VARARGS, "make vertical cut for netcdf format 1.2"},
     {"PPI1", PPI1, METH_VARARGS, "make PPI for netcdf format 1.1"},
     {"PPI2", PPI2, METH_VARARGS, "make PPI for netcdf format 1.2"},
     {"set_resolution",set_resolution,METH_VARARGS,"Set Resolution for (range,azimuth,elevation) in order to make the images"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC

initnetcdf_module(void)
{
     (void) Py_InitModule("netcdf_module", NetcdfMethods);
      /* IMPORTANT: this must be called */
     import_array();
}
