
import pygame
import numpy as np
from math import *
from Sprites import *
import ToolsPanel
from palette import palette
from palette import polarization_pairs
from palette import static
from palette import HMC_legend
from Colorbar import Colorbar
from Text import Text
from Text import writePixel
import GeraProds

class Compare:

    class MomentPanel(ToolsPanel.ToolsPanel):

        def __init__(self,father,name):
            self.father=father
            self.name=name
            width = 90
            height = 30
            # canto superior esquerdo
            left=0
            top = 0 
            ToolsPanel.ToolsPanel.__init__(self,pygame.Rect(left,top,width,height),who_created_me=father.name)   
            rect=pygame.Rect((5,5),(22,22))
            self.buttons["horizontal"]=ToolsPanel.Button(rect,"images/horizontal.bmp","%s:SYNCHRONIZE_HORIZONTAL"%name,"synchronize_horizontal")
            rect=pygame.Rect((35,5),(22,22))
            self.buttons["vertical"]=ToolsPanel.Button(rect,"images/vertical.bmp","%s:SYNCHRONIZE_VERTICAL"%name,"synchronize_vertical")
            rect=pygame.Rect((65,5),(22,22))
            self.buttons["redraw"]=ToolsPanel.Button(rect,"images/view-refresh.bmp","%s:REDRAW"%name,"redraw")




    def __init__(self,rect,jobs,GeraImages,(nc,elev,moment,P),convertion_to_radar,name="Compare"):
    	self.name=name
        self.rect=rect
        self.GeraImages=GeraImages
        self.convertion_to_radar=convertion_to_radar
        self.font = pygame.font.Font("images/Regular.ttf", 14)
        self.outfont = pygame.font.Font("images/Outline.ttf", 14)
        pos=pygame.mouse.get_pos()
        self.pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        self.image=pygame.display.get_surface().subsurface(self.rect)
        rect=self.rect.inflate(-64,-64)
        surf=pygame.display.get_surface().subsurface(rect)
        self.cutSprite=Sprite(image=surf,pos=(rect.left-self.rect.left,rect.top-self.rect.top),who_created_me=name)
        self.scaleFactor=1
        self.scaledDataSprite=Sprite(rect=pygame.Rect((0,0),rect.size),who_created_me=name)
        self.radarDataSurf=self.cutSprite.image.copy()
        self.new_nc=nc #this is a list and can eventualy change externaly, if change, it will update nc
        self.nc_list=[nc[0],nc[0],None] #actual, class internal nc, will update in loop
        self.new_moment=moment
        self.moment_list=[moment[0],moment[0],moment[0]]
        self.new_elev=elev
        self.elev_list=[elev[0],elev[0],elev[0]]
        self.switch_index=2
#        self.new_threshold=threshold
#        self.threshold=threshold[0]
        self.new_P=P
        self.P=[(None,None),(None,None),(None,None)]
#        self.new_Proj=Proj
#        self.Proj=(None,None)
        self.value=[-999]
        self.jobs=jobs
        self.dirty=[1]
        import collections
        self.cutSurfList = collections.deque(maxlen=30)
        self.panOn=False
        self.sprites={}
        self.smooth=False
        self.valueH=0
        self.valueV=0
        
        self.sprites["vertical_scale"]=Sprite(rect=pygame.Rect((0,32),(32,rect.height)),who_created_me=name)
        self.sprites["horizontal_scale"]=Sprite(rect=pygame.Rect((32,32+rect.height),(rect.width,32)),who_created_me=name)
        self.sprites["title"]=Sprite(rect=pygame.Rect((rect.width/4,0),(rect.width/2,32)),who_created_me=name)
        w = self.font.size("DBZH:   No Value dbz")[0]        
        self.sprites["menu"]=self.MomentPanel(self,name)
        self.__writeTitle()
        self.__writeVerticalScale()
        self.__writeHorizontalScale()
#        self.sprites["menu"]=self.MomentPanel(self)
#        self.sprites['colorbar']=Colorbar(moment[0],threshold[0],rect=pygame.Rect(5,20,35,self.rect.height-40),who_created_me="PPI")   
#        self.text=Text(self)
#        self.text.printElev(elev[0],nc[0].variables['elevation'].getValue()[nc[0].variables['sweep_start_ray_index'].getValue()[elev[0]]])
        
        
        
#        jobs["%s:DRAW_LINE_ON"%name]=self.__INFORM_SPRITES
#        jobs["%s:DRAW_LINE_OFF"%name]=self.__INFORM_FILES
#        jobs["%s:INFORM_METADATA"%name]=self.__INFORM_METADATA
        jobs["%s:ZOOM_IN"%name]=self.__ZOOM
        jobs["%s:ZOOM_OUT"%name]=self.__ZOOM
        jobs["%s:PAN_ON"%name]=self.__PAN_ON
        jobs["%s:PAN_OFF"%name]=self.__PAN_OFF
#        jobs["%s:THRESHOLD_UP"%name]=self.__MOVE_THRESHOLD
#        jobs["%s:THRESHOLD_DOWN"%name]=self.__MOVE_THRESHOLD
        jobs["%s:REDRAW"%name]=self.__REDRAW
        jobs["%s:SMOOTH ON/OFF"%name]=self.__SMOOTH_ON_OFF
        jobs["%s:SYNCHRONIZE_HORIZONTAL"%name]=self.__SYNCHRONIZE
        jobs["%s:SYNCHRONIZE_VERTICAL"%name]=self.__SYNCHRONIZE
        jobs["%s:MOMENT_COMP"%name]=self.__MOMENT_COMP

    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf=self.rect.collidepoint(pos[0],pos[1])
        if event.type == KEYDOWN and over_surf:
            if event.key ==pygame.K_i:##< press i releases zoom in
                return "%s:ZOOM_IN"%self.name
            if event.key ==pygame.K_o:##< press o releases zoom out
                return "%s:ZOOM_OUT"%self.name
#            if event.key ==pygame.K_UP:##< press up arrow releases elevation up
#               return "%s:ELEV_UP"%self.name
#            if event.key ==pygame.K_DOWN:##< press down arrow releases elevation down
#                return "%s:ELEV_DOWN"%self.name
            if event.key ==pygame.K_r:##< press r releases redraw PPI
                return "%s:REDRAW"%self.name
            if event.key ==pygame.K_p:##< press p start or stop pan
                if self.panOn:
                    return "%s:PAN_OFF"%self.name
                else:
                    return "%s:PAN_ON"%self.name
            if event.key ==pygame.K_s:##< press p start or stop pan
                return "%s:SMOOTH ON/OFF"%self.name

        #----------------------------------------------
        # DEFINE AS ACOES DOS BOTOES E RODINHA DO MOUSE
        #----------------------------------------------
        # ALGUM BOTAO ESTA SENDO PRESSIONADO

        if event.type==pygame.MOUSEBUTTONDOWN:

            if self.sprites['menu'].mouse_over(self.rect.topleft): #mouse over menu
                return self.sprites['menu'].takeMouseEvents((event.pos[0]-self.rect.left,event.pos[1]-self.rect.top), event.button)  
#            elif over_surf:
#                return "%s:MOMENT_COMP"%self.name
                
        return "NO_JOB"
        
    
    """HANDEL_JOBS"""
    """ZOOM"""
    def __ZOOM(self,job):
            pos=pygame.mouse.get_pos()
            pos=(pos[0]-self.rect.left-self.cutSprite.rect.left,pos[1]-self.rect.top-self.cutSprite.rect.top)
            ## before zooming save relative position of the Line
            #XYOr=GeraProds.screenToRelative((line.X0,line.Y0),displayWindow)
            #XY1r=GeraProds.screenToRelative((line.X1,line.Y1),displayWindow)
            if job=="%s:ZOOM_IN"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width<25: factor=1.1 
            elif job=="%s:ZOOM_OUT"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width>1: factor=0.9
            else: return
            scaledResX = int(self.scaledDataSprite.rect.width*factor)
            scaledResY = int(self.scaledDataSprite.rect.height*factor)
            # calcula a nova position em funcao da posicao do mouse
            newPosX = -int((pos[0]-self.scaledDataSprite.rect.left)*(factor-1)) 
            newPosY = -int((pos[1]-self.scaledDataSprite.rect.top)*(factor-1))
            self.scaledDataSprite.rect.size=(scaledResX, scaledResY)
            self.scaledDataSprite.rect.move_ip(newPosX, newPosY)
            ## restore Line in proper position after zomming
            #line.setStartPoint(GeraProds.relativeToScreen(XYOr,displayWindow))#restore line after zoom
            #line.setEndPoint(GeraProds.relativeToScreen(XY1r,displayWindow))
            ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
            self.up_to_date=False
            self.dirty[0]=1
            self.lastChangeTime = pygame.time.get_ticks()
            print self.scaledDataSprite.rect.size
    """PAN_ON"""
    def __PAN_ON(self,job):
        ## inicialisate pygame's relative mouse moviment
        pan = pygame.mouse.get_rel()
        ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False
        ## turn pan on
        self.panOn=True
        self.dirty[0]=2
    """PAN_OFF"""
    def __PAN_OFF(self,job):
        self.panOn=False
        self.dirty[0]=0
    """REDRAW"""
    def __REDRAW(self,job): 
        ## lock mouse buttons for updating PPI
        pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
        #try:
        P0=self.convertion_to_radar(self.P[0])
        P1=self.convertion_to_radar(self.P[1])
        surf = self.GeraImages.make_dispersion((self.nc_list,self.elev_list,self.moment_list,(P0,P1),self.smooth), self.scaledDataSprite.rect,self.scaledDataSprite.rect)
        self.scaledDataSprite.blit(surf,(0,0))
#                         ((-1)*self.scaledDataSprite.rect.left,
#                         (-1)*self.scaledDataSprite.rect.top))
        #except:
        #    displayWindow.updatePPI(self.nc,self.elev,self.moment)
        pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN) 
        self.dirty[0]=1     
#        self.sprites['colorbar'].reset(self.new_moment[0])
    """SMOOTH_ON_OFF"""    
    def __SMOOTH_ON_OFF(self,job):
        if self.smooth==False:
            self.smooth=True
        else:
            self.smooth=False
        self.up_to_date=False                

    def __SYNCHRONIZE(self,job):
        if job=="%s:SYNCHRONIZE_HORIZONTAL"%self.name:
            self.nc_list[0]=self.new_nc[0] 
            self.moment_list[0]=self.new_moment[0]
            self.elev_list[0]=self.new_elev[0]
        elif job=="%s:SYNCHRONIZE_VERTICAL"%self.name:
            self.nc_list[1]=self.new_nc[0] 
            self.moment_list[1]=self.new_moment[0]
            self.elev_list[1]=self.new_elev[0]
        self.__writeTitle()
        self.__writeVerticalScale()
        self.__writeHorizontalScale()
    
    def __MOMENT_COMP(self,job):
        self.new_moment[0]="COMP"
        
    def loop(self):
        if self.nc_list[self.switch_index]!=self.new_nc[0] or self.moment_list[self.switch_index]!=self.new_moment[0] or self.elev_list[self.switch_index]!=self.new_elev[0]:
            pass
#            if self.switch_index!=2:
#                print "update"
#                self.nc_list[self.switch_index]=self.new_nc[0] 
#                self.moment_list[self.switch_index]=self.new_moment[0]
#                self.elev_list[self.switch_index]=self.new_elev[0]
#                self.__moveSpaceTime(self.nc_list,self.moment_list,self.P)
#                self.__writeTitle()
#                self.__writeVerticalScale()
#                self.__writeHorizontalScale()
        if self.scaledDataSprite.image.get_size() != self.scaledDataSprite.rect.size:
            self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
            self.lastChangeTime = pygame.time.get_ticks()
            self.up_to_date=False
        if self.P[0]!=self.new_P[0] or self.P[1]!=self.new_P[1]:
            self.P[0]=self.new_P[0]
            self.P[1]=self.new_P[1]
            self.up_to_date=False
            self.point_changed=True
            self.lastChangeTime = pygame.time.get_ticks()

        elif self.up_to_date==False and (pygame.time.get_ticks() - self.lastChangeTime)>=0:##< @anchor zoom->redraw if PPI is not up to date and no move has been done in the last 0.6 
            if self.point_changed:
                self.__moveSpaceTime(self.nc_list,self.moment_list,self.P)
                self.point_changed=False
                self.up_to_date=True
            else:
                self.up_to_date=True
                self.__REDRAW(None)


        if self.panOn:
            self.lastChangeTime = pygame.time.get_ticks()
            pan = pygame.mouse.get_rel() #movimento acumulado do mouse
            self.scaledDataSprite.rect.move_ip(pan[0],pan[1])
            #line.setStartPoint((line.X0+pan[0],line.Y0+pan[1]))#move line with surface
            #line.setEndPoint((line.X1+pan[0],line.Y1+pan[1]))#move line with surface


        if self.sprites['menu'].mouse_over(self.rect.topleft):
            pos=pygame.mouse.get_pos()
            self.sprites['menu'].update((pos[0]-self.rect.left,pos[1]-self.rect.top))
            self.sprites['menu'].dirty=1

        pos=pygame.mouse.get_pos()
        pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        if self.pos!=pos: #no need to atualise what didn't change
            self.pos = pos
            if self.cutSprite.rect.collidepoint(pos):
                self.dirty[0]=1
            print self.moment_list,self.switch_index

        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__update()
        else:            
            for sprite in self.sprites.values():
               sprite.write(self.image,None)


    
    
    def __update(self):

        self.image.fill((150,150,150))
        self.scaledDataSprite.blit_in(self.cutSprite.image)
        for sprite in self.sprites.values():
            if sprite.dirty>-1:
                sprite.blit_in(self.image)
        

        (valueH,valueV)=self.convert_to_value(self.pos)

        moment=self.moment_list[0]
        length=len(palette[moment][0])
#        valueH=int(1.0*(self.pos[0]-self.cutSprite.rect.left)/self.cutSprite.rect.width*length)
        t=self.__getIntervalValue(moment,valueH)
        surf=self.image
        rect=self.sprites["horizontal_scale"].rect
        h = float(rect.width)/(length)
        writePixel(surf,self.font,self.outfont,(int(valueH*h)+rect.left,rect.top),(255,255,255),(0,0,0),t)
        a=(int((valueH+0.5)*h),0)
        b=(int((valueH+0.5)*h),self.cutSprite.rect.height)     
        pygame.draw.aaline(self.cutSprite.image, (255,0,0), a,b)
        
        
        moment=self.moment_list[1]
        length=len(palette[moment][0])
#        valueV=length-1-int(1.0*(self.pos[1]-self.cutSprite.rect.top)/self.cutSprite.rect.height*length)
        t=self.__getIntervalValue(moment,valueV)
        surf=self.image
        rect=self.sprites["vertical_scale"].rect
        h = float(rect.height)/(length)
        writePixel(surf,self.font,self.outfont,(rect.left,rect.top+int((length-1-valueV)*h)),(255,255,255),(0,0,0),t)
        a=(0,int((length-0.5-valueV)*h))
        b=(self.cutSprite.rect.width,int((length-0.5-valueV)*h))    
        pygame.draw.aaline(self.cutSprite.image, (255,0,0), a,b)

        self.valueH=valueH
        self.valueV=valueV
        
        
        
    def __moveSpaceTime(self,nc_list,moment_list,P):
        return
        i=next((i for i,tupleSurf in enumerate(self.cutSurfList) if tupleSurf[0:3] == (nc.filename,moment,P)), None) # using generator to search in deque ppiSurfList
        if i==None:
            P0=self.convertion_to_radar(self.P[0])
            P1=self.convertion_to_radar(self.P[1])
            self.cutSurfList.append((nc.filename,moment,(P0,P1),self.radarDataSurf.copy()))#first put one-more element in the list
            self.radarDataSurf=self.cutSurfList[-1][3]#now point to it and then update
            pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
            self.__updateCutfull()
            pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)
            
        else:
            self.radarDataSurf=self.cutSurfList[i][3]#now point to it      
        self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
        self.dirty[0]=1   
            
    def __updateCutfull(self): 
        ##_________funciona
        P0=self.convertion_to_radar(self.P[0])
        P1=self.convertion_to_radar(self.P[1])
#        cutSurf = self.GeraImages.makeVerticalCut((self.nc,self.moment,(P0,P1),0,False), self.cutSprite.image.get_rect(),self.cutSprite.rect,self.topoPath)
#        self.__writeVerticalScale()
#        self.scaledDataSprite.image.blit(pygame.transform.smoothscale(cutSurf, self.scaledDataSprite.rect.size),(0,0));
#        self.radarDataSurf.blit(cutSurf,(0,0))
            
    def __writeVerticalScale(self):
        surf=self.sprites["vertical_scale"].image
        surf.fill((150,150,150))
        rect=self.sprites["vertical_scale"].rect
        moment=self.moment_list[1]
        h = float(rect.height)/(len(palette[moment][0]))
        width=rect.width
        
        length=len(palette[moment][0])
        for i in range(length):
            level = pygame.draw.rect(surf, palette[moment][0][len(palette[moment][0])-1-i], (0, int(i*h), width, int((i+1)*h)-int(i*h)), 0)
            continue
            #dont write numbers, it is all too big
            if abs(palette[moment][1][-1])>10: t = "%.0f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
            elif abs(palette[moment][1][-1])>1: t = "%.1f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
            else: t = "%.2f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
            pos = (0 , int(i*h))
            color = (255,255,255)
            
            writePixel(surf,self.font,self.outfont,pos,color,(0,0,0),t)
        self.sprites["vertical_scale"].dirty=1



    def __writeHorizontalScale(self):
        surf=self.sprites["horizontal_scale"].image
        surf.fill((150,150,150))
        rect=self.sprites["horizontal_scale"].rect
        moment=self.moment_list[0]
        h = float(rect.width)/(len(palette[moment][0]))
        height=rect.height
        
        length=len(palette[moment][0])
        for i in range(length):
            level = pygame.draw.rect(surf, palette[moment][0][i], (int(i*h), 0, int((i+1)*h)-int(i*h),height), 0)
            continue
            #dont write numbers, it is all too big
            if abs(palette[moment][1][-1])>10: t = "%.0f"%((palette[moment][1][i+1]+palette[moment][1][i])/2)
            elif abs(palette[moment][1][-1])>1: t = "%.1f"%((palette[moment][1][i+1]+palette[moment][1][i])/2)
            else: t = "%.2f"%((palette[moment][1][i+1]+palette[moment][1][i])/2)
            pos = (int(i*h) , 0)
            color = (255,255,255)
            
            writePixel(surf,self.font,self.outfont,pos,color,(0,0,0),t)
        self.sprites["horizontal_scale"].dirty=1

    def __writeTitle(self):
        rect=self.sprites["title"].rect
        t="%s vs. %s"%(self.moment_list[0],self.moment_list[1])
        text = self.font.render(t, 1, (0, 0, 0))
        posx = 32+rect.width*0.5 - text.get_width()*0.5
        posy = 0
        self.sprites["title"].image.fill((150,150,150))
        self.sprites["title"].image.blit(text, (posx, posy))
        self.sprites["title"].dirty=1

    def __getIntervalValue(self,moment,index):
        length=len(palette[moment][0])
        if index>=0 and index <length:
            i=length-index-1
        else:
            return "NaN"
        if abs(palette[moment][1][-1])>10: t = "%.0f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
        elif abs(palette[moment][1][-1])>1: t = "%.1f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
        else: t = "%.2f"%((palette[moment][1][length-i]+palette[moment][1][length-1-i])/2)
        return t
    
    def convert_to_value(self,pos):
        moment=self.moment_list[0]
        length=len(palette[moment][0])
        valueH=int(1.0*(pos[0]-self.cutSprite.rect.left)/self.cutSprite.rect.width*length)

        moment=self.moment_list[1]
        length=len(palette[moment][0])
        valueV=length-1-int(1.0*(pos[1]-self.cutSprite.rect.top)/self.cutSprite.rect.height*length)
        return (valueH,valueV)






##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
class variableCOMP():
    def __init__(self,variables):
        self.data=None
        self.dimensions=variables["DBZH"].dimensions
        self.attributes=variables["DBZH"].attributes.copy()
        self.attributes["scale_factor"]=1.0
        self.attributes["add_offset"]=0.0
        self.variables=variables;
        self.moments=[["DBZH",-14,15],["DBZH",-14,15]]
        #calculate value


    def getValue(self):
        moment=self.moments[0][0]
        data1=self.variables[moment].getValue()
        data1=data1*self.variables[moment].attributes["scale_factor"]+self.variables[moment].attributes["add_offset"]
        FillValue=self.variables[moment].attributes["_FillValue"]
        moment=self.moments[1][0]
        data2=self.variables[moment].getValue()
        data2=data2*self.variables[moment].attributes["scale_factor"]+self.variables[moment].attributes["add_offset"]
        data=np.where((data1>self.moments[0][1]) & (data1<self.moments[0][2]) & (data2>self.moments[1][1]) & (data2<self.moments[1][2]), np.ones(data1.shape,dtype=np.float32), np.zeros(data1.shape,dtype=np.float32))
        return data


class MomentCOMP():

    def __init__(self,(nc,moment,moment_list,P),convert_to_value,name="MomentCOMP"):
        self.new_P=P
        self.new_nc=nc
        self.new_moment=moment
        self.P=[(None,None),(None,None),(None,None)]
        self.moment_list=moment_list
        self.convert_to_value=convert_to_value
        self.name=name
        self.dirty=1
        
    
    def get_job(self,job,event):
    
        if event.type == KEYDOWN :
            if event.key ==pygame.K_c:##< press i releases zoom in
                self.new_moment[0]="COMP"
        return "NO_JOB" 
    
    
    def __StartComp(self,nc):
        var=variableCOMP(nc.variables)
        nc.variables["COMP"]=var

            
    def loop(self):
        if "COMP" not in self.new_nc[0].variables:
            self.__StartComp(self.new_nc[0])
            
        if self.P[0]!=self.new_P[0] or self.P[1]!=self.new_P[1]:
            self.P[0]=self.new_P[0]
            self.P[1]=self.new_P[1]
            self.up_to_date=False
            
        if self.up_to_date==False:
            self.new_nc[0].variables["COMP"].moments[0][0]=self.moment_list[0]
            self.new_nc[0].variables["COMP"].moments[1][0]=self.moment_list[1]
            (valueH,valueV)=self.convert_to_value((min(self.P[0][0],self.P[1][0]),max(self.P[0][1],self.P[1][1])))
            self.new_nc[0].variables["COMP"].moments[0][1]=np.interp(valueH, range(len(palette[self.moment_list[0]][0])+1), palette[self.moment_list[0]][1])
            self.new_nc[0].variables["COMP"].moments[1][1]=np.interp(valueV, range(len(palette[self.moment_list[1]][0])+1), palette[self.moment_list[1]][1])
            (valueH,valueV)=self.convert_to_value((max(self.P[0][0],self.P[1][0]),min(self.P[0][1],self.P[1][1])))
            self.new_nc[0].variables["COMP"].moments[0][2]=np.interp(valueH, range(len(palette[self.moment_list[0]][0])+1), palette[self.moment_list[0]][1])
            self.new_nc[0].variables["COMP"].moments[1][2]=np.interp(valueV, range(len(palette[self.moment_list[1]][0])+1), palette[self.moment_list[1]][1])
            print self.new_nc[0].variables["COMP"].moments
            self.up_to_date=True
        
        
        
    def __convert_to_value(self,pos):
        moment=self.moment_list[0]
        length=len(palette[moment][0])
        valueH=1.0*(pos[0]-self.cutSprite.rect.left)/self.cutSprite.rect.width*length
        H = np.interp(valueH, range(len(palette[moment][0])+1), palette[elf.moment_list[0]][1])
        
        moment=self.moment_list[1]
        length=len(palette[moment][0])
        valueV=length-1-1.0*(pos[1]-self.cutSprite.rect.top)/self.cutSprite.rect.height*length
        H = np.interp(valueV, range(length+1), palette[moment][1])
        return (H,V)

