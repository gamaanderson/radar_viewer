#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np 
import scipy
from scipy import interpolate
import pygame
from palette import palette
import netcdf_module
import hdf5_module
import mdv_module
from scipy import ndimage

netcdf_module.set_resolution(100,0.2,0.1) #set resolution range=100m, azimuth=0.5 elevation=0.1


def start_file(nc,moment):#for compactibility with modules that use pre-processing
    return #we don't use pre-processing
    

def returnBackground(image,scaled_rect,image_rect): # load backgrond return surface of size of surfrec (rectangle). If full=False cut current PPI region
        if image==None:
            surf=pygame.Surface(image_rect.size)
            return surf            
            
        background = pygame.image.load(image)
        background=background.convert()
        X=-scaled_rect.left*background.get_width()/scaled_rect.size[0]
        Y=-scaled_rect.top*background.get_width()/scaled_rect.size[1]
        width=image_rect.width*background.get_width()/scaled_rect.size[0]
        height=image_rect.height*background.get_height()/scaled_rect.size[1]
        rect=pygame.Rect(X,Y,width,height)
    
        #Now we have to fix rectange outside surface
        if background.get_rect().contains(rect):# no problem
            #cut rect off and scale
            return pygame.transform.smoothscale(background.subsurface(rect), image_rect.size)
        else:# this only make sence for full=False
            #clif rectangle
            new_rect=rect.clip(background.get_rect())
            # calculate new size to scale
            scale_X=new_rect.width*image_rect.width/width
            scale_Y=new_rect.height*image_rect.height/height
            background_scaled=pygame.transform.smoothscale(background.subsurface(new_rect), (scale_X,scale_Y))
            surf=pygame.Surface(image_rect.size)
            if background.get_rect().collidepoint(rect.topleft): # top-left corner is in the surf, good, no translation to make
                surf.blit(background_scaled,(0,0))
            else: #have to make some coodinates change, this also work in the above case, but we don't need it there
                #calculate new_rect.topleft in relation to rect.topleft
                new_X=new_rect.left-rect.left
                new_Y=new_rect.top-rect.top
                #scale to surf[0] size
                posX=new_X*image_rect.width/rect.width
                posY=new_Y*image_rect.width/rect.width
                surf.blit(background_scaled,(posX,posY))
                
            return surf





def makePPI((nc,elev,moment,threshold,smooth), scaled_rect,image_rect,backgroundPath):
    import palette
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
#    print color
#    print threshold
    if smooth:
        smooth_int=0
    else:
        smooth_int=0

    data=nc.variables[moment].getValue()*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    fill_value=nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    if smooth:
        pass
        filt=nc.variables["COMP"].getValue()
        data=np.where(filt==1,data,fill_value)
        smooth_int=0
    if nc.attributes["n_gates_vary"]=="true":
        array=netcdf_module.PPI2(nc.filename,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,smooth_int,(data,fill_value),color)
    else:
        array=netcdf_module.PPI1(nc.filename,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,smooth_int,(data,fill_value),color)

    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2
    

def makeVerticalCut((nc,moment,P,threshold,smooth), scaled_rect,image_rect,topoPath):
    import palette
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
    #print (x0,y0),(x1,y1)
    if smooth:
        smooth_int=1
    else:
        smooth_int=0
    data=nc.variables[moment].getValue()*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    fill_value=nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    if nc.attributes["n_gates_vary"]=="true":
        array=netcdf_module.vertical2(nc.filename,scaled_rect.size,P[0],P[1],smooth_int,(data,fill_value),color)
    else:
        array=netcdf_module.vertical1(nc.filename,scaled_rect.size,P[0],P[1],smooth_int,(data,fill_value),color)

    surf=pygame.Surface(scaled_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((200, 200, 200))
    

    corteSurf.blit(surf,(0,0))
    makeGrid(corteSurf)
    drawGround(topoPath,P,corteSurf)
    return corteSurf
    

def makeCAPPI_mdv((nc,elev,moment,threshold,smooth), scaled_rect,image_rect,backgroundPath):
    import palette
    
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
    if smooth:
        smooth_int=1
    else:
        smooth_int=0
    
    array=mdv_module.CAPPI(nc.filename,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,smooth_int, moment,color)
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2

def makeCAPPI_mdv_unscaled((nc,elev,moment,threshold), scaled_rect,image_rect,backgroundPath):
    import palette
    
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
    if smooth:
        smooth_int=1
    else:
        smooth_int=0
    
    array=mdv_module.CAPPI_unscaled(nc.filename,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,moment,color)
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2


def makeVerticalCut_mdv((nc,moment,P,threshold,smooth), scaled_rect,image_rect,topoPath):
    
    import palette

    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
#    print color
    if smooth:
        smooth_int=1
    else:
        smooth_int=0
    
    array=mdv_module.vertical(nc.filename,scaled_rect.size,P[0],P[1],smooth_int,moment,color)


    surf=pygame.Surface(scaled_rect.size)

    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    
    # cria uma surf para colar os dados   
    corteSurf = pygame.Surface(array.shape)
    corteSurf.fill((200, 200, 200))
    

    corteSurf.blit(surf,(0,0))
    makeGrid(corteSurf)
    drawGround(topoPath,P,corteSurf)
    return corteSurf
    

def makePPI_hdf((hdf,elev,moment,threshold), scaled_rect,image_rect,backgroundPath):
    import palette
    #print (x0,y0),(x1,y1)
#    color=(palette.palette[moment][0][-threshold:],palette.palette[moment][1],
#                                                    palette.palette[moment][1]-
#                                                    (palette.palette[moment][1]-palette.palette[moment][2])
#                                                    *threshold/len(palette.palette[moment][0]))
    color=(palette.palette[moment][0][threshold::],palette.palette[moment][1][threshold::])
#    print color
#    print threshold
    filename=hdf.filename
#    hdf.close()
    for moment_index in range(hdf["/scan%i/what"%elev].attrs["descriptor_count"]):
        if "".join(hdf["/scan%i/moment_%i"%(elev,moment_index)].attrs["moment"])==moment:
            break
#    print moment_index,moment
    array=hdf5_module.PPI1(hdf.fid.id,scaled_rect.size,(-scaled_rect.left,-scaled_rect.top), image_rect.size,elev,moment_index,color)
#    hdf=h5py.File(filename,'r') 
    surf=pygame.Surface(image_rect.size)
    pygame.surfarray.blit_array(surf,array)
    surf.set_colorkey((0,0,0))
    back=returnBackground(backgroundPath, scaled_rect,image_rect)
    back2=back.copy()
    back.blit(surf,(0,0))
    back.set_alpha(150)
    back2.blit(back,(0,0))
    back.set_colorkey((1,1,1))
    back.set_alpha(None)
    back2.blit(back,(0,0))
    
    return back2






       
def makeGrid(surf):
    cor = (60,60,60)
    cor_eixos = (50, 50, 50)
    nx=10
    ny=8

    height = surf.get_height() #altura da surface para calcular o numero de divisoes
    width = surf.get_width() #largura ra surface

    #dx = grid_width/nx
    dy = float(height)/ny
      
    # linhas horizontais e texto, ny = numero de divisoes horizontais
    for i in range(ny+1):
        #print i
        i=i+0.5
        P0 = (0, int(height-i*dy)) # y de baixo pra cima
        P1 = (width+2, int(height-i*dy))
        pygame.draw.aaline(surf, cor, P0,P1)

    # linhas verticais e texto

    dx = float(width)/ny

    for i in range(nx+1):
        P0 = (int(width-i*dx), 0)
        P1 = (int(width-i*dx), height+2)
        #pygame.draw.aaline(surf, cor, P0,P1)
    pygame.draw.rect(surf,cor, surf.get_rect(),1)



import math
def drawGround(topoFile,P,surf):
#	print "drawGround",topoFile
	if topoFile==None: return 0
	n_points=180
	TOPO = np.fromfile( topoFile, dtype= np.int16)
#	print TOPO
	size = math.sqrt(TOPO.size)
#	print size,TOPO.size, TOPO.shape
	TOPO = np.reshape(TOPO, (size,size))
#	TOPO = TOPO.transpose()
	
	
 	cutWidth = surf.get_width()
 	cutHeight =  surf.get_height()

 	xground = []
 	yground = []
 	p = []

	#for i in range(x1-x0):
	for k in range(n_points):
		x = (P[0][0]+k*(P[1][0]-P[0][0])/(n_points-1.))* size/1000000.+size/2.
		y = -(P[0][1]+k*(P[1][1]-P[0][1])/(n_points-1.))* size/1000000.+size/2.
		xground.append(int(k*cutWidth/(n_points-1.)))
		if x<size and y<size and x>=0 and y>=0:
			if TOPO[int(x)][int(y)]<0:
				TOPO[int(x)][int(y)]=0
			elev = TOPO[int(x)][int(y)]*float(cutHeight)/16000
			yground.append(cutHeight- elev)
 			#print x, y, TOPO[y][x]
 		 	p.append((xground[-1], yground[-1]))

	if len(p)>2:
		pygame.draw.lines(surf, (200,200,200), False, p,1)



def make_dispersion((nc_list,elev_list,moment_list,P,smooth), scaled_rect,image_rect):
    import palette
    if smooth:
        smooth_int=1
    else:
        smooth_int=0
    #calculate ppi rect
    radar_size=(1000,1000)
    # P is already radar
    P1=(min(P[0][0],P[1][0]),max(P[0][1],P[1][1]))
    P2=(max(P[0][0],P[1][0]),min(P[0][1],P[1][1]))
    ppi_topleft=(int(P1[0]/1000+500),int(500-P1[1]/1000))
    ppi_size=(int((P2[0]-P1[0])/1000),int((P1[1]-P2[1])/1000))
    print P1
    print ppi_size,ppi_topleft
    #make first moment
    nc=nc_list[0]
    moment=moment_list[0]
    elev=elev_list[0]
    len1=len(palette.palette[moment][0])
    values= tuple((0, 0,i+1) for i in range(len1))
    color=(values,palette.palette[moment][1])
    data=nc.variables[moment].getValue()*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    fill_value=nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    if nc.attributes["n_gates_vary"]=="true":
        print nc.filename,radar_size,ppi_topleft, ppi_size,elev,smooth_int
        array1=netcdf_module.PPI2(nc.filename,radar_size,ppi_topleft, ppi_size,elev,smooth_int,(data,fill_value),color)
    else:
        array1=netcdf_module.PPI1(nc.filename,radar_size,ppi_topleft, ppi_size,elev,smooth_int,(data,fill_value),color)
    #make second ppi
    nc=nc_list[1]
    moment=moment_list[1]
    elev=elev_list[1]
    len2=len(palette.palette[moment][0])
    values= tuple((0, 0,i+1) for i in range(len2))
    color=(values,palette.palette[moment][1])
    data=nc.variables[moment].getValue()*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    fill_value=nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    if nc.attributes["n_gates_vary"]=="true":
        array2=np.zeros((ppi_size))
        array2=netcdf_module.PPI2(nc.filename,radar_size,ppi_topleft, ppi_size,elev,smooth_int,(data,fill_value),color)
    else:
        array2=netcdf_module.PPI1(nc.filename,radar_size,ppi_topleft, ppi_size,elev,smooth_int,(data,fill_value),color)
    array1=array1.reshape(-1)
    array2=array2.reshape(-1)

    array = np.vstack((array1, array2))
    array= np.transpose(array).tolist() #numpy don't do what i want
    array=map(tuple, array)
    total=array1.size-array.count((0,0))
    print array.count((0,0))
    from itertools import product
    dispersion=np.zeros((len1,len2))
    for par in product(range(1,len1+1),range(1,len2+1)):

        dispersion[(par[0]-1,par[1]-1)]=array.count(par)+1
    #interpolate to size of image
#    print dispersion
    dispersion=np.log(dispersion)
    f = scipy.interpolate.RectBivariateSpline(range(len1), range(len2), dispersion, kx=1, ky=1)
    print scaled_rect.width,scaled_rect.height
    xx = np.linspace(-0.5,len1-0.5,scaled_rect.width)
    yy = np.linspace(-0.5,len2-0.5,scaled_rect.height)
    new_array=np.fliplr(f(xx,yy))
    maxi=new_array.max()
    print "max",maxi
    step=tuple([i*maxi for i in palette.stepCOMPARE])
    color=(palette.coresCOMPARE,step)
    surf = colormap(np.transpose(new_array),color)
    print surf.get_size()
    return surf


def colormap(data,palette, backcolor=(0,0,0)):
    #data = ndimage.filters.median_filter(data, size = 3)
    data = np.transpose(data)
    size = data.shape
    surf = pygame.Surface((size[0],size[1]))     # Create a same shape surface
    surf.fill(backcolor)
    #surf.set_colorkey(backcolor)
    colorArray = pygame.surfarray.array3d(surf)  # Create a colorArray from surface
    ncolors = len(palette[0])        # Get the number of
    #print colorArray.shape
    # For each color in palette, evaluate the data interval for this color, 
    # find the index of all data in the interval and place the colorRGB values 
    # in mapped surface  
    for c in range(ncolors):
        color = palette[0][c]         # get RGB values of color number n
        minimo = palette[1][c]             # The minimum value for this color
        maximo = palette[1][c+1]           # The maximum value for this color
        idx = np.where((data>=minimo)&(data<maximo)) # Get the index for data in interval 
        colorArray[idx] = color

    pygame.surfarray.blit_array(surf, colorArray)
    return pygame.Surface.convert(surf)

