import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
from netcdf import netcdf 
import pygame
import GeraImages
#import pyGeraImages_mdv as GeraImages
# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import CAPPI_mdv

from ToolsPanel import MainMenu
import Line
import Corte_mdv
import Colorbar
netcdf_path=[
"/simepar/projetos/opera/mosaico/20140724160000/BRU/mdv/vol/CART_154431.nc",
"/simepar/projetos/opera/mosaico/20140724160000/BRU/mdv/vol/PPI_154431.nc",
"/simepar/projetos/opera/mosaico/20140724160000/PPR/mdv/vol/CART_160650.nc",
"/simepar/projetos/opera/mosaico/20140724160000/PPR/mdv/vol/PPI_160650.nc",
"/simepar/projetos/opera/radares_hdf5/cgufrog/20140723190637_CGU_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/cgufrog/20140723191023_CGU_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/cgufrog/20140723191023_CGU_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/ga1frog/20140721001647_GA1_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/ga1frog/20140721002026_GA1_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/ga1frog/20140721002026_GA1_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/mdifrog/20140724023550_MDI_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/mdifrog/20140724024003_MDI_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/mdifrog/20140724024003_MDI_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/pcofrog/20140718222674_PCO_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/pcofrog/20140718223026_PCO_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/pcofrog/20140718223026_PCO_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/srofrog/20140718112630_SRO_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/srofrog/20140718113022_SRO_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/srofrog/20140718113022_SRO_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/stifrog/20140722210636_STI_PPI_N3.nc",
"/simepar/projetos/opera/radares_hdf5/stifrog/20140722211023_STI_CAP_N3.nc",
"/simepar/projetos/opera/radares_hdf5/stifrog/20140722211023_STI_PPI_N3.nc",

]



new_nc=[netcdf(netcdf_path[0],'r')]
print "end open nercdf"
Kern.start_up((1400,1600))
ppi_rect=pygame.Rect(50,50,600,600)
moment=["DBZH"]
threshold=[0]
cappi=CAPPI_mdv.CAPPI(ppi_rect,Kern.jobs,GeraImages,(new_nc,[0],moment,threshold),name="CAPPI")


Kern.add_module(cappi,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(55,100,45,500),Kern.jobs,(moment,threshold,cappi.value,cappi.dirty),name="Colorbar"),2)

Kern.add_module(MainMenu(pygame.Rect(600,0,30,30),Kern.jobs,(netcdf_path,new_nc),name="MainMenu"),2)

line=Line.Line(ppi_rect,Kern.jobs,(new_nc,cappi.scaledDataSprite,cappi.dirty),name="Line")
Kern.add_module(line,0)


cut_rect=pygame.Rect(700,10,480+64,533+64)

Kern.add_module(Corte_mdv.VerticalCut(cut_rect,Kern.jobs,GeraImages,(new_nc,moment,threshold,line.P,line.Proj),cappi.convertion_to_radar,name="Cut"),0)

Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")
