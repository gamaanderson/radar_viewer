import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
from netcdf import netcdf
import pygame
import GeraImages


# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import PPI
from ToolsPanel import MainMenu
import Line
import Corte
import Colorbar
netcdf_path=[os.getcwd()+"/"+"dados/TXS131003180057.RAW03EZ.nc"]
netcdf_path=["/home/agama/tmp/JGI-400--2014-10-27--00-17-45dBZ.nc",
]

new_nc=[netcdf("/home/agama/tmp/JGI-400--2014-10-27--00-17-45dBZ.nc",'r')]
netcdf_path=[]
print "end open nercdf"
Kern.start_up((1600,900))
ppi_rect=pygame.Rect(30,30,700,700)
moment=["DBZH"]
elev=[0]
threshold=[0]
ppi=PPI.PPI(ppi_rect,Kern.jobs,GeraImages,(new_nc,elev,moment,threshold),name="PPI")
ppi.dirty[0]=2
Kern.add_module(ppi,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(30,90,55,400),Kern.jobs,(moment,threshold,ppi.value,ppi.dirty),name="Colorbar"),2)
Kern.add_module(MainMenu(pygame.Rect(500,0,30,30),Kern.jobs,(netcdf_path,new_nc),all_power=False,name="MainMenu"),2)
#line=Line.Line(ppi_rect,Kern.jobs,(new_nc,ppi.scaledDataSprite,ppi.dirty),name="Line",)
#Kern.add_module(line,0)
#cut_rect=pygame.Rect(30,540,500,300)
#Kern.add_module(Corte.VerticalCut(cut_rect,Kern.jobs,(new_nc,moment,threshold,line.P,line.Proj),ppi.convertion_to_radar,name="Cut"),0)

import h5py
import pyGeraImages_hdf5odim
import PPI_hdf5odim
new_hdf=[h5py.File("/simepar/radar2/cemaden/400km/JGI-400--2014-10-27--00-17-45dBZ.hdf5",'r')]
netcdf_path2=netcdf_path
ppi_rect=pygame.Rect(800,30,700,700)
moment=["DBZH"]
threshold=[0]
zoomrect=ppi.scaledDataSprite.rect
ppi2=PPI_hdf5odim.PPI(ppi_rect,Kern.jobs,pyGeraImages_hdf5odim,(new_hdf,elev,moment,threshold),name="PPI2")
ppi2.scaledDataSprite.rect=ppi.scaledDataSprite.rect
ppi2.dirty=ppi.dirty
Kern.add_module(ppi2,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(800,90,55,400),Kern.jobs,(moment,threshold,ppi2.value,ppi2.dirty),name="Colorbar2"),2)
Kern.add_module(MainMenu(pygame.Rect(1000,0,30,30),Kern.jobs,(["/simepar/radar2/cemaden/400km/JGI-400--2014-10-27--00-17-45dBZ.hdf5"],new_nc),file_type="HDF",all_power=False,name="MainMenu2"),3)
#line=Line.Line(ppi_rect,Kern.jobs,(new_nc,ppi.scaledDataSprite,ppi.dirty),name="Line2",)
#Kern.add_module(line,0)
#cut_rect=pygame.Rect(600,540,500,300)
#Kern.add_module(Corte.VerticalCut(cut_rect,Kern.jobs,(new_nc,moment,threshold,line.P,line.Proj),ppi.convertion_to_radar,name="Cut2"),0)



Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")
