
import pygame
from math import *
from Sprites import *
import ToolsPanel
from palette import palette
from palette import polarization_pairs
from palette import static
from Colorbar import Colorbar
from Text import Text
import GeraProds

class VerticalCut:

    def __init__(self,rect,jobs,GeraImages,(nc,moment,threshold,P,Proj),convertion_to_radar,name="Cut"):
        self.rect=rect
        self.GeraImages=GeraImages
        self.convertion_to_radar=convertion_to_radar
        self.font = pygame.font.Font("images/LiberationSans-Regular.ttf", 14)
        pos=pygame.mouse.get_pos()
        self.pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        self.image=pygame.display.get_surface().subsurface(self.rect)
        rect=self.rect.inflate(-64,-64)
        surf=pygame.display.get_surface().subsurface(rect)
        self.cutSprite=Sprite(image=surf,pos=(rect.left-self.rect.left,rect.top-self.rect.top),who_created_me=name)
        self.scaleFactor=1
        self.scaledDataSprite=Sprite(rect=pygame.Rect((0,0),rect.size),who_created_me=name)
        self.radarDataSurf=self.cutSprite.image.copy()
        self.new_nc=nc #this is a list and can eventualy change externaly, if change, it will update nc
        self.nc=None #actual, class internal nc, will update in loop
        self.new_moment=moment
        self.moment=moment[0]
        self.new_threshold=threshold
        self.threshold=threshold[0]
        self.new_P=P
        self.P=[(None,None),(None,None),(None,None)]
        self.new_Proj=Proj
        self.Proj=(None,None)
        self.name=name
        self.jobs=jobs
        self.dirty=[1]
        import collections
        self.cutSurfList = collections.deque(maxlen=30)
        self.panOn=False
        self.smooth=False
        self.sprites={}
        
        self.sprites["vertical_scale"]=Sprite(rect=pygame.Rect((0,32),(32,rect.height)),who_created_me=name)
        w = self.font.size("DBZH:   No Value dbz")[0]
        self.sprites['data']=Sprite(pygame.Rect((self.rect.width/2-w/2,self.rect.height-16),(w,self.font.get_height()+2)),who_created_me=name)
        self.sprites['data'].image.fill((150,150,150))        
        
#        self.sprites["menu"]=self.MomentPanel(self)
#        self.sprites['colorbar']=Colorbar(moment[0],threshold[0],rect=pygame.Rect(5,20,35,self.rect.height-40),who_created_me="PPI")   
#        self.text=Text(self)
#        self.text.printElev(elev[0],nc[0].variables['elevation'].getValue()[nc[0].variables['sweep_start_ray_index'].getValue()[elev[0]]])
        
        
        
#        jobs["%s:DRAW_LINE_ON"%name]=self.__INFORM_SPRITES
#        jobs["%s:DRAW_LINE_OFF"%name]=self.__INFORM_FILES
#        jobs["%s:INFORM_METADATA"%name]=self.__INFORM_METADATA
        jobs["%s:ZOOM_IN"%name]=self.__ZOOM
        jobs["%s:ZOOM_OUT"%name]=self.__ZOOM
        jobs["%s:PAN_ON"%name]=self.__PAN_ON
        jobs["%s:PAN_OFF"%name]=self.__PAN_OFF
        jobs["%s:REDRAW"%name]=self.__REDRAW
        jobs["%s:CHANGE_MOMENT"%name]=self.__CHANGE_MOMENT
        jobs["%s:SMOOTH ON/OFF"%name]=self.__SMOOTH_ON_OFF
        
    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf=self.rect.collidepoint(pos[0],pos[1])
        if event.type == KEYDOWN and over_surf:
            if event.key ==pygame.K_i:##< press i releases zoom in
                return "%s:ZOOM_IN"%self.name
            if event.key ==pygame.K_o:##< press o releases zoom out
                return "%s:ZOOM_OUT"%self.name
#            if event.key ==pygame.K_UP:##< press up arrow releases elevation up
#               return "%s:ELEV_UP"%self.name
#            if event.key ==pygame.K_DOWN:##< press down arrow releases elevation down
#                return "%s:ELEV_DOWN"%self.name
            if event.key ==pygame.K_r:##< press r releases redraw PPI
                return "%s:REDRAW"%self.name
            if event.key ==pygame.K_p:##< press p start or stop pan
                if self.panOn:
                    return "%s:PAN_OFF"%self.name
                else:
                    return "%s:PAN_ON"%self.name
            if event.key ==pygame.K_s:##< press p start or stop pan
                return "%s:SMOOTH ON/OFF"%self.name
        #----------------------------------------------
        # DEFINE AS ACOES DOS BOTOES E RODINHA DO MOUSE
        #----------------------------------------------
        # ALGUM BOTAO ESTA SENDO PRESSIONADO

        if event.type==pygame.MOUSEBUTTONDOWN:

            # BOTAO ESQUERDO
            if event.button==1 and over_surf==True:##< press with first mouse botton over PPI start to draw the line
                pass#return "DRAW_LINE_ON"


            # BOTAO DO MEIO
#            if event.button==2 and over_surf==True:##< press with mittel mouse botton over PPI start to lock pan
#                return "%s:PAN_ON"%self.name


            # RODINHA GIROU PARA FRENTE
#            if event.button==4:
#                if self.sprites['colorbar'].mouse_over(self.rect.topleft):
#                    return "%s:THRESHOLD_UP"%self.name
#                 if over_surf==True:##< move wheel forward releases zoom in
#                    return "%s:ZOOM_IN"%self.name

            # RODINHA GIROU PARA TRAS
#            if event.button==5:
#                if self.sprites['colorbar'].mouse_over(self.rect.topleft):
#                    return "%s:THRESHOLD_DOWN"%self.name
#                 if over_surf==True:##< move wheel forward releases zoom in
#                    return "%s:ZOOM_OUT"%self.name


#                # BOTAO DIREITO
#                if event.button==3:
#                # nao faz nada ainda
#                pass

        # O botao do meio esta pressionado
        # so no caso do evento MOUSEBUTTONDOWN ter sido perdido
#        if pygame.mouse.get_pressed()[1] and over_surf==True:
#            if self.panOn:
#                return "NO_JOB"
#            else:
#                return "%s:PAN_ON"%self.name

#        if event.type==pygame.MOUSEBUTTONUP:

            # BOTAO ESQUERDO
#            if event.button==1:
#                pass#if self.lineIsDrawing:##<release first mouse button end drawing the line
                #    return "DRAW_LINE_OFF"


            # BOTAO DO MEIO
#            if event.button==2:##<release mittel mouse button releases pan
#                return "%s:PAN_OFF"%self.name
        

        return "NO_JOB"
        
    def __startNewFile(self):
        self.nc=self.new_nc[0]
        from radarlist import radarlist
        self.topoPath=None
        for radar in radarlist:
            if radar[5] and abs(self.nc.attributes["radar_latitude"]-(radar[0]))<0.01 and abs(self.nc.attributes["radar_longitude"]-(radar[1]))<0.01:
                self.topoPath="map/topografia%s"%radar[2]
                break
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False 
    
    """HANDEL_JOBS"""
    """ZOOM"""
    def __ZOOM(self,job):
            pos=pygame.mouse.get_pos()
            pos=(pos[0]-self.rect.left-self.cutSprite.rect.left,pos[1]-self.rect.top-self.cutSprite.rect.top)
            ## before zooming save relative position of the Line
            #XYOr=GeraProds.screenToRelative((line.X0,line.Y0),displayWindow)
            #XY1r=GeraProds.screenToRelative((line.X1,line.Y1),displayWindow)
            if job=="%s:ZOOM_IN"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width<25: factor=1.1 
            elif job=="%s:ZOOM_OUT"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width>1: factor=0.9
            else: return
            scaledResX = int(self.scaledDataSprite.rect.width*factor)
            scaledResY = int(self.scaledDataSprite.rect.height*factor)
            # calcula a nova position em funcao da posicao do mouse
            newPosX = -int((pos[0]-self.scaledDataSprite.rect.left)*(factor-1)) 
            newPosY = -int((pos[1]-self.scaledDataSprite.rect.top)*(factor-1))
            self.scaledDataSprite.rect.size=(scaledResX, scaledResY)
            self.scaledDataSprite.rect.move_ip(newPosX, newPosY)
            ## restore Line in proper position after zomming
            #line.setStartPoint(GeraProds.relativeToScreen(XYOr,displayWindow))#restore line after zoom
            #line.setEndPoint(GeraProds.relativeToScreen(XY1r,displayWindow))
            ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
            self.up_to_date=False
            self.dirty[0]=1
            self.lastChangeTime = pygame.time.get_ticks()
            print self.scaledDataSprite.rect.size
    """PAN_ON"""
    def __PAN_ON(self,job):
        ## inicialisate pygame's relative mouse moviment
        pan = pygame.mouse.get_rel()
        ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False
        ## turn pan on
        self.panOn=True
        self.dirty[0]=2
    """PAN_OFF"""
    def __PAN_OFF(self,job):
        self.panOn=False
        self.dirty[0]=0
    """REDRAW"""
    def __REDRAW(self,job): 
        ## lock mouse buttons for updating PPI
        pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)

        P0=self.convertion_to_radar(self.P[0])
        P1=self.convertion_to_radar(self.P[1])
        surf = self.GeraImages.makeVerticalCut_mdv((self.nc,self.moment,(P0,P1),self.threshold,self.smooth), self.scaledDataSprite.rect,self.scaledDataSprite.rect,self.topoPath)#self.cutSprite.rect)
        self.scaledDataSprite.blit(surf,(0,0))
#                         ((-1)*self.scaledDataSprite.rect.left,
#                         (-1)*self.scaledDataSprite.rect.top))
        pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN) 
        self.dirty[0]=1     
    """CHANGE_MOMENT"""  
    def __CHANGE_MOMENT(self,job):
        self.new_moment[0]=job[len(self.name)+1+14:]
        self.__REDRAW(job)
#        self.sprites['colorbar'].reset(self.new_moment[0])
    """SMOOTH_ON_OFF"""    
    def __SMOOTH_ON_OFF(self,job):
        if self.smooth==False:
            self.smooth=True
        else:
            self.smooth=False
        self.up_to_date=False  
        
        
    def loop(self):
        if self.nc!=self.new_nc[0]:
            self.__startNewFile()
        if self.scaledDataSprite.image.get_size() != self.scaledDataSprite.rect.size:
            self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
            self.lastChangeTime = pygame.time.get_ticks()
            self.up_to_date=False
        if self.nc!=self.new_nc[0] or self.moment!=self.new_moment[0]:
            self.nc=self.new_nc[0] 
            self.moment=self.new_moment[0]
            self.__moveSpaceTime(self.nc,self.moment,self.P)
        if self.P[0]!=self.new_P[0] or self.P[1]!=self.new_P[1]:
            self.P[0]=self.new_P[0]
            self.P[1]=self.new_P[1]
            self.up_to_date=False
            self.point_changed=True
            self.lastChangeTime = pygame.time.get_ticks()
        if self.Proj!=self.new_Proj[0]:
            self.Proj=self.new_Proj[0]
            self.dirty[0]=1
        if self.threshold!=self.new_threshold[0]:
            self.threshold=self.new_threshold[0]
            self.__REDRAW(None)
        elif self.up_to_date==False and (pygame.time.get_ticks() - self.lastChangeTime)>600:##< @anchor zoom->redraw if PPI is not up to date and no move has been done in the last 0.6 
            if self.point_changed:
                self.__moveSpaceTime(self.nc,self.moment,self.P)
                self.point_changed=False
            else:
                self.up_to_date=True
                self.__REDRAW(None)


        if self.panOn:
            self.lastChangeTime = pygame.time.get_ticks()
            pan = pygame.mouse.get_rel() #movimento acumulado do mouse
            self.scaledDataSprite.rect.move_ip(pan[0],pan[1])
            #line.setStartPoint((line.X0+pan[0],line.Y0+pan[1]))#move line with surface
            #line.setEndPoint((line.X1+pan[0],line.Y1+pan[1]))#move line with surface

        pos=pygame.mouse.get_pos()
        pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        if self.pos!=pos: #no need to atualise what didn't change
            self.pos = pos
            if self.cutSprite.rect.collidepoint(pos):
                k=float(pos[0]-self.cutSprite.rect.left-self.scaledDataSprite.rect.left)/self.scaledDataSprite.rect.width
                X=int(self.P[0][0]+k*(self.P[1][0]-self.P[0][0]))
                Y=int(self.P[0][1]+k*(self.P[1][1]-self.P[0][1]))
                self.new_Proj[0]=(X,Y)
                self.Proj=self.new_Proj[0]
                self.dirty[0]=1
                height=(self.cutSprite.rect.bottom-pos[1])*16.0/self.cutSprite.rect.height
                (D,R,E)=CutPosToDRE(self.convertion_to_radar((X,Y)),height,self.nc,self.moment)
                #print D,R,E
                self.printDRE((D,R,E),self.moment)

         
        # Imprime LatLon
#            if self.rect.collidepoint(pygame.mouse.get_pos()):
#                latlon = GeraProds.eventPosToLatLon(pos,self.nc,self.scaledDataSprite.rect)
#                self.text.setLatLon(latlon)
#                self.text.printLatLon()
#                # atulisa refletividade na sprite
#                (D,R,H) = GeraProds.screenCoordsToDRH(pos,self.scaledDataSprite.rect,self.elev,self.nc,self.moment)
#                self.text.printDRH((D,R,H),self.moment)

        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__update()
        else:            
            for sprite in self.sprites.values():
               sprite.write(self.image,None)


    
    
    def __update(self):
        self.image.fill((150,150,150))
        self.scaledDataSprite.blit_in(self.cutSprite.image)
        for sprite in self.sprites.values():
            if sprite.dirty>-1:
                sprite.blit_in(self.image)
        #internal product      
        product=(self.Proj[0]-self.P[0][0])*(self.P[1][0]-self.P[0][0])+(self.Proj[1]-self.P[0][1])*(self.P[1][1]-self.P[0][1])    
        if product!=0:
            k=float(product)/((self.P[1][0]-self.P[0][0])**2+(self.P[1][1]-self.P[0][1])**2)        
            a=(int(k*self.scaledDataSprite.rect.width+self.scaledDataSprite.rect.left),0)
            b=(int(k*self.scaledDataSprite.rect.width+self.scaledDataSprite.rect.left),self.cutSprite.rect.height)     
            pygame.draw.aaline(self.cutSprite.image, (255,0,0), a,b)
            
            P0=self.convertion_to_radar(self.P[0])
            P1=self.convertion_to_radar(self.Proj)
            
            dis=sqrt((P1[0]-P0[0])**2+(P1[1]-P0[1])**2)/1000
            text = self.font.render("%i km"%dis, 1, (255, 0, 0))
            self.image.blit(text, (b[0]+self.cutSprite.rect.left,b[1]+self.cutSprite.rect.top))
            
            
    
    def __moveSpaceTime(self,nc,moment,P):
        i=next((i for i,tupleSurf in enumerate(self.cutSurfList) if tupleSurf[0:3] == (nc.filename,moment,P)), None) # using generator to search in deque ppiSurfList
        if i==None:
            P0=self.convertion_to_radar(self.P[0])
            P1=self.convertion_to_radar(self.P[1])
            self.cutSurfList.append((nc.filename,moment,(P0,P1),self.radarDataSurf.copy()))#first put one-more element in the list
            self.radarDataSurf=self.cutSurfList[-1][3]#now point to it and then update
            pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
            self.__updateCutfull()
            pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)
            
        else:
            self.radarDataSurf=self.cutSurfList[i][3]#now point to it      
        self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
        self.dirty[0]=1   
            
    def __updateCutfull(self): 
        ##_________funciona
        P0=self.convertion_to_radar(self.P[0])
        P1=self.convertion_to_radar(self.P[1])
        cutSurf = self.GeraImages.makeVerticalCut_mdv((self.nc,self.moment,(P0,P1),0,False), self.cutSprite.image.get_rect(),self.cutSprite.rect,self.topoPath)
        self.__writeVerticalScale()
        self.scaledDataSprite.image.blit(pygame.transform.smoothscale(cutSurf, self.scaledDataSprite.rect.size),(0,0));
        self.radarDataSurf.blit(cutSurf,(0,0))
            
    def __writeVerticalScale(self):
        surf=self.sprites["vertical_scale"].image
        surf.fill((150,150,150))
        rect=self.sprites["vertical_scale"].rect
        dy = float(rect.height)/8
        
        # linhas horizontais e texto, ny = numero de divisoes horizontais
        for i in range(8+1):
        #print i
            t = "%.f"% (-1 + i*(16-0)/8)
            text = self.font.render(t, 1, (0, 0, 0))
            surf.blit(text, (rect.width-text.get_width(), rect.height-i*dy+6))
        
        text = self.font.render("Height (km)", 1, (0, 0, 0))
        text = pygame.transform.rotate(text, 90)  # rotaciona o texto
        posx = 0
        posy = rect.height*0.5 - text.get_height()*0.5
        surf.blit(text, (posx, posy))
            
        
        surf=self.sprites["vertical_scale"].dirty=1
        
    def printDRE(self, (d,r,e),moment="DBZH"):
        if type(d) is str:
            t = moment+":"+ d
        else:
            t = moment+": %.2f" %d+palette[moment][5]
        self.sprites['data'].image.fill((150,150,150))
        text = self.font.render(t, 1, (0, 0, 0))
        self.sprites['data'].blit(text, (0,0))
        self.sprites['data'].dirty=1
        
import numpy as np
def CutPosToDRE((x,y),height,nc,moment): #(DATA,RANGE,HEIGHT)
    rt =  4 * 6375000 / 3 #corrected earth radius

    
    scale_x=nc.variables['xc'].attributes["scale_factor"]
    offset_x=nc.variables['xc'].attributes["add_offset"]
    scale_y=nc.variables['yc'].attributes["scale_factor"]
    offset_y=nc.variables['yc'].attributes["add_offset"]

    x_s=(x-offset_x)/scale_x
    y_s=(y-offset_y)/scale_y    
        
    z=height*1000
    Zpolar = np.array(nc.variables[moment].getValue())
    # pega os valores apropriados para a formacao da matriz no netcdf_radial

    
    Fill_value=(nc.variables[moment].attributes["no_echo"])*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    missing_value=(nc.variables[moment].attributes["missing_value"])*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    # pega o numero de bins        
    
    if x_s>nc.variables['xc'].getValue()[0] and x_s<nc.variables['xc'].getValue()[-1] and  y_s>nc.variables['yc'].getValue()[0] and y_s<nc.variables['yc'].getValue()[-1] and z>nc.variables['bottom_top'].getValue()[0] and  z<nc.variables['bottom_top'].getValue()[-1] :
        x_index=np.argmin(np.abs(nc.variables['xc'].getValue()-x_s))
        y_index=np.argmin(np.abs(nc.variables['yc'].getValue()-y_s))
        z_index=np.argmin(np.abs(nc.variables['bottom_top'].getValue()-z))
        data = Zpolar[0][z_index][y_index][x_index]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
        range2=np.sqrt(x*x+y*y)/1000
    else:
        data = "no value"
        range2=np.sqrt(x*x+y*y)/1000
        
    if data==Fill_value:
        data = "no echo"
    if data== missing_value:
       data = "no value" 
    return (data,range2,None)


