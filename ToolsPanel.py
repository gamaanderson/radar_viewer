# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 16:30:21 2013

@author: buriol
"""
import pygame
from Sprites import *
from palette import palette
from palette import polarization_pairs
from palette import static

class Button: #I would do a sprite, but the pygame implementation is bad
    def __init__(self, rect,image,job,info):
        self.rect=rect
        self.image = pygame.image.load(image)
        #pygame.transform.scale(self.normal, self.rect.size)
        self.image.convert()
        self.job=job
        self.info=info
        self.dirty=1 ## if 0 don't blit, if 1 blit and set to 0, if 2 blit e maintain 2, if 3 blit pressed and set to 1, if 4 blit pressed and set to 0
        
    def dirtyblit(self, surf):
        if self.dirty>0:
            if self.dirty==1:
                self.dirty=0
            surf.blit(self.image,self.rect)

        
    def press(self):
        return self.job



class Switch: #I would do a sprite, but the pygame implementation is bad
    def __init__(self, rect,state_list,info):
        self.rect=rect
        self.images=[]
        self.jobs=[]
        for state in state_list:
            surf=pygame.image.load(state[0])
            surf.convert()
            self.images.append(surf)
            self.jobs.append(state[1])
        self.index=0
        self.lenght=len(state_list)
        self.info=info
        self.dirty=1 ## if 0 don't blit, if 1 blit and set to 0, if 2 blit and maintain 2
        
    def dirtyblit(self, surf):
        if self.dirty>0:
            if self.dirty==1:
                self.dirty=0
            surf.blit(self.images[self.index],self.rect)

    def press(self):
        self.index=(self.index +1)% self.lenght
        self.dirty=1
        return self.jobs[self.index]




class ToolsPanel(Sprite):
    """subclass from Sprite, add blit, blit_in, and mouse_over functions, as well as flags for change"""
    up_to_date=True # lets mantain this as a flag if the Sprite has changed
    moved=False # lets mantain this as a flag if the Sprite was moved
    who_created_me= "" # as sprites may be created every-where, this is impostant to help locating the source of a sprite, this is os reponsability of the develorpers
    
    
    def __init__(self, rect=None,who_created_me="don't know"):# can be inicialased with either a rect or a image
        Sprite.__init__(self,rect,None,None,who_created_me)  
        self.image.set_colorkey((255,0,255))
        self.image.fill((255,0,255))
        self.buttons={}  # dictionary with the buttons, witch are acessory classes similar to sprite
        
    def loadBotton(self):
        pass

    def takeMouseEvents(self, pos, pressed):
         # se o botao esta pressionado   
        if pressed==1:
            rel_pos=(pos[0]-self.rect.left,pos[1]-self.rect.top) #relativ position
            for button in self.buttons.values():
                # se o cursor esta sobre o botao
                if button.rect.collidepoint(rel_pos):
                    return button.press()
        return "NO_JOB"
    
    
    def update(self, pos):
        
        rel_pos=(pos[0]-self.rect.left,pos[1]-self.rect.top) #relativ position
        for button in self.buttons.values():
            
            if button.rect.collidepoint(rel_pos):
                self.showInfo(button.info,pos)
            button.dirtyblit(self.image)

    def showInfo(self, texto, pos):# I don't like this, but is a simple solution to a simple task, no need for complication
        return 
        surf = pygame.display.get_surface()
        font = pygame.font.SysFont("ubuntumono", 16)
        text = font.render(texto, 1, (255,255,0))
        textPos = (pos[0]-5-text.get_width(), pos[1]-10) #escrita a esquerda do mouse, nao a direita
        surf.blit(text, textPos)


import Tkinter
import tkFileDialog  # para abrir file dialog box
import bisect
from netcdf import netcdf
try:
	import h5py
except:
	pass
class MainMenu(ToolsPanel):

    def __init__(self,rect,jobs,(nc_path_list,nc),all_power=True,file_type="NETCDF",name="MainMenu"):
        ToolsPanel.__init__(self,rect,who_created_me=name)
        self.nc_path_list=nc_path_list
        self.nc=nc
        self.name=name
        self.screen=pygame.display.get_surface()
        self.file_type=file_type
        self.all_power=all_power
        jobs["%s:NEW_FILE"%name]=self.__NEW_FILE
        jobs["%s:PREVIOUS_FILE"%name]=self.__PREVIOUS_FILE
        jobs["%s:NEXT_FILE"%name]=self.__NEXT_FILE
        jobs["%s:MOVE_FILE"%name]=self.__MOVE_FILE
        self.i=0
        self.dirty=1
        
        self.loadBotton(name)


    def loadBotton(self,name):
        self.image.fill((255,0,255))
        self.buttonSize = (22, 22)
        rect=pygame.Rect((5,5),self.buttonSize)
        self.buttons['New_file']=Button(rect,"images/add_normal.bmp","%s:NEW_FILE"%name,"Add File")
    
    
    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf_ppi=self.rect.collidepoint(pos[0],pos[1])
        if self.all_power or over_surf_ppi:
            if event.type == KEYDOWN:
                if event.key ==pygame.K_LEFT:##< press left arrow move backwars in time
                    return "%s:PREVIOUS_FILE"%self.name
                if event.key ==pygame.K_RIGHT:##< press right arrow move forwars in time
                    return "%s:NEXT_FILE"%self.name
                if event.key ==pygame.K_LCTRL:
                    return "%s:MOVE_FILE"%self.name
            if event.type==pygame.MOUSEBUTTONDOWN:
                if self.mouse_over(): #mouse over menE
                    return self.takeMouseEvents(event.pos, event.button)
            return "NO_JOB"
        return "NO_JOB" 
    
    
    
    """NEW_FILE"""
    def __NEW_FILE(self,job):
        root = Tkinter.Tk() #nao sei o que faz, mas evita uma janela inconveniente
        root.withdraw() # o mesmo no anteriro
        new_paths=tkFileDialog.askopenfilenames() #pega o path (string) de um arquivo por uma janela
        for path in new_paths:
            i=bisect.bisect_left(self.nc_path_list, path) # find the position to insert new file
            self.nc_path_list.insert(i, path) #insert new_file path in order
        for name in self.nc_path_list:
        	print name
    """PREVIOUS_FILE"""
    def __PREVIOUS_FILE(self,job):
        i=bisect.bisect_left(self.nc_path_list, self.nc[0].filename) # find the position of self.open_path in self.netcdf_path, assuming it is in the list

        if i>0:## if there is a file back in time move 1 file backwards
            #self.nc[0].close();##close current NetCDF file and open new one, updating number of elevation and current elevation
            self.__openfile(self.nc_path_list[i-1])
            #print self.nc[0].filename
            #print self.nc[0].variables["polarization_mode"].data
            #print self.nc[0].variables["prt_mode"].data
            #for i in self.nc[0].variables["sweep_start_ray_index"].data:
            #    print self.nc[0].variables["prt_ratio"].data[i]
    """NEXT_FILE"""
    def __NEXT_FILE(self,job):
        i=bisect.bisect_right(self.nc_path_list, self.nc[0].filename) # find the position of self.open_path in self.netcdf_path, assuming it is in the list
        if i<len(self.nc_path_list):## if there is a file back in time move 1 file backwards
            #self.nc[0].close();##close current NetCDF file and open new one, updating number of elevation and current elevation
            self.__openfile(self.nc_path_list[i])
            #print self.nc[0].filename
            #print self.nc[0].variables["polarization_mode"].data
            #print self.nc[0].variables["prt_mode"].data
    """MOVE_FILE"""
    def __MOVE_FILE(self,job):
        open_file=switch_files(self.nc_path_list,self.nc[0].filename)
        if open_file!=self.nc[0].filename:
        	self.__openfile(open_file)

    
    def __openfile(self,open_file):
        if self.file_type=="NETCDF":
            self.nc[0]=netcdf(open_file,'r')
        elif self.file_type=="HDF":
            self.nc[0]=h5py.File(open_file,'r')    
            	
    def loop(self):

        if self.mouse_over():
            pos=pygame.mouse.get_pos()
            self.update(pos)
            self.dirty=1
        else:
            self.image.fill((111,117,123),self.rect)
            self.dirty=0
            
            
        if self.dirty!=0:
            if self.dirty==1:
                self.dirty=0
            
            self.blit_in(self.screen)




def switch_files(file_list,open_file):
    original=open_file
    file_list=file_list+["~"]
    font=pygame.font.Font("images/LiberationSans-Regular.ttf", 16)
    clock = pygame.time.Clock()
    h=font.get_height()
    surf=pygame.display.get_surface().subsurface(pygame.Rect(0,0,600,5*h))
    draw=True
    while(pygame.key.get_pressed()[pygame.K_LCTRL]):
        clock.tick(100)
        
        for event in pygame.event.get():
            if event.type == KEYDOWN:
               if event.key ==pygame.K_LEFT or event.key ==pygame.K_UP:
                   i=bisect.bisect_left(file_list, open_file) 
                   open_file=file_list[i-1]
                   draw=True           
               elif event.key ==pygame.K_RIGHT or event.key ==pygame.K_DOWN:
                   i=bisect.bisect_right(file_list, open_file) 
                   if i==len(file_list): i=0
                   open_file=file_list[i]   
                   draw=True
            elif event.type==pygame.MOUSEBUTTONDOWN:
               if event.button==4:
                   i=bisect.bisect_left(file_list, open_file) 
                   open_file=file_list[i-1]
                   draw=True           
               elif event.button==5:
                   i=bisect.bisect_right(file_list, open_file) 
                   if i==len(file_list): i=0
                   open_file=file_list[i]   
                   draw=True            
        
        if draw:
            draw=False
            surf.fill((0,0,0))
            surf.fill((255,255,255),pygame.Rect(0,2*h,600,h))
            i=bisect.bisect_left(file_list, open_file) 
            if len(file_list)>2:
                text=font.render(file_list[i-2],1,(255,255,255))
                surf.blit(text,(0,0))
            text=font.render(file_list[i-1],1,(255,255,255))
            surf.blit(text,(0,h))    
            text=font.render(file_list[i],1,(0,0,0))
            surf.blit(text,(0,2*h))
            if i<len(file_list)-1:
                text=font.render(file_list[i+1],1,(255,255,255))
                surf.blit(text,(0,3*h))  
            else:
                text=font.render(file_list[0],1,(255,255,255))
                surf.blit(text,(0,3*h))   
            if i<len(file_list)-2:
                text=font.render(file_list[i+2],1,(255,255,255))
                surf.blit(text,(0,4*h))
            elif len(file_list)>2:                       
                text=font.render(file_list[1],1,(255,255,255))
                surf.blit(text,(0,4*h))    
        pygame.display.flip()
    surf.fill((0,0,0))
    print "end swith file"
    if open_file=="~": open_file=original
    return open_file
    

