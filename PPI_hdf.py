
import pygame
from Sprites import *
import ToolsPanel
from palette import palette
from palette import polarization_pairs
from palette import static
from Colorbar import Colorbar
from Text import Text
import GeraProds
import numpy as np

class PPI:

    class MomentPanel(ToolsPanel.ToolsPanel):

        def __init__(self,father):
            self.father=father
            width = 60
            height = father.rect.height-100
            # canto superior esquerdo
            left=father.rect.width-width
            top = 0
            
            ToolsPanel.ToolsPanel.__init__(self,pygame.Rect(left,top,width,height),who_created_me="PPI")   


        def loadBotton(self,name,guarda_momento):
            self.image.fill((255,0,255))
            #Esta funcao serve para fazer os botoes dos momentos com base na biblioteca abaixo e dos momentos fornecidos no arquivo netcdf
            a=2
            self.name=name
            self.horizontal={}
            self.vertical={}
            font = pygame.font.Font("images/LiberationSans-Regular.ttf", 10)
            font.set_bold(True)


            self.buttonSizeMoment = (50,20)          
            espacamento=int(380/len(guarda_momento)+4)
            for i in polarization_pairs:
                flag=0
                if i[0] in guarda_momento:
                    rect=pygame.Rect((5,20*a),self.buttonSizeMoment)    
                    f=palette[i[0]][3]
                    g=palette[i[0]][4]
                    self.horizontal[i[0]]=ToolsPanel.Button(rect,"images/Button_rectangle.bmp","%s:CHANGE_MOMENT_%s"%(name,i[0]),"%s"%(f)) 
                    
                    text = font.render(i[0], 1, (255,255,255))
                    self.horizontal[i[0]].image.blit(text,(2,1))
                    flag=1
                if i[1] in guarda_momento:
                    rect=pygame.Rect((5,20*a),self.buttonSizeMoment)    
                    f=palette[i[1]][3]
                    g=palette[i[1]][4]
                    self.vertical[i[1]]=ToolsPanel.Button(rect,"images/Button_rectangle.bmp","%s:CHANGE_MOMENT_%s"%(name,i[1]),"%s"%(f)) 
                    
                    text = font.render(i[1], 1, (255,255,255))
                    self.vertical[i[1]].image.blit(text,(2,1))
                    flag=1
                if flag==1: a=a+1
            a=a+2
            for i in static:
                if i in guarda_momento:
                    print i
                    rect=pygame.Rect((5,20*a),self.buttonSizeMoment)    
                    f=""
                    g=""
                    f=palette[i][3]
                    g=palette[i][4]
                    if (f=="") and (g==""):
                        f=guarda_momento[a]
                        g=f

                    self.horizontal[i]=ToolsPanel.Button(rect,"images/Button_rectangle.bmp","%s:CHANGE_MOMENT_%s"%(name,i),"%s"%(f)) 
                    
                    text = font.render(i, 1, (255,255,255))
                    self.horizontal[i].image.blit(text,(2,1))   
                    self.vertical[i]=self.horizontal[i]
                    a=a+1

            self.buttons=self.horizontal

            rect=pygame.Rect((10,5),(22,22))
            self.horizontal['polarization']=ToolsPanel.Switch(rect,[("images/horizontal.bmp","%s:POLARIZATION_HORIZONTAL"%name),("images/vertical.bmp","%s:POLARIZATION_VERTICAL"%name)],"Polarization")
            self.vertical['polarization']=self.horizontal['polarization']
            self.pressed=self.buttons.itervalues().next()
            
        """CHANGE_POLARIZATION"""
        def CHANGE_POLARIZATION(self,job):
             self.image.fill((255,0,255))
             self.father.image.fill((111,117,123),self.rect)
             if job=="%s:POLARIZATION_HORIZONTAL"%self.name:
                self.buttons=self.horizontal
                index=1
             else:
                self.buttons=self.vertical
                index=0
             for button in self.buttons.values():
                button.dirty=1
             for i in polarization_pairs:
                if i[index]==self.pressed.job[14:]:
                    moment=i[(index+1)%2]
                    if moment in self.buttons: 
                        self.pressed=self.buttons[moment]
                        ui.CHANGE_MOMENT(ui,displayWindow,self.buttons[moment].job,text,line)
             self.dirty=1 
                            
        
        
        
    def __init__(self,rect,jobs,GeraImages,(hdf,elev,moment,threshold),name="PPI"):
        self.rect=rect
        self.GeraImages=GeraImages
        pos=pygame.mouse.get_pos()
        self.pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        self.image=pygame.display.get_surface().subsurface(self.rect)
        self.scaleFactor=1
        self.scaledDataSprite=Sprite(rect=pygame.Rect((0,0),self.rect.size),who_created_me="PPI")
        self.radarDataSurf=self.image.copy()
        self.new_hdf=hdf #this is a list and can eventualy change externaly, if change, it will update nc
        self.hdf=None #actual, class internal nc, will update in loop
        self.new_elev=elev 
        self.elev=None
        self.new_moment=moment
        self.moment=None
        self.new_threshold=threshold
        self.threshold=threshold[0]
        self.name=name
        self.jobs=jobs
        self.dirty=[1]
        self.value=[-999]
        self.backgroundPath=None
        import collections
        self.ppiSurfList = collections.deque(maxlen=30)
        self.panOn=False
        self.sprites={}
        
        self.sprites["menu"]=self.MomentPanel(self)
        self.text=Text(self)

#        jobs["%s:DRAW_LINE_ON"%name]=self.__INFORM_SPRITES
#        jobs["%s:DRAW_LINE_OFF"%name]=self.__INFORM_FILES
#        jobs["%s:INFORM_METADATA"%name]=self.__INFORM_METADATA
        jobs["%s:ZOOM_IN"%name]=self.__ZOOM
        jobs["%s:ZOOM_OUT"%name]=self.__ZOOM
        jobs["%s:PAN_ON"%name]=self.__PAN_ON
        jobs["%s:PAN_OFF"%name]=self.__PAN_OFF
        jobs["%s:ELEV_UP"%name]=self.__ELEV
        jobs["%s:ELEV_DOWN"%name]=self.__ELEV
        jobs["%s:REDRAW"%name]=self.__REDRAW
        jobs["%s:CHANGE_MOMENT"%name]=self.__CHANGE_MOMENT
        jobs["%s:POLARIZATION_HORIZONTAL"%name]=self.sprites["menu"].CHANGE_POLARIZATION                           
        jobs["%s:POLARIZATION_VERTICAL"%name]=self.sprites["menu"].CHANGE_POLARIZATION
        
    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf_ppi=self.rect.collidepoint(pos[0],pos[1])
        if event.type == KEYDOWN and over_surf_ppi:
            if event.key ==pygame.K_i:##< press i releases zoom in
                return "%s:ZOOM_IN"%self.name
            if event.key ==pygame.K_o:##< press o releases zoom out
                return "%s:ZOOM_OUT"%self.name
            if event.key ==pygame.K_UP:##< press up arrow releases elevation up
                return "%s:ELEV_UP"%self.name
            if event.key ==pygame.K_DOWN:##< press down arrow releases elevation down
                return "%s:ELEV_DOWN"%self.name
            if event.key ==pygame.K_r:##< press r releases redraw PPI
                return "%s:REDRAW"%self.name
            if event.key ==pygame.K_p:##< press p start or stop pan
                if self.panOn:
                    return "%s:PAN_OFF"%self.name
                else:
                    return "%s:PAN_ON"%self.name

        #----------------------------------------------
        # DEFINE AS ACOES DOS BOTOES E RODINHA DO MOUSE
        #----------------------------------------------
        # ALGUM BOTAO ESTA SENDO PRESSIONADO

        if event.type==pygame.MOUSEBUTTONDOWN:

            if self.sprites['menu'].mouse_over(self.rect.topleft): #mouse over menu
                return self.sprites['menu'].takeMouseEvents((event.pos[0]-self.rect.left,event.pos[1]-self.rect.top), event.button)     
            # BOTAO ESQUERDO
            if event.button==1 and over_surf_ppi==True:##< press with first mouse botton over PPI start to draw the line
                pass#return "DRAW_LINE_ON"


            # BOTAO DO MEIO
            if event.button==2 and over_surf_ppi==True:##< press with mittel mouse botton over PPI start to lock pan
                return "%s:PAN_ON"%self.name


            # RODINHA GIROU PARA FRENTE
            if event.button==4:
                if over_surf_ppi==True:##< move wheel forward releases zoom in
                    return "%s:ZOOM_IN"%self.name

            # RODINHA GIROU PARA TRAS
            if event.button==5:
                if over_surf_ppi==True:##< move wheel forward releases zoom in
                    return "%s:ZOOM_OUT"%self.name


#                # BOTAO DIREITO
#                if event.button==3:
#                # nao faz nada ainda
#                pass

        # O botao do meio esta pressionado
        # so no caso do evento MOUSEBUTTONDOWN ter sido perdido
        if pygame.mouse.get_pressed()[1] and over_surf_ppi==True:
            if self.panOn:
                return "NO_JOB"
            else:
                return "%s:PAN_ON"%self.name

        if event.type==pygame.MOUSEBUTTONUP:

            # BOTAO DO MEIO
            if event.button==2:##<release mittel mouse button releases pan
                return "%s:PAN_OFF"%self.name
        

        return "NO_JOB"
        
    def __startNewFile(self):
        self.hdf=self.new_hdf[0]
        self.nElevs = self.hdf["/what"].attrs["sets"]
        print self.nElevs
        if self.elev>self.nElevs-1: self.new_elev[0]=self.nElevs-1
        ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False
        ## Update Date displayed in the screen
        self.text.printDate("".join(self.hdf["/scan0/how"].attrs["timestamp"]))
        ## Update in Bottons
##        var=self.nc.variables
##        guarda_momento=range(self.hdf["/scan0/what"].attrs["descriptor_count"][0])
        guarda_momento=[]
        for moment_index in range(self.hdf["/scan0/what"].attrs["descriptor_count"]):
            moment_name="".join(self.hdf["/scan0/moment_%i"%moment_index].attrs["moment"])
            guarda_momento.append(moment_name)
            self.jobs['%s:CHANGE_MOMENT_%s'%(self.name,moment_name)]=self.__CHANGE_MOMENT
        print guarda_momento 
        self.sprites['menu'].loadBotton(self.name,guarda_momento)      
        
        ## Update Background
        from radarlist import radarlist
        self.backgroundPath=None
        for radar in radarlist:
            if radar[4] and abs(self.hdf["/where"].attrs["lat"]-(radar[0]))<0.01 and abs(self.hdf["/where"].attrs["lon"]-(radar[1]))<0.01:
                self.backgroundPath="map/map_%s.bmp"%radar[2]
                break
        self.text.printElev(self.new_elev[0],self.hdf["/scan%i/how" %self.new_elev[0]].attrs["elevation"])
        print self.hdf["/where"].attrs["lat"],self.hdf["/where"].attrs["lon"],self.hdf["/where"].attrs["height"]
        print "range step",self.hdf["/scan0/how"].attrs["range_step"],self.hdf["/scan0/how"].attrs["range_samples"]
        print "nbins",self.hdf["/scan0/how"].attrs["bin_count"]
        print "data",self.hdf["/what"].attrs["date"]
        print "site_name",self.hdf["/how"].attrs["site_name"]

#        print "keys", self.hdf["/scan0/how"].attrs.keys()
#        print self.hdf["/scan0/how"].attrs["unfolding"],self.hdf["/scan0/how"].attrs["radar_wave_length"],self.hdf["/how"].attrs["azimuth_beam"]



#        print "ray",self.hdf["/scan0/ray_header"].value
#        step=tuple(np.linspace(-var["nyquist_velocity"].getValue()[0],var["nyquist_velocity"].getValue()[0],22))
#        palette["VELH"]=(palette["VELH"][0],step,palette["VELH"][2],palette["VELH"][3],palette["VELH"][4],palette["VELH"][5])
#        palette["VELV"]=(palette["VELV"][0],step,palette["VELV"][2],palette["VELV"][3],palette["VELV"][4],palette["VELV"][5])

#        for key in var:
#            if key not in guarda_momento:
#                print key, var[key].getValue() 
        
  
    
    """HANDEL_JOBS"""
    """ZOOM"""
    def __ZOOM(self,job):
            pos=pygame.mouse.get_pos()
            pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
            ## before zooming save relative position of the Line
            #XYOr=GeraProds.screenToRelative((line.X0,line.Y0),displayWindow)
            #XY1r=GeraProds.screenToRelative((line.X1,line.Y1),displayWindow)
            if job=="%s:ZOOM_IN"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width<25: factor=1.1 
            elif job=="%s:ZOOM_OUT"%self.name and 1.0*self.scaledDataSprite.rect.width/self.rect.width>1: factor=0.9
            else: return
            scaledResX = int(self.scaledDataSprite.rect.width*factor)
            scaledResY = int(self.scaledDataSprite.rect.height*factor)
            # calcula a nova position em funcao da posicao do mouse
            newPosX = -int((pos[0]-self.scaledDataSprite.rect.left)*(factor-1)) 
            newPosY = -int((pos[1]-self.scaledDataSprite.rect.top)*(factor-1))
            self.scaledDataSprite.rect.size=(scaledResX, scaledResY)
            self.scaledDataSprite.rect.move_ip(newPosX, newPosY)
            ## restore Line in proper position after zomming
            #line.setStartPoint(GeraProds.relativeToScreen(XYOr,displayWindow))#restore line after zoom
            #line.setEndPoint(GeraProds.relativeToScreen(XY1r,displayWindow))
            ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
            self.up_to_date=False
            self.dirty[0]=1
            self.lastChangeTime = pygame.time.get_ticks()
    """PAN_ON"""
    def __PAN_ON(self,job):
        ## inicialisate pygame's relative mouse moviment
        pan = pygame.mouse.get_rel()
        ## record that PPI is no more not up to date and save time, @see \ref zoom->redraw
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False
        ## turn pan on
        self.panOn=True
        self.dirty[0]=2
    """PAN_OFF"""
    def __PAN_OFF(self,job):
        self.panOn=False
        self.dirty[0]=0
    """ELEV"""
    def __ELEV(self,job):
        print self.nElevs,self.elev
        if job=="%s:ELEV_UP"%self.name and self.elev+1<self.nElevs: self.new_elev[0]=self.elev+1 ## if there is a higher elevation, increase elevation in 1 level
        elif job=="%s:ELEV_DOWN"%self.name and self.elev>0: self.new_elev[0]=self.elev-1
        else: return
        self.lastChangeTime = pygame.time.get_ticks()
        self.up_to_date=False

    """REDRAW"""
    def __REDRAW(self,job): 
        ## lock mouse buttons for updating PPI
        pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
        #try:
        surf=self.GeraImages.makePPI_hdf((self.hdf,self.elev,self.moment,self.threshold), self.scaledDataSprite.rect,self.rect,self.backgroundPath)
        self.scaledDataSprite.blit(surf,
                         ((-1)*self.scaledDataSprite.rect.left,
                         (-1)*self.scaledDataSprite.rect.top))
   
        self.__desenhaCirculos(self.scaledDataSprite.image)
        #except:
        #    displayWindow.updatePPI(self.hdf,self.elev,self.moment)
        pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN) 
        self.dirty[0]=1     
    """CHANGE_MOMENT"""  
    def __CHANGE_MOMENT(self,job):
        self.new_moment[0]=job[len(self.name)+1+14:]
        #self.__REDRAW(job)
        self.up_to_date=False
        self.lastChangeTime = pygame.time.get_ticks()


    def loop(self):

        flag=False
        if self.hdf!=self.new_hdf[0]:
            self.__startNewFile()
            flag=True
        if self.scaledDataSprite.image.get_size() != self.scaledDataSprite.rect.size:
            self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
            self.lastChangeTime = pygame.time.get_ticks()
            self.up_to_date=False
        if flag or self.elev!=self.new_elev[0] or self.moment!=self.new_moment[0]:
            self.hdf=self.new_hdf[0] 
            self.elev=self.new_elev[0]
            if self.moment!=self.new_moment[0]:
                self.moment=self.new_moment[0]
                self.new_threshold[0]=0
                self.threshold=self.new_threshold[0]
                
            self.__moveSpaceTime(self.hdf,self.elev,self.moment)
            
        if self.threshold!=self.new_threshold[0]:
            self.threshold=self.new_threshold[0]
            self.lastChangeTime = pygame.time.get_ticks()
            self.up_to_date=False
            #self.__REDRAW(None)
        elif self.up_to_date==False and (pygame.time.get_ticks() - self.lastChangeTime)>600:##< @anchor zoom->redraw if PPI is not up to date and no move has been done in the last 0.6 
            self.up_to_date=True
            self.__REDRAW(None)

        if self.panOn:
            self.lastChangeTime = pygame.time.get_ticks()
            pan = pygame.mouse.get_rel() #movimento acumulado do mouse
            self.scaledDataSprite.rect.move_ip(pan[0],pan[1])
            #line.setStartPoint((line.X0+pan[0],line.Y0+pan[1]))#move line with surface
            #line.setEndPoint((line.X1+pan[0],line.Y1+pan[1]))#move line with surface


        if self.sprites['menu'].mouse_over(self.rect.topleft):
            pos=pygame.mouse.get_pos()
            self.sprites['menu'].update((pos[0]-self.rect.left,pos[1]-self.rect.top))
            self.sprites['menu'].dirty=1
        else:
            self.image.fill((111,117,123),self.sprites['menu'].rect)
            self.sprites['menu'].dirty=-1
            
            
        pos=pygame.mouse.get_pos()
        pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        if self.pos!=pos: #no need to atualise what didn't change
            self.pos = pos
        # Imprime LatLon
            if self.rect.collidepoint(pygame.mouse.get_pos()):
                latlon = GeraProds.eventPosToLatLon(pos,(self.hdf["/where"].attrs["lat"],self.hdf["/where"].attrs["lon"]),self.scaledDataSprite.rect)
                self.text.setLatLon(latlon)
                self.text.printLatLon()
                # atulisa refletividade na sprite
                (D,R,H) = screenCoordsToDRH(pos,self.scaledDataSprite.rect,self.elev,self.hdf,self.moment)
                self.value[0]=D
                self.text.printDRH((D,R,H),self.moment)

        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__update()
        else:            
            for sprite in self.sprites.values():
               sprite.write(self.image,self.scaledDataSprite)


    
    
    def __update(self):
        self.image.fill((111,117,123))
        self.scaledDataSprite.blit_in(self.image)
        for sprite in self.sprites.values():
            if sprite.dirty>-1:
                sprite.blit_in(self.image)
        
    def __moveSpaceTime(self,hdf,elev,moment):
        i=next((i for i,tupleSurf in enumerate(self.ppiSurfList) if tupleSurf[0:3] == (hdf.filename,elev,moment)), None) # using generator to search in deque ppiSurfList
        if i==None:
            self.ppiSurfList.append((hdf.filename,elev,moment,self.radarDataSurf.copy()))#first put one-more element in the list
            self.radarDataSurf=self.ppiSurfList[-1][3]#now point to it and then update
            pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
            self.__updatePPIfull()
            pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)
            
        else:
            self.radarDataSurf=self.ppiSurfList[i][3]#now point to it      
        self.scaledDataSprite.image = pygame.transform.smoothscale(self.radarDataSurf, self.scaledDataSprite.rect.size)
        self.dirty[0]=1   
        self.lastChangeTime = pygame.time.get_ticks()
        ## Update Elevation displayed in the screen
        self.text.printElev(self.elev,self.hdf["/scan%i/how" %self.elev].attrs["elevation"])
    def __updatePPIfull(self): 
        ##_________funciona
        ppiSurf = self.GeraImages.makePPI_hdf((self.hdf,self.elev,self.moment,0), self.image.get_rect(),self.rect,self.backgroundPath)
        self.scaledDataSprite.image.blit(pygame.transform.smoothscale(ppiSurf, self.scaledDataSprite.rect.size),(0,0));
        self.radarDataSurf.blit(ppiSurf,(0,0))
        self.__desenhaCirculos(self.radarDataSurf)


            
    def __desenhaCirculos(self, surf):
        w = surf.get_width()
        h = surf.get_height()
        radar_rect= surf.get_rect().inflate(w*(self.hdf["/scan%i/how" %self.elev].attrs["range"]/500000.-1),h*(self.hdf["/scan%i/how"% self.elev].attrs["range"]/500000.-1))
        surf=surf.subsurface(radar_rect)
        w = surf.get_width()
        h = surf.get_height()
        pygame.draw.circle(surf, (150,150,150), (w/2, h/2), h/8, 1)
        pygame.draw.circle(surf, (150,150,150), (w/2, h/2), 2*h/8, 1)
        pygame.draw.circle(surf, (150,150,150), (w/2, h/2), 3*h/8, 1)
        pygame.draw.circle(surf, (150,150,150), (w/2, h/2), 4*h/8, 1)
        pygame.draw.line(surf, (150, 150, 150), (0,w/2), (h, w/2))
        pygame.draw.line(surf, (150, 150, 150), (h/2,0), (h/2,w))        
            
    def convertion_to_radar(self,pos):
        return GeraProds.radarSurfCoordsToRadarCoords(pos,self.scaledDataSprite.rect)





import math

def screenCoordsToBinRay(eventPos,surf_rect,elev,hdf):
    
#    Sweep_start=nc.variables['sweep_start_ray_index'].getValue()
#    Sweep_end=nc.variables['sweep_end_ray_index'].getValue()
    Azim=[line[0] for line in hdf["/scan%i/ray_header"%elev]]

    

    Bin_Spacing =hdf["/scan%i/how" %elev].attrs["range_step"]* hdf["/scan%i/how" %elev].attrs["range_samples"]#250.0 meters
#    print "bin spacing",Bin_Spacing
    toBin = 1/Bin_Spacing
    rt =  4 * 6375000 / 3
    
    rsc = GeraProds.eventPosToRadarSurfCoords(eventPos,surf_rect)
    radarCoords = GeraProds.radarSurfCoordsToRadarCoords(rsc,surf_rect)
    x = radarCoords[0]
    y = radarCoords[1]
    #Range = nc.variables['range'].getValue()[-1] #200.0 km
    #Range=Range/1000
    Range=500
    fi = math.radians(elev) # elevacao
    rg = rt * math.atan2((Range * 1000 * math.cos(fi)),(rt+Range*1000*math.sin(fi)))
    s = math.sqrt(x**2+y**2)
    
    r0 = math.tan(s/rt)*rt 
    
    sigma = math.atan(r0/rt)
    betha = 0.5*math.pi + sigma
    alpha = 0.5*math.pi - sigma - fi
    r = r0 * math.sin(betha) / math.sin(alpha)  
    
    #----------------------------------------------------
    # Calcula o azimute, bin e ray 
    #----------------------------------------------------
    azim = math.degrees(math.atan2(x,y))
    if azim<0:
        azim=azim+360
    bin = int(round(r*toBin))
    array=np.array(Azim)
    ray=(np.abs(array-azim)).argmin()
  
    return (bin, ray, elev)

def screenCoordsToDRH(eventPos,surf_rect,elev,hdf,moment='DBZH'): #(DATA,RANGE,HEIGHT)
    rt =  4 * 6375000 / 3 #corrected earth radius
        
    (b,r,e)=screenCoordsToBinRay(eventPos,surf_rect,elev,hdf)
    for moment_index in range(hdf["/scan%i/what"%elev].attrs["descriptor_count"]):
        if "".join(hdf["/scan%i/moment_%i"%(elev,moment_index)].attrs["moment"])==moment:
            break
    
    Zpolar = hdf["/scan%i/moment_%i"%(elev,moment_index)]
    # pega os valores apropriados para a formacao da matriz no netcdf_radial

    # pega o numero de bins        

    if b<Zpolar.shape[1] and r>=0:
        range_max=Zpolar.attrs["dyn_range_max"]
        range_min=Zpolar.attrs["dyn_range_min"]
#        print range_max,range_min
        data_format="".join(Zpolar.attrs["format"])
        if data_format=="UV8": scale =(range_max - range_min)  /254.
        elif data_format=="UV16": scale =(range_max - range_min)  /(math.pow(2,16)-2.)
        else: print " Data format %s not reconised"%data_format
        offset=range_min-scale;#-scale remove the zero
        
        if Zpolar[r][b]==0:
            data = "no echo"  
        else:
            data = Zpolar[r][b]*scale+offset
#        data = Zpolar[Distance[Sweep_start[elev]+r]+b]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
    else:
        data = "no value"
    range2=hdf["/scan%i/how" %elev].attrs["range_start"]+hdf["/scan%i/how" %elev].attrs["range_step"]*b
    height=int(np.sqrt(range2*range2+rt*rt+2*range2*rt*np.sin(e*1.570796326794897/90))-rt+hdf["/where"].attrs["height"])
    range2=range2/1000
    
    return (data,range2,height)







