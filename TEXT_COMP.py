import pygame
from Sprites import *


colors =  (
(255,255,255),
(0,255,0),
(255,0,0),
(255,0,255),
(0,0,255),
(0,255,255),
(255,255,0),
(150,150,150),
(249,242,0),
(244,209,0),
(242,193,0),
(232,140,0),
(229,122,0),
(249,0,0),
(221,0,0),
(224,33,224),
(255,114,247),
(255,153,244),
)

class TEXT_COMP:

    def __init__(self,rect,jobs,(text_list,index),name="TEXT"):# can be inicialased with either a rect or a image
        self.rect=rect
        self.image=pygame.display.get_surface().subsurface(self.rect) 
        self.jobs=jobs
        self.name=name
        self.relativ=Sprite(rect=pygame.Rect((0,0),(2*self.rect.size[0],2*self.rect.size[1])),who_created_me=self.name)
        self.image.fill((0,0,0))
        self.text_list=text_list
        self.index=index
        self.font = pygame.font.SysFont("ubuntumono", 25)
        self.dirty=[2]
        jobs["%s:MOVE_UP"%name]=self.__MOVE_UP
        jobs["%s:MOVE_DOWN"%name]=self.__MOVE_DOWN
        
    def get_job(self,job,event):
         # se o botao esta pressionado   
        pos=pygame.mouse.get_pos()
        over_surf_ppi=self.rect.collidepoint(pos[0],pos[1])
        if over_surf_ppi:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button==5:##< press left arrow move backwars in time
                    return "%s:MOVE_UP"%self.name
                if event.button==4:##< press right arrow move forwars in time
                    return "%s:MOVE_DOWN"%self.name
        return "NO_JOB"
    
    def __MOVE_UP(self,job):
        self.relativ.rect.move_ip((0,+25))
    
    def __MOVE_DOWN(self,job):
        self.relativ.rect.move_ip((0,-25))
        
    def __rewrite(self):
        cont_line=0
        cont_word=0
        cont_letter=0
        self.relativ.image.fill((50,50,50))
        for line in self.text_list[self.index]:
            cont_word=0
            letter_pos=0
            for word in line:
                cont_letter=0
                for letter in word:
                    l=list(set([f[cont_line][cont_word][cont_letter] for f in self.text_list if cont_letter<len(f[cont_line][cont_word])]))
                    if len(l)==1:
                        pos=0
                    else:
                        pos=l.index(letter)+1
                    #print letter,l,pos
                    cor=colors[pos]
                    text = self.font.render(letter, 1, cor)
                    self.relativ.image.blit(text, (letter_pos,cont_line*25))
                    letter_pos=letter_pos+text.get_width()
                    cont_letter=cont_letter+1
                text = self.font.render(" ", 1, colors[0])
                self.relativ.image.blit(text, (letter_pos,cont_line*25))
                letter_pos=letter_pos+text.get_width()
                cont_word=cont_word+1
            cont_line=cont_line+1
    
    def __update(self):
        self.image.fill((0,0,0))
        self.relativ.blit_in(self.image)

    def loop(self):
        
        self.__rewrite()
        
        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__update()




