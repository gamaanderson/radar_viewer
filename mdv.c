#include <Python.h>
#include <stdio.h>
#include <netcdf.h>
#include <math.h>
#include <numpy/arrayobject.h>
//#include <time.h>
#define HALF_PI (1.570796326794897)

float range_res=100;
float azimuth_res=0.5;
float elev_res=0.1;

typedef struct {
    int id;
    size_t len;
} Dim;

 


static PyObject* set_resolution(PyObject* self, PyObject* args){

    PyArg_ParseTuple(args, "fff", &range_res, &azimuth_res, &elev_res);
    return  Py_BuildValue("i",0);

}


static PyArrayObject * vertical(PyObject* self, PyObject* args){

 
//    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    int smooth;

    PyArg_ParseTuple(args, "s(ii)(ff)(ff)is(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&smooth,&moment,&obj1,&obj2);
    int top=0,left=0,bottom=height,right=width;

    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to netcdf data
    //double radarRange;
    int ncid;
   
    //get netcdf data
    nc_open (pathnc, 0, &ncid);
    //get netcdf dimension, pair dim_id,dim_lenght
    Dim z_dim;
        nc_inq_dimid(ncid, "bottom_top", &z_dim.id);
        nc_inq_dimlen(ncid, z_dim.id,&z_dim.len);
       
    Dim y_dim;
        nc_inq_dimid(ncid, "south_north", &y_dim.id);
        nc_inq_dimlen(ncid, y_dim.id,&y_dim.len);
    Dim x_dim;
        nc_inq_dimid(ncid, "west_east", &x_dim.id);
        nc_inq_dimlen(ncid, x_dim.id,&x_dim.len);

    //get netcdf variables
    int z_id;
    double *zc=(double*)malloc(sizeof(double)*z_dim.len);
        nc_inq_varid(ncid,"bottom_top", &z_id);
        nc_get_var_double(ncid,z_id, zc);
    int y_id;
    double *yc=(double*)malloc(sizeof(double)*y_dim.len);
        nc_inq_varid(ncid,"yc", &y_id);
        nc_get_var_double(ncid,y_id, yc);
        float scale_y;
        nc_get_att_float(ncid, y_id, "scale_factor",&scale_y);
        float offset_y;
        nc_get_att_float(ncid, y_id, "add_offset", &offset_y);
    int x_id;
    double *xc=(double*)malloc(sizeof(double)*x_dim.len);
        nc_inq_varid(ncid,"xc", &x_id);
        nc_get_var_double(ncid,x_id,xc);
        float scale_x;
        nc_get_att_float(ncid, x_id, "scale_factor",&scale_x);
        float offset_x;
        nc_get_att_float(ncid, x_id, "add_offset", &offset_x);


    // get moments and attributes
    int data_id;
    signed char *data=(signed char*)malloc(sizeof(char)*z_dim.len*x_dim.len*y_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        size_t start[4]={0,0,0,0},count[4]={1,z_dim.len,y_dim.len,x_dim.len};
        nc_get_vara_schar(ncid,data_id, start,count,data);
        
        signed char fill_value;
        nc_get_att_schar(ncid, data_id, "no_echo", &fill_value);
        signed char missing_value;
        nc_get_att_schar(ncid, data_id, "missing_value", &missing_value);
        float scale;
        nc_get_att_float(ncid, data_id, "scale_factor",&scale);
        float offset;
        nc_get_att_float(ncid, data_id, "add_offset", &offset);



    nc_close(ncid);


    //criating data matriz
    int nCols = right-left;//height;
    int nRows = bottom-top;//width;
    short DATA[nRows][nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float x,y,h,t;
    int x_index, y_index,z_index;

    npy_intp dims[2]={nCols,nRows};
    PyArrayObject *array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;
    float dh=heightkm * 1000/height;

//getc(stdin);
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
            t=((j+left)*1.0/(nCols-1));
            x= (1-t)*X1+t*X2;
            y= (1-t)*Y1+t*Y2;
            h= (+0.5 + height-(i+top)) * dh;
            //printf("%f %f %f\n",x,y,h);
            x_index=(x-(xc[0]*scale_x+offset_x))/((xc[1]*scale_x+offset_x)-(xc[0]*scale_x+offset_x))+0.5;
            y_index=(y-(yc[0]*scale_y+offset_y))/((yc[1]*scale_y+offset_y)-(yc[0]*scale_y+offset_y))+0.5;
            z_index=(h-zc[0])/(zc[1]-zc[0])-0.5;
            //printf("index %i %i %i\n",x_index,y_index,z_index);
            
            
            if(x_index>=0 && y_index>=0 && x_index<x_dim.len && y_index<y_dim.len && z_index>=0 && z_index<z_dim.len){
                DATA[i][j]=data[z_index*y_dim.len*x_dim.len+y_index*x_dim.len+x_index];
                }
            else
                DATA[i][j]=missing_value;
            }
        }
   
    if (smooth==1) median_filter_short (DATA, nRows,nCols, 1); 

    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
            value=DATA[i][j]*scale+offset;
            if(DATA[i][j]==(fill_value*scale+offset))
                 pixelarray[j*nRows+i]=0;
            else if(DATA[i][j]==missing_value)
                pixelarray[j*nRows+i]=110*256*256+110*256+110;     
            else if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[j*nRows+i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else pixelarray[j*nRows+i]=0;            
 
        }
    }

    free(data);
    free(xc);
    free(yc);

    free(zc);

    return array;

}


static PyArrayObject * CAPPI(PyObject* self, PyObject* args){

 
//    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    int elev;
    int smooth;
    
    PyArg_ParseTuple(args, "s(ii)(ii)(ii)iis(OO)", &pathnc, &height, &width,&left,&top,&right,&bottom, &elev,&smooth,&moment,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim z_dim;
        nc_inq_dimid(ncid, "bottom_top", &z_dim.id);
        nc_inq_dimlen(ncid, z_dim.id,&z_dim.len);
    Dim y_dim;
        nc_inq_dimid(ncid, "south_north", &y_dim.id);
        nc_inq_dimlen(ncid, y_dim.id,&y_dim.len);
    Dim x_dim;
        nc_inq_dimid(ncid, "west_east", &x_dim.id);
        nc_inq_dimlen(ncid, x_dim.id,&x_dim.len);

    //get netcdf variables
//    int z_id;
//    double *zc=(double*)malloc(sizeof(double)*z_dim.len);
//        nc_inq_varid(ncid,"bottom_top", &z_id);
//        nc_get_var_double(ncid,z_id, zc);
    int y_id;
    double *yc=(double*)malloc(sizeof(double)*y_dim.len);
        nc_inq_varid(ncid,"yc", &y_id);
        nc_get_var_double(ncid,y_id, yc);
        float scale_y;
        nc_get_att_float(ncid, y_id, "scale_factor",&scale_y);
        float offset_y;
        nc_get_att_float(ncid, y_id, "add_offset", &offset_y);
    int x_id;
    double *xc=(double*)malloc(sizeof(double)*x_dim.len);
        nc_inq_varid(ncid,"xc", &x_id);
        nc_get_var_double(ncid,x_id,xc);
        float scale_x;
        nc_get_att_float(ncid, x_id, "scale_factor",&scale_x);
        float offset_x;
        nc_get_att_float(ncid, x_id, "add_offset", &offset_x);

    // get moments and attributes
    int data_id;
    signed char *data=(signed char*)malloc(sizeof(char)*x_dim.len*y_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        size_t start[4]={0,elev,0,0},count[4]={1,1,y_dim.len,x_dim.len};
        nc_get_vara_schar(ncid,data_id, start,count,data);
        
        signed char fill_value;
        nc_get_att_schar(ncid, data_id, "no_echo", &fill_value);
        signed char missing_value;
        nc_get_att_schar(ncid, data_id, "missing_value", &missing_value);
        float scale;
        nc_get_att_float(ncid, data_id, "scale_factor",&scale);
        float offset;
        nc_get_att_float(ncid, data_id, "add_offset", &offset);

    radarRange=500000; //maximal range in meters

    nc_close(ncid);


    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    short DATA[nRows][nCols];
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float x,y;
    int x_index, y_index;

    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;

    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){

            x=-radarRange+(j+left)*2*radarRange/height;
            y=radarRange-(i+top)*2*radarRange/width;
            x_index=(x-(xc[0]*scale_x+offset_x))/((xc[1]*scale_x+offset_x)-(xc[0]*scale_x+offset_x))+0.5;
            y_index=(y-(yc[0]*scale_y+offset_y))/((yc[1]*scale_y+offset_y)-(yc[0]*scale_y+offset_y))+0.5;

            if(x_index>=0 && y_index>=0 && x_index<x_dim.len && y_index<y_dim.len)
                DATA[i][j]=data[y_index*x_dim.len+x_index];
            else
                DATA[i][j]=missing_value;
            }
        }
        
    if (smooth==1) median_filter_short (DATA, nRows,nCols, 1);
    
    for(i=0;i<nRows;i++){
        for(j=0;j<nCols;j++){
            value=DATA[i][j]*scale+offset;
            if (DATA[i][j]==fill_value)
                pixelarray[j*nCols+i]=0;
            else if(DATA[i][j]==missing_value)
                pixelarray[j*nCols+i]=1*256*256+1*256+1;
            else if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[j*nCols+i]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else pixelarray[j*nCols+i]=0;

        }
    }

    free(yc);
    free(xc);
    free(data);
    
    

    return array;
}



static PyObject* vertical_unscaled(PyObject* self, PyObject* args){

 
//    float rt = 8500000;//raio aparente da terra
    float X1,Y1,X2,Y2;
    int heightkm=16;
    int height=200,width=400;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    
    PyArg_ParseTuple(args, "s(ii)(ff)(ff)s(OO)", &pathnc, &width,&height, &X1,&Y1,&X2,&Y2,&moment,&obj1,&obj2);
    int top=0,left=0,bottom=height,right=width;
    
    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);


    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);


    //variable to netcdf data
    //double radarRange;
    int ncid;
   
    //get netcdf data
    nc_open (pathnc, 0, &ncid);
    //get netcdf dimension, pair dim_id,dim_lenght
    Dim z_dim;
        nc_inq_dimid(ncid, "bottom_top", &z_dim.id);
        nc_inq_dimlen(ncid, z_dim.id,&z_dim.len);
       
    Dim y_dim;
        nc_inq_dimid(ncid, "south_north", &y_dim.id);
        nc_inq_dimlen(ncid, y_dim.id,&y_dim.len);
    Dim x_dim;
        nc_inq_dimid(ncid, "west_east", &x_dim.id);
        nc_inq_dimlen(ncid, x_dim.id,&x_dim.len);

    //get netcdf variables
    int z_id;
    double *zc=(double*)malloc(sizeof(double)*z_dim.len);
        nc_inq_varid(ncid,"bottom_top", &z_id);
        nc_get_var_double(ncid,z_id, zc);
    int y_id;
    double *yc=(double*)malloc(sizeof(double)*y_dim.len);
        nc_inq_varid(ncid,"yc", &y_id);
        nc_get_var_double(ncid,y_id, yc);
    int x_id;
    double *xc=(double*)malloc(sizeof(double)*x_dim.len);
        nc_inq_varid(ncid,"xc", &x_id);
        nc_get_var_double(ncid,x_id,xc);

    // get moments and attributes
    int data_id;
    float *data=(float*)malloc(sizeof(float)*z_dim.len*x_dim.len*y_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        size_t start[4]={0,0,0,0},count[4]={1,z_dim.len,y_dim.len,x_dim.len};
        nc_get_vara_float(ncid,data_id, start,count,data);
        
        float fill_value;
        nc_get_att_float(ncid, data_id, "no_echo", &fill_value);
        float missing_value;
        nc_get_att_float(ncid, data_id, "missing_value", &missing_value);



    nc_close(ncid);


    //criating data matriz
    int nCols = right-left;//height;
    int nRows = bottom-top;//width;
    float DATA;
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float x,y,h,t;
    int x_index, y_index,z_index;

    npy_intp dims[2]={nCols,nRows};
    PyArrayObject *array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;
    float dh=heightkm * 1000/height;
 

//getc(stdin);
    for(j=left;j<right;j++){
        for(i=top;i<bottom;i++){
            t=(j*1.0/(nCols-1));
            x= (1-t)*X1+t*X2;
            y= (1-t)*Y1+t*Y2;
            h= (+0.5 + height-i) * dh;
            //printf("%f %f %f\n",x,y,h);
            x_index=(x-xc[0])/(xc[1]-xc[0])+0.5;
            y_index=(y-yc[0])/(yc[1]-yc[0])+0.5;
            z_index=(h-zc[0])/(zc[1]-zc[0])-0.5;
            //printf("index %i %i %i\n",x_index,y_index,z_index);
            if(x_index>=0 && y_index>=0 && x_index<x_dim.len && y_index<y_dim.len && z_index>=0 && z_index<z_dim.len){
                DATA=data[z_index*y_dim.len*x_dim.len+y_index*x_dim.len+x_index];
                }
            else
                DATA=missing_value;
            value=DATA;

            if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[(j-left)*nRows+(i-top)]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else if(DATA==missing_value)
                pixelarray[(j-left)*nRows+(i-top)]=110*256*256+110*256+110;
            else pixelarray[(j-left)*nRows+(i-top)]=0;
        }
    }

    free(data);
    free(xc);
    free(yc);

    free(zc);

    return array;

}


static PyArrayObject * CAPPI_unscaled(PyObject* self, PyObject* args){

 
//    float rt = 8500000;//raio aparente da terra
    int height=200,width=400;
    int top,left,bottom,right;
    int i,j,k;
    const char *pathnc,*moment;
    PyObject* obj1,*obj2;
    PyArrayObject * py_palette,*py_step;
    int * palette;
    float * step;
    int elev;
    
    PyArg_ParseTuple(args, "s(ii)(ii)(ii)is(OO)", &pathnc, &height, &width,&left,&top,&right,&bottom, &elev,&moment,&obj1,&obj2);
    right=right+left;
    bottom=bottom+top;


    py_palette=(PyArrayObject * )PyArray_ContiguousFromAny(obj1,NPY_INT, 0, 4);
    py_step=(PyArrayObject * )PyArray_ContiguousFromAny(obj2,NPY_FLOAT, 0, 4);

    npy_intp *DIMS;

    DIMS=PyArray_DIMS(py_palette);
    int ncolors=DIMS[0];

    palette=(int *)PyArray_DATA(py_palette);
    step=(float *)PyArray_DATA(py_step);

    //variable to netcdf data
    double radarRange;
    int ncid;
    
    //get netcdf data
    nc_open (pathnc, 0, &ncid);

    //get netcdf dimension, pair dim_id,dim_lenght
    Dim z_dim;
        nc_inq_dimid(ncid, "bottom_top", &z_dim.id);
        nc_inq_dimlen(ncid, z_dim.id,&z_dim.len);
    Dim y_dim;
        nc_inq_dimid(ncid, "south_north", &y_dim.id);
        nc_inq_dimlen(ncid, y_dim.id,&y_dim.len);
    Dim x_dim;
        nc_inq_dimid(ncid, "west_east", &x_dim.id);
        nc_inq_dimlen(ncid, x_dim.id,&x_dim.len);

    //get netcdf variables
//    int z_id;
//    double *zc=(double*)malloc(sizeof(double)*z_dim.len);
//        nc_inq_varid(ncid,"bottom_top", &z_id);
//        nc_get_var_double(ncid,z_id, zc);
    int y_id;
    double *yc=(double*)malloc(sizeof(double)*y_dim.len);
        nc_inq_varid(ncid,"yc", &y_id);
        nc_get_var_double(ncid,y_id, yc);
    int x_id;
    double *xc=(double*)malloc(sizeof(double)*x_dim.len);
        nc_inq_varid(ncid,"xc", &x_id);
        nc_get_var_double(ncid,x_id,xc);

    // get moments and attributes
    int data_id;
    float *data=(float*)malloc(sizeof(float)*x_dim.len*y_dim.len);
        nc_inq_varid(ncid,moment, &data_id);
        size_t start[4]={0,elev,0,0},count[4]={1,1,y_dim.len,x_dim.len};
        nc_get_vara_float(ncid,data_id, start,count,data);
        
        float fill_value;
        nc_get_att_float(ncid, data_id, "no_echo", &fill_value);
        float missing_value;
        nc_get_att_float(ncid, data_id, "missing_value", &missing_value);



    radarRange=500000; //maximal range in meters

    nc_close(ncid);


    //criating data matriz
    int nRows = right-left;//height;
    int nCols = bottom-top;//width;
    float DATA;
    // cria os elementos do vetor h, s, R0, BETHA e H0

    float x,y;
    int x_index, y_index;

    npy_intp dims[2]={nCols,nRows};
    PyArrayObject * array=(PyArrayObject *)PyArray_SimpleNew(2,dims,NPY_UINT32);
    int* pixelarray=(int*)array->data;
//    int pixelarray[nCols][nRows];printf("%i\n",__LINE__);fflush(stdout);
    float value;


    for(j=left;j<right;j++){
        for(i=top;i<bottom;i++){

            x=-radarRange+j*2*radarRange/height;
            y=radarRange-i*2*radarRange/width;
            x_index=(x-xc[0])/(xc[1]-xc[0])+0.5;
            y_index=(y-yc[0])/(yc[1]-yc[0])+0.5;

            if(x_index>=0 && y_index>=0 && x_index<x_dim.len && y_index<y_dim.len)
                DATA=data[y_index*x_dim.len+x_index];
            else
                DATA=missing_value;
            value=DATA;
            if(value>step[0] && value<step[ncolors]){
                k=0;
                while(value>step[k+1]) k++;
                pixelarray[(j-left)*nCols+(i-top)]=palette[k*3+0]*256*256+palette[k*3+1]*256+palette[k*3+2];//acontece que a orientacao corrente é coluna linha, mudando so aqui por preguisa
            }
            else if(DATA==missing_value)
                pixelarray[(j-left)*nCols+(i-top)]=1*256*256+1*256+1;
            else pixelarray[(j-left)*nCols+(i-top)]=0;

        }
    }

    free(yc);
    free(xc);
    free(data);
    
    

    return array;
}




/*  define functions in module */
static PyMethodDef CarMethods[] =
{

     {"vertical", vertical, METH_VARARGS, "make vertical cut over cartesian volume"},
     {"CAPPI", CAPPI, METH_VARARGS, "make CAPPI over cartesian volume"},
     {"vertical_unscaled", vertical_unscaled, METH_VARARGS, "make vertical cut over cartesian volume"},
     {"CAPPI_unscaled", CAPPI_unscaled, METH_VARARGS, "make CAPPI over cartesian volume"},
     {"set_resolution",set_resolution,METH_VARARGS,"Set Resolution for (range,azimuth,elevation) in order to make the images"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC

initmdv_module(void)
{
     (void) Py_InitModule("mdv_module", CarMethods);
      /* IMPORTANT: this must be called */
     import_array();
}
