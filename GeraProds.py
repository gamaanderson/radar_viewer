#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np 
import math
import pygame
   
    
"""Funções de Conversão de Coordenadas Originalmente na Main"""    
# Coordenadas do Display para Sistema de Coordenadas da Superficie de Radar (pixels)
def eventPosToRadarSurfCoords(eventPos,surf_rect):
    x = eventPos[0]-surf_rect.left
    y = eventPos[1]-surf_rect.top
    return (x,y)

# Sistema de Coordenadas da Superficie de Radar para Coordenadas do Radar (metros) 
def radarSurfCoordsToRadarCoords(radarSurfCoords,surf_rect):
    #r = nc.variables['range'].getValue()[-1]
    r=500000
    x = 2*r*(float(radarSurfCoords[0])/float(surf_rect.width)) - r
    y = (-1)*(2*r*(float(radarSurfCoords[1])/float(surf_rect.height)) - r)
    return (x,y)

# Metros (radar na origem) para Graus de LatLon (universal)
def radarCoordsToLatLon(radarCoords,(radar_lat,radar_lon)):

    EARTH_RADIUS=6375000 #in meters
    HALF_PI=1.570796326794897

    lat = radar_lat + radarCoords[1]*90/EARTH_RADIUS/HALF_PI;
    lon = radar_lon + radarCoords[0]*90/EARTH_RADIUS/HALF_PI/np.cos(HALF_PI*lat/90)

    return (lat, lon)

# As três de cima concatenadas
def eventPosToLatLon(eventPos,(radar_lat,radar_lon),surf_rect):
    rsc = eventPosToRadarSurfCoords(eventPos,surf_rect)
    rc = radarSurfCoordsToRadarCoords(rsc,surf_rect)
    return radarCoordsToLatLon(rc,(radar_lat,radar_lon))

# As duas de cima concatenadas    
def screenToRadarCoords(eventPos,nc):
    rsc = eventPosToRadarSurfCoords(eventPos)
    return radarSurfCoordsToRadarCoords(rsc,nc)

# pos in PPI to relative pos in radarsurface
def screenToRelative(pos):
    x = float(pos[0]-spritedic['PPI'].rect.top - dw.radarSurfPos[0])/dw.scaledRes
    y = float(pos[1]-spritedic['PPI'].rect.left - dw.radarSurfPos[1])/dw.scaledRes
    return (x,y)
    
# invert (pos in PPI to relative pos in radarsurface)
def relativeToScreen(pos):
    x=pos[0]*dw.scaledRes+dw.radarSurfPos[0]+spritedic['PPI'].rect.top
    y=pos[1]*dw.scaledRes+dw.radarSurfPos[1]+spritedic['PPI'].rect.left
    return (x,y)


def screenCoordsToBinRay(eventPos,surf_rect,elev,nc):
    
    Sweep_start=nc.variables['sweep_start_ray_index'].getValue()
    Sweep_end=nc.variables['sweep_end_ray_index'].getValue()
    Azim=nc.variables['azimuth'].getValue()

    
    
    Bin_Spacing = nc.variables['range'].attributes["meters_between_gates"] #250.0 meters
    toBin = 1/Bin_Spacing
    rt =  4 * 6375000 / 3
    
    rsc = eventPosToRadarSurfCoords(eventPos,surf_rect)
    radarCoords = radarSurfCoordsToRadarCoords(rsc,surf_rect)
    x = radarCoords[0]
    y = radarCoords[1]
    #Range = nc.variables['range'].getValue()[-1] #200.0 km
    #Range=Range/1000
    Range=500
    fi = math.radians(elev) # elevacao
    rg = rt * math.atan2((Range * 1000 * math.cos(fi)),(rt+Range*1000*math.sin(fi)))
    s = math.sqrt(x**2+y**2)
    
    r0 = math.tan(s/rt)*rt 
    
    sigma = math.atan(r0/rt)
    betha = 0.5*math.pi + sigma
    alpha = 0.5*math.pi - sigma - fi
    r = r0 * math.sin(betha) / math.sin(alpha)  
    
    #----------------------------------------------------
    # Calcula o azimute, bin e ray 
    #----------------------------------------------------
    azim = math.degrees(math.atan2(x,y))
    if azim<0:
        azim=azim+360
    bin = int(round(r*toBin))
    array=Azim[Sweep_start[elev]:Sweep_end[elev]+1]
    ray=(np.abs(array-azim)).argmin()
    if(ray>Sweep_end[elev]-Sweep_start[elev]):
        ray = -1
  
    return (bin, ray, elev)

def screenCoordsToDRH(eventPos,surf_rect,elev,nc,moment='DBZH'): #(DATA,RANGE,HEIGHT)
    if moment not in nc.variables:
        moment='DBZH'
    rt =  4 * 6375000 / 3 #corrected earth radius
        
    (b,r,e)=screenCoordsToBinRay(eventPos,surf_rect,elev,nc)
    Zpolar = np.array(nc.variables[moment].getValue())
    # pega os valores apropriados para a formacao da matriz no netcdf_radial

    Sweep_start=nc.variables['sweep_start_ray_index'].getValue()
    if nc.attributes["n_gates_vary"]=="true":
        Gates=nc.variables['ray_n_gates'].getValue()
        Distance=nc.variables['ray_start_index'].getValue()
    else:
        Gates=np.zeros([nc.dimensions["time"]])#  nc.variables['ray_n_gates'].getValue()
        Gates.fill(nc.dimensions["range"])
        Distance=range(0,nc.dimensions["range"]*nc.dimensions["time"],nc.dimensions["range"])
        Zpolar=Zpolar.reshape(-1)
    Fill_value=(nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"])
    # pega o numero de bins        

    if b<nc.dimensions['range'] and r>=0:
        range2=nc.variables['range'].getValue()[b]
        data = Zpolar[Distance[Sweep_start[elev]+r]+b]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
        height=int(np.sqrt(range2*range2+rt*rt+2*range2*rt*np.sin(e*1.570796326794897/90))-rt+nc.variables['altitude'].getValue())
        range2=range2/1000
    else:
        data = "no value"
        try: 
            range2=b*nc.variables['range'].attributes["meters_between_gates"]
            height=int(np.sqrt(range2*range2+rt*rt+2*range2*rt*np.sin(e*1.570796326794897/90))-rt+nc.variables['altitude'].getValue())
            range2=range2/1000
        except: 
            range2="unknow "
            height="unknow "
    if data==Fill_value:
        data = "no echo"  
    return (data,range2,height)

def CutPosToDRE((x,y),height,nc,moment):
    if moment not in nc.variables:
        moment='DBZH'
    rt =  4 * 6375000 / 3
    HALF_PI=1.570796326794897
    Bin_Spacing = nc.variables['range'].attributes["meters_between_gates"] #250.0 meters
    toBin = 1/Bin_Spacing    
    #First Calculate actual elevation
    s=np.sqrt(x**2+y**2)
    SIGMA = s/rt;
    R0 = rt*np.tan(SIGMA);
    BETHA =HALF_PI+ SIGMA;
    H0 = np.sqrt(R0**2+rt**2)-rt;
    H1 = height*1000-nc.variables["altitude"].getValue()-H0;
    R = np.sqrt(R0**2 + H1**2 - 2*R0*H1*np.cos(BETHA));
    ELEV = np.arcsin(H1*np.sin(BETHA)/R)*90/HALF_PI;#in degrees
    #find nominal elevation
    idx=(np.abs(nc.variables['fixed_angle'].getValue()-ELEV)).argmin()
    elev=nc.variables['fixed_angle'].getValue()[idx]
#    print ELEV,elev,idx
    if abs(elev-ELEV)>0.5:
        return ("no value",R,-1)
    azim = math.degrees(math.atan2(x,y))
    
    #----------------------------------------------------
    # Calcula o azimute, bin e ray 
    #----------------------------------------------------
    Sweep_start=nc.variables['sweep_start_ray_index'].getValue()
    Sweep_end=nc.variables['sweep_end_ray_index'].getValue()
    Azim=nc.variables['azimuth'].getValue()
    if azim<0:
        azim=azim+360
    b = int(round(R*toBin))
    array=Azim[Sweep_start[idx]:Sweep_end[idx]+1]
    r=(np.abs(array-azim)).argmin()
    if(r>Sweep_end[idx]-Sweep_start[idx]):
        r = -1
    e=elev

    Zpolar = np.array(nc.variables[moment].getValue())
    # pega os valores apropriados para a formacao da matriz no netcdf_radial

    Sweep_start=nc.variables['sweep_start_ray_index'].getValue()
    if nc.attributes["n_gates_vary"]=="true":
        Gates=nc.variables['ray_n_gates'].getValue()
        Distance=nc.variables['ray_start_index'].getValue()
    else:
        Gates=np.zeros([nc.dimensions["time"]])#  nc.variables['ray_n_gates'].getValue()
        Gates.fill(nc.dimensions["range"])
        Distance=range(0,nc.dimensions["range"]*nc.dimensions["time"],nc.dimensions["range"])
        Zpolar=Zpolar.reshape(-1)
    Fill_value=(nc.variables[moment].attributes["_FillValue"]*nc.variables[moment].attributes["scale_factor"])
    # pega o numero de bins        

    if b<nc.dimensions['range'] and r>=0:
        range2=nc.variables['range'].getValue()[b]
        data = Zpolar[Distance[Sweep_start[idx]+r]+b]*nc.variables[moment].attributes["scale_factor"]+nc.variables[moment].attributes["add_offset"]
        height=int(np.sqrt(range2*range2+rt*rt+2*range2*rt*np.sin(e*1.570796326794897/90))-rt+nc.variables['altitude'].getValue())
        range2=range2/1000
    else:
        data = "no value"
        try: 
            range2=b*nc.variables['range'].attributes["meters_between_gates"]
            height=int(np.sqrt(range2*range2+rt*rt+2*range2*rt*np.sin(e*1.570796326794897/90))-rt+nc.variables['altitude'].getValue())
            range2=range2/1000
        except: 
            range2="unknow "
            height="unknow "
    if data==Fill_value:
        data = "no echo" 
    return (data,range2,elev)


    #url = "http://www.simepar.br/geoserver/simepar/wms?service=WMS&version=1.1.0&request=GetMap&layers=simepar:bluemarble.B2&styles=&bbox=-52.35054016113281,-27.300827026367188,-48.366703033447266,-23.70523452758789&width=512&height=512&srs=EPSG:4326&format=image/png"
    #url = "http://www.simepar.br/geoserver/simepar/wms?service=WMS&version=1.1.0&request=GetMap&layers=simepar:bluemarble.B2&styles=&bbox=-52.35054016113281,-27.300827026367188,-48.366703033447266,-23.70523452758789&width=512&height=512&srs=EPSG:4326&format=image/png"
    #url = "http://www.simepar.br/geoserver/simepar/wms?service=WMS&version=1.1.0&request=GetMap&layers=simepar:bluemarble.B2&styles=&bbox=-52.3530292511,-27.3030738831,-48.3691921234,-23.7074813843&width=400&height400&srs=EPSG:4326&format=image/png
    #url = "http://www.simepar.br/geoserver/simepar/wms?service=WMS&version=1.1.0&request=GetMap&layers=simepar:bluemarble.B2&styles=&bbox="+str(lon0)+","+str(lat1)+","+str(lon1)+","+str(lat0)+"&width="+str(res)+"&height="+str(res)+"&srs=EPSG:4326&format=image/png"    
    #print url    
    #urllib.urlretrieve(url, "background.png")

