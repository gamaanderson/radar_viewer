#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame
from math import *

class Line:
    OverP0="OverP0"
    OverP1="OverP1"
    OverLine="OverLine"
            
    def __init__(self,rect,jobs,(nc,radarSprite,external_dirty),name="Line",mode="line"):
        self.rect=rect
        pos=pygame.mouse.get_pos()
        self.pos=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        self.image=pygame.display.get_surface().subsurface(self.rect)
        self.new_nc=nc #this is a list and can eventualy change externaly, 
        self.name=name
        self.jobs=jobs
        self.dirty=[1]
        self.radarSprite=radarSprite
        self.radarRect=radarSprite.rect.copy()
        width=self.radarSprite.rect.width
        self.P=[(width/2,width/2),(width,width/2)]
        self.Proj = [(width/2,width/2)]
        self.new_mouseOver=[False]
        self.mouseOver=False
        self.external_dirty=external_dirty
        self.isMovingP0=False
        self.isMovingP1=False
        self.isMovingLine=False
        self.mode=mode
        
        jobs["%s:MOVE_P0"%self.name]=self.__MOVE_P0
        jobs["%s:MOVE_P1"%self.name]=self.__MOVE_P1
        jobs["%s:MOVE_LINE"%self.name]=self.__MOVE_LINE
        jobs["%s:START_LINE"%self.name]=self.__START_LINE
        jobs["%s:STOP"%self.name]=self.__STOP
        jobs["%s:CHANGE_LINE_MODE"%self.name]=self.__CHANGE_LINE_MODE
                    
    def get_job(self,job,event):
        pos=pygame.mouse.get_pos()
        over_surf_ppi=self.rect.collidepoint(pos[0],pos[1])
        
        if event.type==pygame.MOUSEBUTTONDOWN :
            if event.button==1 and over_surf_ppi:
                if self.mouseOver==self.OverP0:
                    return "%s:MOVE_P0"%self.name

                elif self.mouseOver==self.OverP1:
                    return "%s:MOVE_P1"%self.name

                elif self.mouseOver==self.OverLine:
                    return "%s:MOVE_LINE"%self.name

                if self.distPosToLine()>5:
                    return "%s:START_LINE"%self.name
        elif event.type==pygame.MOUSEBUTTONUP:
            return "%s:STOP"%self.name
        elif event.type == pygame.KEYDOWN and over_surf_ppi:
            if event.key ==pygame.K_l:##< press i releases zoom in
                return "%s:CHANGE_LINE_MODE"%self.name
        return "NO_JOB"
        
 
 
    
    """HANDEL_JOBS"""
    def __REDEFINE(self,job):
        P=self.P
        P[0]=(float(P[0][0])/self.radarRect.width*self.radarSprite.rect.width,float(P[0][1])/self.radarRect.height*self.radarSprite.rect.height)
        P[1]=(float(P[1][0])/self.radarRect.width*self.radarSprite.rect.width,float(P[1][1])/self.radarRect.height*self.radarSprite.rect.height)
    
    def __MOVE_P0(self,job):
        self.isMovingP0=True
        self.isMovingP1=False
        self.isMovingLine=False    
    
    def __MOVE_P1(self,job):
        self.isMovingP1=True
        self.isMovingP0=False
        self.isMovingLine=False        
    def __MOVE_LINE(self,job):
        self.isMovingP0=False
        self.isMovingP1=False    
        self.isMovingLine=True
    def __START_LINE(self,job):
        pos=pygame.mouse.get_pos()
        pos=(pos[0]-self.rect.left-self.radarRect.left,pos[1]-self.rect.top-self.radarRect.top)
        self.setStartPoint(pos)
        self.__MOVE_P1(job)
    def __STOP(self,job):
        self.isMovingP0=False
        self.isMovingP1=False
        self.isMovingLine=False
    def __CHANGE_LINE_MODE(self,job):
        if self.mode=="square":
            self.mode="line"
        elif self.mode=="line":
            self.mode="square"
    def loop(self):
        if self.dirty[0]!=0:
            if self.dirty[0]==1:
                self.dirty[0]=0
            self.__draw()
       
        
        pos=pygame.mouse.get_pos()
        pos1=(pos[0]-self.rect.left,pos[1]-self.rect.top)
        
        if pygame.mouse.get_pressed()[0]:
            if self.isMovingP0:
                self.setStartPoint((pos1[0]-self.radarRect.left,pos1[1]-self.radarRect.top))
            if self.isMovingP1:
                self.setEndPoint((pos1[0]-self.radarRect.left,pos1[1]-self.radarRect.top))
            if self.isMovingLine:
                dx = int(pos1[0]-self.pos[0])
                dy = int(pos1[1]-self.pos[1])
                self.setStartPoint((self.P[0][0] + dx, self.P[0][1] + dy))
                self.setEndPoint((self.P[1][0]+dx, self.P[1][1]+dy))
        else:
            self.__STOP(None)
        # Atualiza a posicao
        self.pos= pos1
        if self.rect.collidepoint(pos):
            self.Proj[0] = self.projOverLine()
        if self.distPosToP0()<5:
            self.new_mouseOver[0]=self.OverP0
        elif self.distPosToP1()<5:
            self.new_mouseOver[0]=self.OverP1
        elif self.distPosToLine()<3:
            self.new_mouseOver[0]=self.OverLine
        else:
            self.new_mouseOver[0]=False
        
        
        #self.external_dirty[0]=2
        self.dirty[0]=2

                
        if self.mouseOver!=self.new_mouseOver[0]:
            self.external_dirty[0]=1
            self.dirty[0]=1
            self.mouseOver=self.new_mouseOver[0]
 
        if self.radarRect.size!=self.radarSprite.rect.size or self.radarRect.topleft!=self.radarSprite.rect.topleft:
            self.__REDEFINE(None)
            self.radarRect=self.radarSprite.rect.copy()
            self.external_dirty[0]=1
            self.dirty[0]=1
         
        
    def __draw(self):
 
        
        P0 = (self.P[0][0]+self.radarRect.left, self.P[0][1]+self.radarRect.top)
        P1 = (self.P[1][0]+self.radarRect.left, self.P[1][1]+self.radarRect.top)

        cross1Width = 2;  cross1Color = (220, 220, 220)
        cross2Width = 2;  cross2Color = (220, 220, 220)
        lineWidth   = 4;  lineColor   = (200, 200, 200)

    #           self.pontoMedio()

        # verifica a distancia do mouse ate a linha
        # e destaca a linha se estiver suficientemente proximo
        if self.mouseOver==self.OverP0:
            cross1Width = 3
            cross1Color = (255, 255, 255)

        elif self.mouseOver==self.OverP1:
            cross2Width = 3
            cross2Color = (255, 255, 255)

        elif self.mouseOver==self.OverLine:
            lineWidth = 4
            lineColor = (255, 255, 255)


        if self.mode=="line":
        # desenha a linha principal
            pygame.draw.line(self.image, lineColor, P0, P1, lineWidth)
            # desenha cruzinha na extremidade X0Y0
            pygame.draw.line(self.image, cross1Color, (P0[0]-5, P0[1]), (P0[0]+5, P0[1]),cross1Width)
            pygame.draw.line(self.image, cross1Color, (P0[0], P0[1]-5), (P0[0], P0[1]+5),cross1Width)
            # desenha cruzinha na extremidade X1Y1
            pygame.draw.line(self.image, cross2Color, (P1[0]-5, P1[1]), (P1[0]+5, P1[1]),cross2Width)
            pygame.draw.line(self.image, cross2Color, (P1[0], P1[1]-5), (P1[0], P1[1]+5),cross2Width)
            if self.distPosToLine()<10:
                self.showLineLenght(P0,P1)
            self.drawPointOverLine()

        elif self.mode =="square":
            lineColor = (100, 100, 100)
            lineWidth   = 2
            pygame.draw.line(self.image, lineColor, (P0[0],P0[1]), (P0[0],P1[1]), lineWidth)
            pygame.draw.line(self.image, lineColor, (P0[0],P1[1]), (P1[0],P1[1]), lineWidth)
            pygame.draw.line(self.image, lineColor, (P1[0],P1[1]), (P1[0],P0[1]), lineWidth)
            pygame.draw.line(self.image, lineColor, (P1[0],P0[1]), (P0[0],P0[1]), lineWidth)


    def setStartPoint(self, pos):
        self.P[0] = pos
        self.external_dirty[0]=1
        self.dirty[0]=1


    def setEndPoint(self, pos):
        self.P[1] = pos
        self.external_dirty[0]=1
        self.dirty[0]=1

    def distPosToLine(self):
        pos = self.pos
        dist = max(abs(pos[0]-self.radarRect.left-self.Proj[0][0]),abs(pos[1]-self.radarRect.top-self.Proj[0][1]))
        return dist


    # distancia da posicao do mouse ate a extremidade 1 da linha
    def distPosToP0(self):
        pos =  self.pos
        dist = max(abs(pos[0]-self.radarRect.left-self.P[0][0]),abs(pos[1]-self.radarRect.top-self.P[0][1]))
        return dist

    # distancia da posicao do mouse ate a extremidade 2 da linha
    def distPosToP1(self):
        pos = self.pos
        dist = max(abs(pos[0]-self.radarRect.left-self.P[1][0]),abs(pos[1]-self.radarRect.top-self.P[1][1]))
        return dist


    # MOSTRA O COMPRIMENTO DA LINHA
    def showLineLenght(self,pos0,pos1):
        lenght=sqrt(((self.P[0][0]-self.P[1][0])/float(self.radarRect.width))**2+((self.P[0][1]-self.P[1][1])/float(self.radarRect.height))**2)*2*500    
        text = str(lenght)[0:6] + " km"
        
        pos=((pos0[0]+pos1[0])/2,(pos0[1]+pos1[1])/2)
        
        writePixel(self.image,pos,(255,255,255),(0,0,0),text)
    
    def projOverLine(self):
        """
        Funcao que retorna a projecao
        ortogonal da posicao cursor sobre a linha.
        """
        pos = pygame.mouse.get_pos() 	# pega a posicao do cursor
        pos=(pos[0]-self.rect.left-self.radarRect.left,pos[1]-self.rect.top-self.radarRect.top)
        # internal product
        product=(pos[0]-self.P[0][0])*(self.P[1][0]-self.P[0][0])+(pos[1]-self.P[0][1])*(self.P[1][1]-self.P[0][1])    
        if product==0:
            return self.P[0]
        else:
            k=float(product)/((self.P[1][0]-self.P[0][0])**2+(self.P[1][1]-self.P[0][1])**2)
            if k<0: k=0
            elif k>1: k=1
            x=self.P[0][0]+k*(self.P[1][0]-self.P[0][0])
            y=self.P[0][1]+k*(self.P[1][1]-self.P[0][1])
            return (x,y)
    
    
    def drawPointOverLine(self):
        Proj = self.Proj[0]
        x = int(round(Proj[0]))
        y = int(round(Proj[1]))
        color = (255, 0,0 )
        radius = 2
        pygame.draw.circle(self.image, color, (x+self.radarRect.left+1,y+self.radarRect.top+1), radius )
        return (x,y)


def writePixel(s,pos,color,outline_color,text):
    font= pygame.font.Font("images/Regular.ttf", 10)
    outline = pygame.font.Font("images/Outline.ttf", 10)
    text=" "+text
    text_surf=font.render(text,0,color)
    outline_surf=outline.render(text,0,outline_color)
    s.blit(outline_surf,(pos[0]-3,pos[1]))  
    s.blit(text_surf,(pos[0]-3,pos[1]))     






