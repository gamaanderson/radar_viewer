


import pygame
from pygame.locals import *
import os

jobs={}
modules=[]
layers=[]


def start_up(res):
    pygame.display.set_mode(res, pygame.RESIZABLE)
    
    jobs["QUIT"]=__QUIT
    jobs["STOP"]=__NO_JOB
    jobs["NO_JOB"]=__NO_JOB
    jobs["RESIZE"]=__NO_JOB


def add_module(module,layer):
    modules.append(module)
    layers.append(layer)


def __NO_JOB(job):
    pass 

def __QUIT(job):
    exit(0)
 
def get_job(job):
    while True:
        event=pygame.event.poll()
        if event.type == NOEVENT:
            return "NO_JOB"
        if event.type == QUIT:
            return "QUIT"

        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                return "QUIT"
            if event.key ==pygame.K_q:
                return "STOP"
                 
        if event.type==pygame.VIDEORESIZE:
            return "RESIZE"

        for module in job_module:
            job2= module.get_job(job,event)
            if job2 != "NO_JOB":
                return job2
    
    


def loop():
    job="NO_JOB"
    clock = pygame.time.Clock()
    global job_module
    job_module=sorted(modules,key=lambda mod: layers[modules.index(mod)],reverse=True)
    print job_module
    t1=0
    while job!="STOP" :

        clock.tick(1000)
        
        
        for module in modules:
            module.loop()
        
        
        job=get_job(job)
        if (job!="NO_JOB"):
            print job
            print "iteration time: ", pygame.time.get_ticks()-t1, "ms"
            t1 = pygame.time.get_ticks()
        jobs[job](job)
        
        pygame.display.flip()





  
