import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
import h5py
import pygame
import pyGeraImages_hdf5odim as GeraImages
# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import PPI_hdf5odim as PPI
from ToolsPanel import MainMenu
import Line
import Corte_hdf5odim as Corte
import Colorbar
hdf_path=["/simepar/projetos/opera/ZPHI/HDF5/9505CAS-20141104-143004-PPIVol.hdf5",
]



new_hdf=[h5py.File(hdf_path[0],'r')]
print "end open nercdf"
Kern.start_up((1600,800))
ppi_rect=pygame.Rect(50,50,600,600)
moment=["Z"]
threshold=[0]
ppi=PPI.PPI(ppi_rect,Kern.jobs,GeraImages,(new_hdf,[0],moment,threshold),name="PPI")


Kern.add_module(ppi,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(50,100,55,500),Kern.jobs,(moment,threshold,ppi.value,ppi.dirty),name="Colorbar"),2)

Kern.add_module(MainMenu(pygame.Rect(600,0,30,30),Kern.jobs,(hdf_path,new_hdf),file_type="HDF",name="MainMenu"),2)

line=Line.Line(ppi_rect,Kern.jobs,(new_hdf,ppi.scaledDataSprite,ppi.dirty),name="Line",)
Kern.add_module(line,0)


cut_rect=pygame.Rect(700,100,650,300)

Kern.add_module(Corte.VerticalCut(cut_rect,Kern.jobs,GeraImages,(new_hdf,moment,threshold,line.P,line.Proj),ppi.convertion_to_radar,name="Cut"),0)

Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")
