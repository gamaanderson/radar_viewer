# This Python file uses the following encoding: utf-8
# lat , lon , sigla, city , there is map, there is topografia
radarlist=[
(-19.94,-44.43,"RME","Morro do Elefante/MG",True,True),
(-25.50,-50.36,"TXS","Teixeira Soares/PR",True,True),
(-24.87,-53.52,"CAS","Cascavel/PR",True,True),
(-25.33,-57.52,"ASU","Assunção/PARAGUAI",True,True),
(-28.13,-49.47,"MDI","Morro da Igreja/SC",True,True),
(-31.40,-52.70,"CGU","Canguçu/RS",True,False),
(-15.98,-48.02,"GA1","Gama/DF",True,False),
(-22.46,-43.29,"PCO","Petropolis/RJ",True,False),
(-23.60,-47.09,"SRO","Sao Roque/SP",True,False),
(-29.23,-54.93,"STI","Santiago/RS",True,False),
(-20.27,-54.47,"JGI","Jaraguari/MS",True,False),
(-05.90,-35.25,"NT1","Natal/RN",True,False),
(-09.36,-40.57,"PL1","Petrolina/PE",True,False),
(-12.90,-38.32,"SV1","Salvador/BA",True,False),
(-18.20,-45.46,"TM1","Tres Marias/MG",True,False),
(-18.20,-45.46,"TRM","TresMarias/MG",True,False),
(-22.36,-49.03,"BRU","Bauru/SP",True,False),
(-22.17,-51.37,"PPR","Presidente Prudente/SP",True,False),
(-27.23,-49.46,"SAM","SAM/SC",True,False),
(+08.97,-79.56,"PAN","Panama",True,False),

]

