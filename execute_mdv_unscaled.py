import os
#try to compile
os.system("python setup.py build_ext --inplace")



import Kern
from netcdf import netcdf 
import pygame
# Inicia o PyGame
pygame.init()
#permite que teclas fiquem precionadas, uso principal tecla "i" para ZOOM_IN e "o" para ZOOM_OUT
pygame.key.set_repeat(100,100)
# as we don't use mousemotion event it is better to let it down for perfomace
pygame.event.set_blocked(pygame.MOUSEMOTION)


import os
import CAPPI_mdv_unscaled

from ToolsPanel import MainMenu
import Line
import Corte_mdv
import Colorbar
netcdf_path=["/tmp/dados_leonardo/20141017215157.nc"]


new_nc=[netcdf(netcdf_path[0],'r')]
print "end open nercdf"
Kern.start_up((1200,800))
ppi_rect=pygame.Rect(50,50,600,600)
moment=["UH"]
threshold=[0]
cappi=CAPPI_mdv_unscaled.CAPPI(ppi_rect,Kern.jobs,(new_nc,[0],moment,threshold),name="CAPPI")


Kern.add_module(cappi,1)
Kern.add_module(Colorbar.Colorbar(pygame.Rect(55,100,45,500),Kern.jobs,(moment,threshold,cappi.value,cappi.dirty),name="Colorbar"),2)

Kern.add_module(MainMenu(pygame.Rect(600,0,30,30),Kern.jobs,(netcdf_path,new_nc),name="MainMenu"),2)

line=Line.Line(ppi_rect,Kern.jobs,(new_nc,cappi.scaledDataSprite,cappi.dirty),name="Line")
Kern.add_module(line,0)


cut_rect=pygame.Rect(700,100,450,300)

#Kern.add_module(Corte_mdv.VerticalCut(cut_rect,Kern.jobs,(new_nc,moment,threshold,line.P,line.Proj),cappi.convertion_to_radar,name="Cut"),0)

Kern.loop()
import cProfile as profile
#profile.run("Kern.loop()")
