


from netCDF4 import Dataset
import numpy as np

class variable():

    def __init__(self):
        self.data=None
        self.dimensions=None
        self.attributes={}
        
    def getValue(self):
        if self.data==None:
            return self.var.getValue()
        else:
            return self.data


class variableRR():
    def __init__(self,variables):
        self.data=None
        self.dimensions=variables["DBZH"].dimensions
        self.attributes=variables["DBZH"].attributes.copy()
        self.attributes["scale_factor"]=1
        self.attributes["add_offset"]=0
        self.variables=variables;
        self.a=200;
        self.b=1.6;
        #calculate value
        data1=self.variables["DBZH"].getValue()
        data=data1*self.variables["DBZH"].attributes["scale_factor"]+self.variables["DBZH"].attributes["add_offset"]
        FillValue=self.variables["DBZH"].attributes["_FillValue"]
        a=self.a
        b=self.b
        volume = 10.**((data/(10.*b)) - ((np.log10(a))/b))
        self.data=np.where(np.absolute(data1-FillValue)<1, FillValue, volume)


    def getValue(self):
        return self.data


class variableDIFF():
    def __init__(self,variables,m2,m1):
        self.data=None
        self.dimensions=variables[m1].dimensions
        self.attributes=variables[m1].attributes.copy()
        self.attributes["scale_factor"]=1
        self.attributes["add_offset"]=0
        self.variables=variables;
        data1a=self.variables[m1].getValue()
        data1=data1a*self.variables[m1].attributes["scale_factor"]+self.variables[m1].attributes["add_offset"]
        FillValue1=self.variables[m1].attributes["_FillValue"]

        data2a=self.variables[m2].getValue()
        data2=data2a*self.variables[m2].attributes["scale_factor"]+self.variables[m2].attributes["add_offset"]
        FillValue2=self.variables[m2].attributes["_FillValue"]
        volume = data2-data1
        self.data=np.where(np.absolute(data1a-FillValue1)<1, FillValue1, volume)


    def getValue(self):
        return self.data

class variableZ_KDP():
    def __init__(self,variables):
        self.data=None
        m1="DBZH"
        m2="KDP"
        self.dimensions=variables[m1].dimensions
        self.attributes=variables[m1].attributes.copy()
        self.attributes["scale_factor"]=1
        self.attributes["add_offset"]=0
        self.variables=variables;
        data1a=self.variables[m1].getValue()
        data1=data1a*self.variables[m1].attributes["scale_factor"]+self.variables[m1].attributes["add_offset"]
        FillValue1=self.variables[m1].attributes["_FillValue"]

        data2a=self.variables[m2].getValue()
        data2=data2a*self.variables[m2].attributes["scale_factor"]+self.variables[m2].attributes["add_offset"]
        FillValue2=self.variables[m2].attributes["_FillValue"]
        data2=np.where(data2==0, 0.001, data2)
        volume = np.where(np.absolute(data1a-FillValue1)<1, FillValue1, data1/data2) 
        self.data=np.where(np.absolute(data2a-FillValue2)<1, FillValue1, volume)


    def getValue(self):
        return self.data


class netcdf2():

    def __init__(self,filename,mode):
        print "loading netcdf"
        self.filename=filename
        nc=Dataset(filename,"r")
        self.dimensions={}
        self.variables={}
        self.attributes={}
        for dim in nc.dimensions:
            self.dimensions[dim]=len(nc.dimensions[dim])
        
        for var in nc.variables:
            self.variables[var]=variable()
            self.variables[var].data=np.array(nc.variables[var][:])
            self.variables[var].dimensions=nc.variables[var].dimensions
            for name in nc.variables[var].ncattrs():
                self.variables[var].attributes[name]=getattr(nc.variables[var],name)
                
        for name in nc.ncattrs():
            self.attributes[name]=getattr(nc,name)
               
        nc.close()
        "end loading nc"



        
class netcdf1():

    def __init__(self,filename,mode):
        print "loading netcdf"
        import Nio        
        self.filename=filename
        nc=Nio.open_file(filename, 'r')
        self.dimensions={}
        self.variables={}
        self.attributes={}
        for dim in nc.dimensions:
            self.dimensions[dim]=nc.dimensions[dim]
        
        for var in nc.variables:
            self.variables[var]=variable()
            self.variables[var].data=np.array(nc.variables[var].get_value())
            self.variables[var].dimensions=nc.variables[var].dimensions
            for name in nc.variables[var].attributes:
                self.variables[var].attributes[name]=nc.variables[var].attributes[name]
                
        for name in nc.attributes:
            self.attributes[name]=nc.attributes[name]
               
        nc.close()
        "end loading nc"
 
 
from scipy.io import netcdf as old_nc  
        
class netcdf3():

    def __init__(self,filename,mode):
        print "loading netcdf_old"
        
        self.filename=filename
        nc=old_nc.netcdf_file(filename, 'r')
        self.dimensions={}
        self.variables={}
        self.attributes={}
        for dim in nc.dimensions:
            self.dimensions[dim]=nc.dimensions[dim]
        
        for var in nc.variables:
            self.variables[var]=variable()
            self.variables[var].data=np.array(nc.variables[var].data)
            self.variables[var].dimensions=nc.variables[var].dimensions
            for name in nc.variables[var]._attributes:
                self.variables[var].attributes[name]=nc.variables[var]._attributes[name]
                
        for name in nc._attributes:
            self.attributes[name]=nc._attributes[name]
               
        nc.close()
        "end loading nc"
        


import pygame
class netcdf():
    
    def __init__(self,filename,mode):
        print "loading netcdf"
        t1 = pygame.time.get_ticks()
        
        import Scientific.IO.NetCDF as ScientificNetcdf
        print "Import netcdf: ", pygame.time.get_ticks()-t1, "ms"
        self.filename=filename
        nc=ScientificNetcdf.NetCDFFile(filename, 'r')
        print "Open netcdf: ", pygame.time.get_ticks()-t1, "ms"
        self.dimensions={}
        self.variables={}
        self.attributes=vars(nc)
        for dim in nc.dimensions:
            self.dimensions[dim]=nc.dimensions[dim]
        
        for var in nc.variables:
            self.variables[var]=variable()
            self.variables[var].var=nc.variables[var]
            self.variables[var].dimensions=nc.variables[var].dimensions
            self.variables[var].attributes=vars(nc.variables[var])
        try:
            if "RR" not in self.variables:
                var=variableRR(self.variables)
                self.variables["RR"]=var
            if "DBZH" in self.variables and "ACZH"  in self.variables:
                var=variableDIFF(self.variables,"ACZH","DBZH")
                self.variables["DIFF"]=var
            if "UH" in self.variables and "ACUH"  in self.variables:
                var=variableDIFF(self.variables,"ACUH","UH")
                self.variables["DIFF2"]=var
            if "DBZH" in self.variables and "KDP"  in self.variables:
                var=variableZ_KDP(self.variables)
                self.variables["Z_KDP"]=var
        except:
            pass
        print "Envelop: ", pygame.time.get_ticks()-t1, "ms"        
        
               
        #nc.close()
        "end loading nc"

#import Scientific.IO.NetCDF as netcdf     
