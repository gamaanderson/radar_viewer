#compile with python setup.py build_ext --inplace
from distutils.core import setup, Extension

# define the extension module
cut_module = Extension('netcdf_module', sources=['netcdf2.c','median.c'],libraries=["netcdf"])
cartesian = Extension('mdv_module', sources=['mdv.c','median.c'],libraries=["netcdf"])
hdf5_module = Extension('hdf5_module', sources=['hdf5.c'],libraries=["hdf5"],include_dirs=["/usr/include/mpi/"])
# run the setup
setup(ext_modules=[cut_module,cartesian,hdf5_module])


